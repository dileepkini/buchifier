﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Buchifier.Construction;
using Buchifier.Construction.Enumeration;
using Buchifier.Grammar;
using Buchifier.Logic;
using NUnit.Framework;

namespace Buchifier.Tests {
    public static class TestUtils {
        private static void CreateFile(string filePath) {
            var fi = new FileInfo(filePath);
            if (!fi.Directory.Exists) {
                Directory.CreateDirectory(fi.DirectoryName);
            }
        }

        private static string GetClass(string fullName) {
            var match = Regex.Match(fullName, @"\.(\w+)\.\w+$");
            return match.Groups[1].Value;
        }

        private const string OutputFolder = @"C:\Users\Dileep\Documents\Buchifier\";

        public static void PerformTest(string predicateString) {
            PerformTest(ExprGenerator.Generate(predicateString));
        }

        public static void PerformTest(Predicate predicate) {
            Console.WriteLine("Performing Test on : {0}", predicate);
            Console.WriteLine("LaTeX: {0}", predicate.ToTex());
            var automaton = predicate.ConstructBddTransitionSystem();
            Console.WriteLine("{0}, {1}, {2}({3})", automaton.StateCount, automaton.TransitionCount, automaton.FinalTransitionCount, automaton.CountConnectedComponents);
            var filename = TestContext.CurrentContext.Test.Name;
            var folder = GetClass(TestContext.CurrentContext.Test.FullName);
            var filePath = OutputFolder + folder + "\\" + filename + ".dot";
            CreateFile(filePath);
            using (var writer = new StreamWriter(filePath)) {
                writer.Write(automaton.DotString());
            }
        }

        public static void EnumerateInitialState(string predicateString) {
            var initSet = Constructor.EnumerateInitialHybridStates(ExprGenerator.Generate(predicateString));
            Console.WriteLine(String.Join("\n", initSet));
        }
    }
}
