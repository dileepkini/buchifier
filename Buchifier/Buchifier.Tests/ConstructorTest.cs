﻿using NUnit.Framework;

namespace Buchifier.Tests {
    [TestFixture]
    public class ConstructorTest {


        [Test]
        public void Tautology1() {
            TestUtils.PerformTest("a | ! a");
        }

        [Test]
        public void Tautology2() {
            TestUtils.PerformTest("G a | F ! a");
        }

        [Test]
        public void Tautology3() {
            TestUtils.PerformTest("X a | X ! a");
        }

        [Test]
        public void InfiniteConj() {
            TestUtils.PerformTest("G F (a & b) & G F (c | d) & G F (e & f)");
        }

        [Test]
        public void a() {
            TestUtils.PerformTest("a");
        }



        [Test]
        public void GFaorFborFc() {
            TestUtils.PerformTest("G (F a | F b | F c)");
        }

        [Test]
        public void aAndGb() {
            TestUtils.PerformTest("a & G b");
        }

        [Test]
        public void GFGTest() {
            TestUtils.PerformTest("G ((a & G b) | (c & G d))");
        }

        [Test]
        public void Fp() {
            TestUtils.PerformTest("F p");
        }

        [Test]
        public void Ga() {
            TestUtils.PerformTest("G a");
        }

        [Test]
        public void FGaOrGFb() {
            TestUtils.PerformTest("F G a | G F b");
        }

        [Test]
        public void FGa() {
            TestUtils.PerformTest("F G a");
        }

        [Test]
        public void GFb() {
            TestUtils.PerformTest("G F b");
        }

        [Test]
        public void FGaAndFGc() {
            TestUtils.PerformTest("F G a & F G c");
        }

        [Test]
        public void EK2() {
            TestUtils.PerformTest("(F G a | G F b) & (F G c | G F d)");
        }

        [Test]
        public void EK3() {
            TestUtils.PerformTest("(F G a | G F b) & (F G c | G F d) & (F G e | G F f)");
        }

        [Test]
        public void EK4() {
            TestUtils.PerformTest("(F G a | G F b) & (F G b | G F c)");
        }

        [Test]
        public void EK5() {
            TestUtils.PerformTest("(F G a | G F b) & (F G b | G F c) & (F G c | G F d)");
        }


        [Test]
        public void WorstCase1() {
            TestUtils.PerformTest("G ((a1 & F b1) | (a2 & F b2))");
        }

        [Test]
        public void WorstCase() {
            TestUtils.PerformTest("G ((a1 & F b1) | (a2 & F b2) | (a3 & F b3))");
        }

        [Test]
        public void LaTorre() {
            TestUtils.PerformTest("G ((a1 & G b1) | (a2 & G b2) | (a3 & G b3))");
        }

        [Test]
        public void GlobalImpl() {
            TestUtils.PerformTest("G ((a1 | (F b1 & F c1)) & (a2 | (F b2 & F c2)))");
        }

        [Test]
        public void AckFuture3() {
            TestUtils.PerformTest("G ((a1 | G b1) & (a2 | G b2) & (a3 | G b3))");
        }

        [Test, Ignore("Takes 2+ minutes")]
        public void GlobalImpl2() {
            TestUtils.PerformTest("G ((a1 | (F b1 & F c1)) & (a2 | (F b2 & F c2)) & (a3 | G b3))");
        }
    }
}
