﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Buchifier.Tests {
    [TestFixture]
    public class EnumeratorTests {
        [Test]
        public void Propositional() {
            TestUtils.EnumerateInitialState("a");
        }

        [Test]
        public void Ga() {
            TestUtils.EnumerateInitialState("G a");
        }

        [Test]
        public void Fa() {
            TestUtils.EnumerateInitialState("F a");
        }

        [Test]
        public void FaorFb() {
            TestUtils.EnumerateInitialState("F a | F b");
        }

        [Test]
        public void GaorFb() {
            TestUtils.EnumerateInitialState("G a | F b");
        }

        [Test]
        public void GaorGb() {
            TestUtils.EnumerateInitialState("G a | G b");
        }

        [Test]
        public void GaandGb() {
            TestUtils.EnumerateInitialState("G a & G b");
        }

        [Test]
        public void Impl() {
            TestUtils.EnumerateInitialState("G (a | F b)");
        }

        [Test]
        public void FGaorFGborGFc() {
            TestUtils.EnumerateInitialState("F G a | F G b | G F c");
        }

        [Test]
        public void EK2() {
            TestUtils.EnumerateInitialState("(F G a | G F b) & (F G c | G F d)");
        }

        [Test]
        public void aUb() {
            TestUtils.EnumerateInitialState("a U b");
        }
    }
}
