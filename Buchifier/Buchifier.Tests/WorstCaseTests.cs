﻿using System.Linq;
using Buchifier.Logic;
using NUnit.Framework;

namespace Buchifier.Tests {

    [TestFixture]
    public class WorstCaseTests {
        private static Proposition A(int i) {
            return new Proposition("a" + i);
        }

        private static Proposition B(int i) {
            return new Proposition("b" + i);
        }

        private static Proposition C(int i) {
            return new Proposition("c" + i);
        }

        private static Proposition D(int i) {
            return new Proposition("d" + i);
        }

        private Predicate G(Predicate p) {
            return new Global(p);
        }

        private Predicate F(Predicate p) {
            return new Future(p);
        }

        private Predicate Or(params Predicate[] operands) {
            if (operands.Length == 0)
                return new False();
            var build = operands[0];
            for (int i = 1; i < operands.Length; i++) {
                build = new Or(build, operands[i]);
            }
            return build;
        }

        private Predicate And(params Predicate[] operands) {
            if (operands.Length == 0)
                return new True();
            var build = operands[0];
            for (int i = 1; i < operands.Length; i++) {
                build = new And(build, operands[i]);
            }
            return build;
        }

        private Predicate Ack1(int i) {
            return Or(A(i), F(B(i)));
        }

        private Predicate Ack2(int i) {
            return Or(A(i), And(F(B(i)), F(C(i))));
        }

        private Predicate AndAlws(int i) {
            return And(A(i), G(B(i)));
        }

        private Predicate AndEvnt(int i) {
            return And(A(i), F(B(i)));
        }

        private delegate Predicate Predicator(int i);

        private Predicate[] Repeat(Predicator P, int count) {
            return Enumerable.Range(0, count).Select(i => P(i)).ToArray();
        }

        

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        [TestCase(4)]
        public void GlobalAck(int n) {
            TestUtils.PerformTest(G(And(Repeat(Ack1, n))));
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        public void Global2Ack(int n) {
            TestUtils.PerformTest(G(And(Repeat(Ack2, n))));
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        [TestCase(4)]
        public void GlobalAndEvnt(int n) {
            TestUtils.PerformTest(G(Or(Repeat(AndEvnt, n))));
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        [TestCase(4)]
        [TestCase(5)]
        public void GlobalAndAlws(int n) {
            TestUtils.PerformTest(G(Or(Repeat(AndAlws, n))));
        }
    }
}
