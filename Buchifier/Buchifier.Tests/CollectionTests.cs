﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using Utils;

namespace Buchifier.Tests {
    public class CollectionTests {
        [Test]
        public void TestSplit() {
            var set = new HashSet<int> {1, 2, 3, 4};
            foreach (var tup in set.Split()) {
                Console.WriteLine("{0}, {1}", "{" + String.Join(", ", tup.Item1) + "}", "{" + String.Join(", ", tup.Item2) + "}");
            }
        }
    }
}
