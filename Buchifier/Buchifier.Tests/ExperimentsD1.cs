﻿using Buchifier.Logic;
using NUnit.Framework;

namespace Buchifier.Tests {
    [TestFixture]
    public class ExperimentsD1 {

        private static readonly Predicate _a = new Proposition("a");
        private static readonly Predicate _b = new Proposition("b");
        private static readonly Predicate _c = new Proposition("c");

        private Predicate G(Predicate p) {
            return new Global(p);
        }

        private Predicate F(Predicate p) {
            return new Future(p);
        }

        private Predicate Or(Predicate p1, Predicate p2) {
            return new Or(p1, p2);
        }
        private Predicate And(Predicate p1, Predicate p2) {
            return new And(p1, p2);
        }

        private Predicate N(Predicate p) {
            return new Not(p);
        }

        [Test]
        public void Test01() {
            TestUtils.PerformTest(G(Or(_a, F(_b))));
        }


        [Test]
        public void Test02() {
            TestUtils.PerformTest(Or(Or(F(G(_a)), F(G(_b))), G(F(_c))));
        }

        //[Test]
        //public void Test() {
        //    TestUtils.PerformTest();
        //}

        [Test]
        public void Test03() {
            TestUtils.PerformTest(F(Or(_a, _b)));
        }

        [Test]
        public void Test04() {
            TestUtils.PerformTest(G(F(Or(_a, _b))));
        }

        [Test]
        public void Test05() {
            TestUtils.PerformTest(G(Or(_a, Or(_b, _c))));
        }

        [Test]
        public void Test06() {
            TestUtils.PerformTest(G(Or(_a, F(Or(_b, _c)))));
        }

        [Test]
        public void Test07() {
            TestUtils.PerformTest(Or(F(_a), G(_b)));
        }

        [Test]
        public void Test08() {
            TestUtils.PerformTest(G(Or(_a, F(And(_b, _c)))));
        }

        [Test]
        public void TestCheck() {
            TestUtils.PerformTest(G(Or(_b, G(F(_a)))));
        }

        [Test]
        public void Test09() {
            TestUtils.PerformTest(Or(F(G(_a)), G(F(_b))));
        }

        [Test]
        public void Test10() {
            TestUtils.PerformTest(And(G(F(Or(_a, _b))), G(F(Or(_b, _c)))));
        }

        [Test]
        public void Test11() {
            TestUtils.PerformTest(Or(And(F(F(_a)), G(N(_a))), And(G(G(N(_a))), F(_a))));
        }

        [Test]
        public void Test12() {
            TestUtils.PerformTest(And(G(F(_a)), F(G(_b))));
        }
        
        [Test]
        public void Test13() {
            TestUtils.PerformTest(Or(And(G(F(_a)), F(G(_b))), And(F(G(N(_a))), G(F(N(_b))))));
        }

        [Test]
        public void Test14() {
            TestUtils.PerformTest(And(F(G(_a)), G(F(_a))));
        }

        [Test]
        public void Test15() {
            TestUtils.PerformTest(G(And(F(_a), F(_b))));
        }

        [Test]
        public void Test16() {
            TestUtils.PerformTest(And(F(_a), F(N(_a))));
        }

        [Test]
        public void TestCheck2() {
            TestUtils.PerformTest(And(G(Or(_b, G(F(_a)))), G(Or(_c, G(F(N(_a)))))));
        }

        [Test]
        public void Test17() {
            TestUtils.PerformTest(Or(And(G(Or(_b, G(F(_a)))), G(Or(_c, G(F(N(_a)))))), Or(G(_b), G(_c))));
        }

        [Test]
        public void Test18() {
            TestUtils.PerformTest(Or(And(G(Or(_b, F(G(_a)))), G(Or(_c, F(G(N(_a)))))), Or(G(_b), G(_c))));
        }

        [Test]
        public void Test19() {
            TestUtils.PerformTest(And(Or(F(And(_b, F(G(_a)))), F(And(_c, F(G(N(_a)))))), And(F(_b), F(_c))));
        }

        [Test]
        public void Test20() {
            TestUtils.PerformTest(And(Or(F(And(_b, G(F(_a)))), F(And(_c, G(F(N(_a)))))), And(F(_b), F(_c))));
        }
    }
}
