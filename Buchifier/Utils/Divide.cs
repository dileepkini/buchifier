﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Utils {
    public static class Divide {
        public static IEnumerable<Tuple<HashSet<T>, HashSet<T>>> Split<T>(this HashSet<T> set) {
            if (set.IsEmpty()) {
                yield return Tuple.Create(new HashSet<T>(), new HashSet<T>());
                yield break;
            }
            var member = set.First();
            var remainder = new HashSet<T>(set);
            remainder.Remove(member);
            foreach(var tup in Split(remainder)) {
                var set1 = new HashSet<T>(tup.Item1) {member};
                var set2 = new HashSet<T>(tup.Item2);
                yield return Tuple.Create(set1, set2);
                tup.Item2.Add(member);
                yield return tup;
            }
        }

        private static int TernaryBit(int number, int index) {
            int q = number / ((int)Math.Pow(3, index));
            return q % 3;
        }

        public static IEnumerable<Tuple<HashSet<T>, HashSet<T>, HashSet<T>>> Triples<T>(this IEnumerable<T> set) {
            var ordered = set.ToArray();
            var initSet = new HashSet<T>();
            for (int i = 0; i < Math.Pow(3, ordered.Length); i++) {
                var triple = Tuple.Create(new HashSet<T>(), new HashSet<T>(), new HashSet<T>());
                for (int j = 0; j < ordered.Length; j++) {
                    switch (TernaryBit(i, j)) {
                        case 0:
                            triple.Item1.Add(ordered[j]);
                            break;
                        case 1:
                            triple.Item2.Add(ordered[j]);
                            break;
                        case 2:
                            triple.Item3.Add(ordered[j]);
                            break;
                        default:
                            throw new Exception();
                    }
                }
                yield return triple;
            }
            //if (set.IsEmpty()) {
            //    yield return Tuple.Create(new HashSet<T>(), new HashSet<T>(), new HashSet<T>());
            //    yield break;
            //}
            //foreach (var split in set.Split()) {
            //    foreach (var secondSplit in split.Item2.Split()) {
            //        yield return Tuple.Create(split.Item1, secondSplit.Item1, secondSplit.Item2);
            //    }
            //}
        } 
    }
}
