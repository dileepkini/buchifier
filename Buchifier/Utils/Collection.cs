﻿using System.Collections.Generic;
using System.Linq;

namespace Utils {
    public static class Collection {
        public static bool IsEmpty<T>(this IEnumerable<T> enumerable) {
            return !enumerable.Any();
        }

        public static IEnumerable<T> With<T>(this IEnumerable<T> sequence, object item) {
            return sequence.Concat(item.Yield<T>());
        }

        public static IEnumerable<T> Without<T>(this IEnumerable<T> sequence, object item) {
            return sequence.Except(item.Yield<T>());
        }

        public static IEnumerable<T> Yield<T>(this object obj) {
            yield return (T) obj;
        }

        public static IEnumerable<T> Intersection<T>(this IEnumerable<IEnumerable<T>> collection) {
            if (collection.IsEmpty())
                return Enumerable.Empty<T>();
            return collection.Skip(1).Aggregate(new HashSet<T>(collection.First()), (s1, s2) => {
                s1.IntersectWith(s2);
                return s1;
            });
        }
    }
}
