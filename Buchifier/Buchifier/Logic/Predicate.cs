﻿using System;
using System.Collections.Generic;
using System.Linq;
using Buchifier.Construction;
using Buchifier.Semantics;
using Utils;

namespace Buchifier.Logic {
    public abstract class Predicate {

        public abstract IEnumerable<T> AcceptEnumerator<T>(RecursiveEnumerator<T> rEnumerator, T seed);
        public abstract bool AcceptEvaluator<T>(Evaluator<T> evaluator, T data);

        public abstract bool IsFg();
        public abstract bool IsNext();

        public bool ContainsNext() {
            if (this is Next)
                return true;
            return ArgumentPredicates().Any(predicate => predicate.ContainsNext());
        }


        // push all nexts to propositions, (push inside all operators including negation)
        public abstract Predicate PushNext(int n);

        public abstract Predicate ReplaceNext(Dictionary<Proposition, Tuple<int, Proposition>> replacement, int n);

        public abstract bool Evaluate(Guess g, Symbol s);
        public abstract Predicate Unfold(Guess g, Symbol s);

        protected abstract IEnumerable<Predicate> ArgumentPredicates();
        public abstract HashSet<Predicate> GetBasis();
        public abstract IEnumerable<Predicate> TopLevelFG();

        public abstract IEnumerable<Proposition> LocalPropositions();
        public abstract HashSet<Proposition> GetPropositions();

        public bool TopGuessIsBest(Guess topGuess, Symbol symbol, IEnumerable<Predicate> topLevel) {
            var trueF = new HashSet<Future>(topLevel.OfType<Future>().Where(f => !topGuess.InTau(f)));
            // if moving a G from tau (true) to kap (don't care) yields a guess that is good enough then ignore the original guess
            foreach (var g in topGuess.EnumerateTau<Global>()) {
                var newGuess = new Guess(topGuess.EnumerateTau<object>().Except(g.Yield<object>()), topGuess.EnumerateUps<object>());
                if (Evaluate(newGuess, symbol))
                    return false;
            }

            // if moving a F from !tau (true) to tau (don't care) yields a guess that is good enough then ignore the original guess
            foreach (var f in trueF) {
                var newGuess = new Guess(topGuess.EnumerateTau<object>().Concat(f.Yield<object>()), topGuess.EnumerateUps<object>().Except(f.Yield<object>()));
                if (Evaluate(newGuess, symbol))
                    return false;
            }
            return true;
        }

        

        //Assumption: guess IsGlobalValid w.r.t symbol
        public bool GuessIsBestInitial(Guess guess, HashSet<Predicate> basis, HashSet<Predicate> topLevel, Symbol symbol) {
            var maximal = new HashSet<Predicate>(topLevel.Where(p => topLevel.All(top => top.Equals(p) || !top.ConsistsOf(p))));
            if (guess.EnumerateTau<Global>().Where(maximal.Contains).Select(g => new Guess(guess.EnumerateTau<object>().Except(g.Yield<object>()), guess.EnumerateUps<object>())).Any(newGuess => Evaluate(newGuess, symbol))) {
                return false;
            }
            if (guess.EnumerateUps<Global>().Where(maximal.Contains).Any())
                return false;
            // if a maximal F is in kappa, it is clearly not needed
            if (maximal.OfType<Future>().Any(f => !guess.InTau(f) && !guess.InUps(f)))
                return false;
            if (guess.EnumerateUps<Future>().Where(maximal.Contains).Select(
                        f => new Guess(guess.EnumerateTau<object>().With(f), guess.EnumerateUps<object>().Without(f)))
                    .Any(newGuess => Evaluate(newGuess, symbol)))
                return false;


            var trueF = new HashSet<Future>(basis.OfType<Future>().Where(f => !guess.InTau(f)));
            var ultTrueG = new HashSet<Global>(guess.EnumerateTau<Global>().Concat(guess.EnumerateUps<Global>()));
            var ultTruePred = new HashSet<Predicate>(trueF);
            ultTruePred.UnionWith(ultTrueG);
            var truePredicates = new HashSet<Predicate>(trueF.Union<Predicate>(guess.EnumerateTau<Global>()));
            var trueMaximals = new HashSet<Predicate>(truePredicates.Intersect(maximal));

            if (ultTruePred.Any(predicate => !topLevel.Contains(predicate) && !trueMaximals.Any(max => max.ConsistsOf(predicate))))
                return false;

            return true;

        }

        public bool GuessIsBest(Guess guess, HashSet<Predicate> basis, HashSet<Predicate> topLevel, Symbol symbol) {
            var maximal = new HashSet<Predicate>(topLevel.Where(p => topLevel.All(top => top.Equals(p) || !top.ConsistsOf(p))));
            //return true;
            var trueF = new HashSet<Future>(basis.OfType<Future>().Where(f => !guess.InTau(f)));
            var ultTrueG = new HashSet<Global>(guess.EnumerateTau<Global>().Concat(guess.EnumerateUps<Global>()));
            var ultTruePred = new HashSet<Predicate>(trueF);
                ultTruePred.UnionWith(ultTrueG);
            var truePredicates = new HashSet<Predicate>(trueF.Union<Predicate>(guess.EnumerateTau<Global>()));
            var trueMaximals = new HashSet<Predicate>(truePredicates.Intersect(maximal));

            // justifying the presence of maximal G's in tau // !basis.Any(b => !b.Equals(g) && b.ConsistsOf(g))
            foreach (var g in guess.EnumerateTau<Global>().Where(maximal.Contains)) { 
                var newGuess = new Guess(guess.EnumerateTau<object>().Except(g.Yield<object>()), guess.EnumerateUps<object>());
                if (newGuess.IsGlobalGuessValid(symbol) && Evaluate(newGuess, symbol))
                    return false;
            }

            // justifying the presence of of maximal G's in ups // !basis.Any(b => !b.Equals(g) && b.ConsistsOf(g))
            foreach (var g in guess.EnumerateUps<Global>().Where(maximal.Contains)) { 
                var newGuess = new Guess(guess.EnumerateTau<object>(), guess.EnumerateUps<object>().Except(g.Yield<object>()));
                if (newGuess.IsGlobalGuessValid(symbol) && Evaluate(newGuess, symbol))
                    return false;
            }

            // justifying the presence of maximal F's not in tau (A maximal F should be in \tau or \ups)
            foreach (var f in trueF.Where(maximal.Contains)) { 
                var newGuess = new Guess(guess.EnumerateTau<object>().Concat(f.Yield<object>()), guess.EnumerateUps<object>().Except(f.Yield<object>()));
                if (newGuess.IsGlobalGuessValid(symbol) && Evaluate(newGuess, symbol))
                    return false;
                if (!guess.InUps(f)) {
                    newGuess = new Guess(guess.EnumerateTau<object>(), guess.EnumerateUps<object>().Concat(f.Yield<object>())); // kappa -> ups
                    if (newGuess.IsGlobalGuessValid(symbol) && Evaluate(newGuess, symbol))
                        return false;
                }
            }
            if(ultTruePred.Any(predicate => !topLevel.Contains(predicate) && !trueMaximals.Any(max => max.ConsistsOf(predicate)))) {
                return false;
            }
            return true;
        }

        public abstract bool ConsistsOf(Predicate predicate);

        public abstract Predicate GetPropositionalRemainder(Guess guess);

        public bool EvaluateValidity(Guess guess) {
            return GetPropositionalRemainder(guess) is True;
        }

        public abstract string ToTex();
    }
}
