﻿using System;
using System.Collections.Generic;
using Buchifier.Construction;
using Buchifier.Semantics;
using Utils;

namespace Buchifier.Logic {
    public class Proposition : Predicate {

        public string Name { get; private set; }

        public Proposition(string name) {
            Name = name;
        }

        protected bool Equals(Proposition other) {
            return string.Equals(Name, other.Name);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Proposition)obj);
        }

        public override int GetHashCode() {
            return (Name != null ? Name.GetHashCode() : 0);
        }


        public override bool AcceptEvaluator<T>(Evaluator<T> evaluator, T data) {
            return evaluator.Evaluate(this, data);
        }

        public override bool IsFg() {
            return true;
        }

        public override bool IsNext() {
            return true;
        }

        public override Predicate PushNext(int n) {
            Predicate ret = this;
            for (int i = 0; i < n; i++)
                ret = new Next(ret);
            return ret;
        }

        public override Predicate ReplaceNext(Dictionary<Proposition, Tuple<int, Proposition>> replacement, int n) {
            var abstractProp = n==0 ? this : new Proposition("_" + Name + "(" + n + ")");
            if (!replacement.ContainsKey(abstractProp))
                replacement[abstractProp] = Tuple.Create(n, this);
            return abstractProp;
        }

        public override bool Evaluate(Guess g, Symbol s) {
            return s.IsTrue(this);
        }

        public override Predicate Unfold(Guess g, Symbol s) {
            return (s.IsTrue(this) ? (Predicate) new True() : new False());
        }

        public override HashSet<Proposition> GetPropositions() {
            return new HashSet<Proposition>{this};
        }

        public override IEnumerable<T> AcceptEnumerator<T>(RecursiveEnumerator<T> rEnumerator, T seed) {
            return rEnumerator.Enumerate(this, seed);
        }

        public override HashSet<Predicate> GetBasis() {
            return new HashSet<Predicate>();
        }

        public override bool ConsistsOf(Predicate predicate) {
            return Equals(predicate);
        }

        public override IEnumerable<Predicate> TopLevelFG() {
            yield break;
        }

        public override Predicate GetPropositionalRemainder(Guess guess) {
            return this;
        }

        public override IEnumerable<Proposition> LocalPropositions() {
            yield return this;
        }

        protected override IEnumerable<Predicate> ArgumentPredicates() {
            yield break;
        }

        public override string ToString() {
            return Name;
        }

        public override string ToTex() {
            return Name;
        }
    }
}
