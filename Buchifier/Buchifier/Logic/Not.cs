﻿using System;
using System.Collections.Generic;
using Buchifier.Construction;
using Buchifier.Semantics;

namespace Buchifier.Logic {
    public class Not : Predicate {

        protected bool Equals(Not other) {
            return Equals(_predicate, other._predicate);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Not) obj);
        }

        public override int GetHashCode() {
            return (_predicate != null ? _predicate.GetHashCode() : 0);
        }

        private readonly Predicate _predicate;
        public Predicate Arg { get { return _predicate; } }

        public Not(Predicate predicate) {
            _predicate = predicate;
        }

        public override bool AcceptEvaluator<T>(Evaluator<T> evaluator, T data) {
            return evaluator.Evaluate(this, data);
        }

        public override bool IsFg() {
            return _predicate is Proposition;
        }

        public override bool IsNext() {
            return _predicate is Proposition;
        }

        public override Predicate PushNext(int n) {
            return new Not(_predicate.PushNext(n));
        }

        public override Predicate ReplaceNext(Dictionary<Proposition, Tuple<int, Proposition>> replacement, int n) {
            return new Not(_predicate.ReplaceNext(replacement, n));
        }

        public override bool Evaluate(Guess g, Symbol s) {
            return !_predicate.Evaluate(g, s);
        }

        public override Predicate Unfold(Guess g, Symbol s) {
            return new Not(_predicate.Unfold(g, s));
        }

        public override HashSet<Proposition> GetPropositions() {
            return _predicate.GetPropositions();
        }

        public override IEnumerable<T> AcceptEnumerator<T>(RecursiveEnumerator<T> rEnumerator, T seed) {
            return rEnumerator.Enumerate(this, seed);
        }

        public override HashSet<Predicate> GetBasis() {
            return _predicate.GetBasis();
        }

        public override bool ConsistsOf(Predicate predicate) {
            return Equals(predicate) || _predicate.ConsistsOf(predicate);
        }

        public override IEnumerable<Predicate> TopLevelFG() {
            yield break;
        }

        public override Predicate GetPropositionalRemainder(Guess guess) {
            var rem = _predicate.GetPropositionalRemainder(guess);
            if (rem is True)
                return new False();
            if (rem is False)
                return new True();
            return new Not(rem);
        }

        public override string ToTex() {
            return "\\neg " + _predicate.ToTex();
        }

        public override IEnumerable<Proposition> LocalPropositions() {
            return _predicate.LocalPropositions();
        }

        protected override IEnumerable<Predicate> ArgumentPredicates() {
            yield return _predicate;
        }

        public override string ToString() {
            return "!" + _predicate;
        }
    }
}
