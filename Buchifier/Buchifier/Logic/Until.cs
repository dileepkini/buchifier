﻿using System;
using System.Collections.Generic;
using Buchifier.Construction;
using Buchifier.Semantics;

namespace Buchifier.Logic {
    public class Until : Predicate {

        protected bool Equals(Until other) {
            return Equals(_predicate1, other._predicate1) && Equals(_predicate2, other._predicate2);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Until) obj);
        }

        public override int GetHashCode() {
            unchecked {
                return ((_predicate1 != null ? _predicate1.GetHashCode() : 0)*397) ^ (_predicate2 != null ? _predicate2.GetHashCode() : 0);
            }
        }

        private readonly Predicate _predicate1;
        private readonly Predicate _predicate2;

        public Predicate Arg1 { get { return _predicate1; } }
        public Predicate Arg2 { get { return _predicate2; } }

        public Until(Predicate predicate1, Predicate predicate2) {
            _predicate1 = predicate1;
            _predicate2 = predicate2;
        }

        public override bool AcceptEvaluator<T>(Evaluator<T> evaluator, T data) {
            return evaluator.Evaluate(this, data);
        }

        public override bool IsFg() {
            return false;
        }

        public override bool IsNext() {
            return false;
        }

        public override Predicate PushNext(int n) {
            return new Until(_predicate1.PushNext(n), _predicate2.PushNext(n));
        }

        public override Predicate ReplaceNext(Dictionary<Proposition, Tuple<int, Proposition>> replacement, int n) {
            return new Until(_predicate1.ReplaceNext(replacement, n), _predicate2.ReplaceNext(replacement, n));
        }

        public override bool Evaluate(Guess g, Symbol s) {
            throw new NotImplementedException();
        }

        public override Predicate Unfold(Guess g, Symbol s) {
            var expand = new And(new Future(_predicate2),
                new Or(new Global(_predicate1), new Or(_predicate2, new And(_predicate1, new Next(this)))));
            return expand.Unfold(g, s);
        }

        public override HashSet<Proposition> GetPropositions() {
            var ret = new HashSet<Proposition>();
            ret.UnionWith(_predicate1.GetPropositions());
            ret.UnionWith(_predicate2.GetPropositions());
            return ret;
        }

        public override IEnumerable<T> AcceptEnumerator<T>(RecursiveEnumerator<T> rEnumerator, T seed) {
            return rEnumerator.Enumerate(this, seed);
        }

        public override HashSet<Predicate> GetBasis() {
            var ret = new HashSet<Predicate>();
            ret.UnionWith(_predicate1.GetBasis());
            ret.UnionWith(_predicate2.GetBasis());
            return ret;
        }

        public override bool ConsistsOf(Predicate predicate) {
            return Equals(predicate) || _predicate1.ConsistsOf(predicate) || _predicate2.ConsistsOf(predicate);
        }

        public override IEnumerable<Predicate> TopLevelFG() {
            yield break;
        }

        public override Predicate GetPropositionalRemainder(Guess guess) {
            throw new NotImplementedException();
        }

        public override IEnumerable<Proposition> LocalPropositions() {
            yield break;
        }

        protected override IEnumerable<Predicate> ArgumentPredicates() {
            yield return _predicate1;
            yield return _predicate2;
        }

        public override string ToString() {
            return _predicate1 + " U " + _predicate2;
        }

        public override string ToTex() {
            return _predicate1.ToTex() + " \\unt " + _predicate2.ToTex();
        }
    }
}
