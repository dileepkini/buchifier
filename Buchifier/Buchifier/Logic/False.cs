﻿using System;
using System.Collections.Generic;
using Buchifier.Construction;
using Buchifier.Semantics;

namespace Buchifier.Logic {
    public class False : Predicate {
        public override bool AcceptEvaluator<T>(Evaluator<T> evaluator, T data) {
            return evaluator.Evaluate(this, data);
        }

        public override bool IsFg() {
            return true;
        }

        public override bool IsNext() {
            return true;
        }

        public override Predicate PushNext(int n) {
            return this;
        }

        public override Predicate ReplaceNext(Dictionary<Proposition, Tuple<int, Proposition>> replacement, int n) {
            if (n == 0) return this;
            throw new Exception();
        }

        public override bool Evaluate(Guess g, Symbol s) {
            return false;
        }

        public override Predicate Unfold(Guess g, Symbol s) {
            return this;
        }

        public override HashSet<Proposition> GetPropositions() {
            return new HashSet<Proposition>();
        }

        public override IEnumerable<T> AcceptEnumerator<T>(RecursiveEnumerator<T> rEnumerator, T seed) {
            return rEnumerator.Enumerate(this, seed);
        }

        public override HashSet<Predicate> GetBasis() {
            return new HashSet<Predicate>();
        }

        public override bool ConsistsOf(Predicate predicate) {
            return false;
        }

        public override IEnumerable<Predicate> TopLevelFG() {
            yield break;
        }

        public override Predicate GetPropositionalRemainder(Guess guess) {
            return this;
        }

        public override string ToTex() {
            return "\\mathbf{ff}";
        }

        public override IEnumerable<Proposition> LocalPropositions() {
            yield break;
        }

        protected override IEnumerable<Predicate> ArgumentPredicates() {
            yield break;
        }
    }
}
