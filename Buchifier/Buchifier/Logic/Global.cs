﻿using System;
using System.Collections.Generic;
using Buchifier.Construction;
using Buchifier.Semantics;

namespace Buchifier.Logic {
    public class Global : Predicate {

        protected bool Equals(Global other) {
            return Equals(Predicate, other.Predicate);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Global) obj);
        }

        public override int GetHashCode() {
            return (Predicate != null ? Predicate.GetHashCode() : 0);
        }

        internal readonly Predicate Predicate;

        public Global(Predicate predicate) {
            Predicate = predicate;
        }

        public override bool AcceptEvaluator<T>(Evaluator<T> evaluator, T data) {
            return evaluator.Evaluate(this, data);
        }

        public override bool IsFg() {
            return Predicate.IsFg();
        }

        public override bool IsNext() {
            return false;
        }

        public override Predicate PushNext(int n) {
            return new Global(Predicate.PushNext(n));
        }

        public override Predicate ReplaceNext(Dictionary<Proposition, Tuple<int, Proposition>> replacement, int n) {
            return new Global(Predicate.ReplaceNext(replacement, n));
        }

        public override bool Evaluate(Guess g, Symbol s) {
            return g.InTau(this);
        }

        public override Predicate Unfold(Guess g, Symbol s) {
            return (g.InTau(this) ? (Predicate) new True() : new False());
        }

        public override HashSet<Proposition> GetPropositions() {
            return Predicate.GetPropositions();
        }

        public override IEnumerable<T> AcceptEnumerator<T>(RecursiveEnumerator<T> rEnumerator, T seed) {
            return rEnumerator.Enumerate(this, seed);
        }

        public override HashSet<Predicate> GetBasis() {
            var ret = Predicate.GetBasis();
            ret.Add(this);
            return ret;
        }

        public override bool ConsistsOf(Predicate predicate) {
            return Equals(predicate) || Predicate.ConsistsOf(predicate);
        }

        public override IEnumerable<Predicate> TopLevelFG() {
            yield return this;
        }

        public override Predicate GetPropositionalRemainder(Guess guess) {
            return (guess.InTau(this) ? (Predicate) new True() : new False());
        }

        public override string ToTex() {
            return "\\alws " + Predicate.ToTex();
        }

        public override IEnumerable<Proposition> LocalPropositions() {
            yield break;
        }

        protected override IEnumerable<Predicate> ArgumentPredicates() {
            yield return Predicate;
        }

        public override string ToString() {
            return "G " + Predicate;
        }
    }
}
