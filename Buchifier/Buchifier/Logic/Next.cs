﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Buchifier.Construction;
using Buchifier.Semantics;

namespace Buchifier.Logic {
    public class Next : Predicate {
        private readonly Predicate _predicate;

        public Next(Predicate predicate) {
            _predicate = predicate;
        }

        public override bool AcceptEvaluator<T>(Evaluator<T> evaluator, T data) {
            return evaluator.Evaluate(this, data);
        }

        public override bool IsFg() {
            return _predicate.IsNext();
        }

        public override bool IsNext() {
            return _predicate.IsNext();
        }

        public override Predicate PushNext(int n) {
            return _predicate.PushNext(n + 1);
        }

        public override Predicate ReplaceNext(Dictionary<Proposition, Tuple<int, Proposition>> replacement, int n) {
            return _predicate.ReplaceNext(replacement, n + 1);
        }

        public override bool Evaluate(Guess g, Symbol s) {
            throw new NotImplementedException();
        }

        public override Predicate Unfold(Guess g, Symbol s) {
            return _predicate;
        }

        public override HashSet<Proposition> GetPropositions() {
            return _predicate.GetPropositions();
        }

        public override IEnumerable<T> AcceptEnumerator<T>(RecursiveEnumerator<T> rEnumerator, T seed) {
            return rEnumerator.Enumerate(this, seed);
        }

        public override HashSet<Predicate> GetBasis() {
            throw new NotImplementedException();
        }

        public override bool ConsistsOf(Predicate predicate) {
            return Equals(predicate) || _predicate.ConsistsOf(predicate);
        }

        public override IEnumerable<Predicate> TopLevelFG() {
            yield break;
        }

        public override Predicate GetPropositionalRemainder(Guess guess) {
            throw new NotImplementedException();
        }

        public override string ToTex() {
            return "\\nxt " + _predicate.ToTex();
        }

        public override IEnumerable<Proposition> LocalPropositions() {
            yield break;
        }

        protected override IEnumerable<Predicate> ArgumentPredicates() {
            yield return _predicate;
        }

        public override string ToString() {
            return "X " + _predicate;
        }
    }
}
