﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buchifier.Logic {
    /// <summary>
    /// Evaluate methods should return true if evaluation was possible, otherwise false. If successful The return value should be sent through the out boolean
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class Evaluator<T> {

        public bool Evaluate(Predicate predicate, T data) {
            return Evaluate((dynamic)predicate, data);
        }

        public abstract bool Evaluate(Proposition proposition, T data);

        public virtual bool Evaluate(And and, T data) {
            return Evaluate(and.Arg1, data) && Evaluate(and.Arg2, data);
        }

        public bool Evaluate(True p, T data) {
            return true;
        }

        public abstract bool Evaluate(Until until, T data);

        public bool Evaluate(False p, T data) {
            return false;
        }

        public abstract bool Evaluate(Future future, T data);

        public abstract bool Evaluate(Global global, T data);

        public bool Evaluate(Next next, T data) {
            throw new NotImplementedException();
        }

        public abstract bool Evaluate(Not not, T data);

        public virtual bool Evaluate(Or or, T data) {
            return Evaluate(or.Arg1, data) || Evaluate(or.Arg2, data);
        }
    }
}
