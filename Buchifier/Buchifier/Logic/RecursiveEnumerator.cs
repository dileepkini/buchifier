﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utils;

namespace Buchifier.Logic {
    public abstract class RecursiveEnumerator<T> {
        public abstract IEnumerable<T> Enumerate(Predicate predicate, T seed);
        public abstract IEnumerable<T> Enumerate(Until until, T seed);
        public abstract IEnumerable<T> Enumerate(Future future, T seed);
        public abstract IEnumerable<T> Enumerate(Global global, T seed);
        public virtual IEnumerable<T> Enumerate(Next next, T seed) {
            throw new NotImplementedException();
        }

        public IEnumerable<T> Enumerate(And and, T seed) {
            return Enumerate(and.Arg1, seed).SelectMany(first => Enumerate(and.Arg2, first));
        }
        public virtual IEnumerable<T> Enumerate(Or or, T seed) {
            return Enumerate(or.Arg1, seed).Concat(Enumerate(or.Arg2, seed));
        }

        public virtual IEnumerable<T> Enumerate(Not not, T seed) {
            yield return seed;
        }
        public virtual IEnumerable<T> Enumerate(Proposition proposition, T seed) {
            yield return seed;
        }

        public IEnumerable<T> Enumerate(True p, T seed) {
            yield return seed;
        }
        public IEnumerable<T> Enumerate(False p, T seed) {
            yield break;
        }
    }

    public class NegationNotLiteralException : Exception {}
}
