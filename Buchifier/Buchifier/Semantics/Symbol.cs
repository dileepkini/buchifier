﻿using System;
using System.Collections.Generic;
using System.Linq;
using BDDSharp;
using Buchifier.Logic;
using Utils;

namespace Buchifier.Semantics {
    public class Symbol {
        internal readonly HashSet<Proposition> Assignment;

        public Symbol(HashSet<Proposition> assignment) {
            Assignment = assignment;
        }

        public Symbol Project(IEnumerable<Proposition> propositions) {
            return new Symbol(new HashSet<Proposition>(propositions.Intersect(Assignment)));
        }

        public bool IsTrue(Proposition p) {
            return Assignment.Contains(p);
        }

        public bool IsFalse() {
            return Assignment.IsEmpty();
        }

        public BDDNode ToBddNode(BDDManager manager, Dictionary<int, Proposition> propositions, HashSet<Proposition> relevantPropositions) {
            
            var ret = manager.One;
            for (int i = 0; i < manager.N; i++) {
                if (!relevantPropositions.Contains(propositions[i]))
                    continue;
                var bddProp = manager.Create(i, manager.One, manager.Zero);
                var cur = Assignment.Contains(propositions[i])
                    ? bddProp
                    : manager.Negate(bddProp);
                cur = manager.Reduce(cur);
                ret = manager.And(ret, cur);
                ret = manager.Reduce(ret);
            }
            return ret;
        }

        public static IEnumerable<Symbol> EnumerateSymbols(Proposition[] propositions) {
            for (int i = 0; i < (1 << propositions.Length); i++) {
                var assign = new HashSet<Proposition>();
                for (int j = 0; j < propositions.Length; j++) {
                    if ((i >> j)%2 == 1)
                        assign.Add(propositions[j]);
                }
                yield return new Symbol(assign);
            }
        }

        protected bool Equals(Symbol other) {
            return Assignment.SetEquals(other.Assignment);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Symbol)obj);
        }

        public override int GetHashCode() {
            return (Assignment != null ? Assignment.OrderIndependenHashCode() : 0);
        }

        public override string ToString() {
            return "{" + String.Join(", ", Assignment.OrderBy(prop => prop.Name)) + "}";
        }

        public Dictionary<Proposition, bool> ToDictionary(IEnumerable<Proposition> propositions) {
            return propositions.ToDictionary(prop => prop, prop => Assignment.Contains(prop));
        }

        public IEnumerable<Symbol> Extension(Proposition[] irrelevantPropositions) {
            return
                EnumerateSymbols(irrelevantPropositions).Select(ext => new Symbol(new HashSet<Proposition>(ext.Assignment.Union(Assignment))));
        }
    }
}
