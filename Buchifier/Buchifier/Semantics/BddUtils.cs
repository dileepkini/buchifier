﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BDDSharp;
using Buchifier.Logic;

namespace Buchifier.Semantics {
    public static class BddUtils {
        public static string ToFormulaString(this BDDNode v, Dictionary<int, Proposition> prop) {
            if (v.IsZero)
                return "ff";
            if (v.IsOne)
                return "tt";
            if (v.High.IsZero) {
                if(v.Low.IsOne)
                    return "!" + prop[v.Index];
                return String.Format("!{0}.{1}", prop[v.Index], v.Low.ToFormulaString(prop));
            }
            if (v.High.IsOne) {
                if (v.Low.IsZero)
                    return prop[v.Index].ToString();
                return String.Format("({0}+!{0}.{1})", prop[v.Index], v.Low.ToFormulaString(prop));
            }
            if (v.Low.IsZero) {
                return String.Format("{0}.{1}", prop[v.Index], v.High.ToFormulaString(prop));
            }
            if (v.Low.IsOne) {
                return String.Format("({0}.{1}+!{0})", prop[v.Index], v.High.ToFormulaString(prop));
            }
            return String.Format("({0}.{1}+!{0}.{2})", prop[v.Index], v.High.ToFormulaString(prop),
                v.Low.ToFormulaString(prop));
        }


    }
}
