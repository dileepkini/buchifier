﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Buchifier.Semantics {
    public class Circuit {

        private readonly HashSet<Symbol> _symbols;

        public Circuit(IEnumerable<Symbol> symbols) {
            _symbols = new HashSet<Symbol>(symbols.Distinct());
        }

        public Circuit(IEnumerable<Circuit> circuits) {
            _symbols = new HashSet<Symbol>(circuits.SelectMany(circuit => circuit._symbols).Distinct());
        }

        public bool Evaluate(Symbol symbol) {
            return _symbols.Any(set => set.Equals(symbol));
        }

        public override string ToString() {
            return "[" + String.Join(",", _symbols) + "]";
        }

        public string ToString(bool isFinal) {
            if (!isFinal)
                return ToString();
            return "[" + String.Join(",", _symbols) + " *]";
        }

        public int Size {
            get { return _symbols.Count; }
        }

        protected bool Equals(Circuit other) {
            return _symbols.SetEquals(other._symbols);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Circuit)obj);
        }

        public override int GetHashCode() {
            return (_symbols != null ? _symbols.GetHashCode() : 0);
        }
    }
}
