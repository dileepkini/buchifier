﻿using System;
using System.IO;
using Antlr4.Runtime;
using Buchifier.Logic;

namespace Buchifier.Grammar {
    public static class ExprGenerator {
        public static Predicate Generate(string str) {
            var input = new AntlrInputStream(str);
            var lexer = new LTLLexer(input);
            var tokens = new CommonTokenStream(lexer);
            var parser = new LTLParser(tokens);
            var tree = parser.expr();
            var visitor = new ExprVisitor();
            return visitor.Visit(tree);
        }

        public static Predicate GenerateFromFile(string filename) {
            var input = new AntlrFileStream(filename);
            var lexer = new LTLLexer(input);
            var tokens = new CommonTokenStream(lexer);
            var parser = new LTLParser(tokens);
            var tree = parser.expr();
            var visitor = new ExprVisitor();
            return visitor.Visit(tree);
        }
    }
}
