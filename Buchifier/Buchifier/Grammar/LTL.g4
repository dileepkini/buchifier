grammar LTL;

/*
 * Parser Rules
 */

expr	:	expr 'U' expr			# Until
		|	op=('G'|'F'|'X') expr	# AlwsEvntNxt
		|	expr op=('&'|'|') expr	# AndOr
		|	'!' expr				# Not
		|	PROP					# Prop
		|	TRUE					# True
		|	'(' expr ')'			# Paren
		;



/*
 * Lexer Rules
 */

UNT  : 'U';
ALW  : 'G';
EVN  : 'F';
NXT  : 'X';


AND  : '&';
OR   : '|';
TRUE : 'T';
NOT  : '!';
PROP : [a-zA-Z][a-zA-Z0-9]*;

WS
	:	' ' -> channel(HIDDEN)
	;
