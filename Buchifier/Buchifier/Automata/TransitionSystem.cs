﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using BDDSharp;
using Buchifier.Construction;
using Buchifier.Logic;
using Buchifier.Semantics;
using Utils;

namespace Buchifier.Automata {
    public abstract class TransitionSystem<T> where T : Transition {
        public readonly HashSet<Location> States;
        protected readonly HashSet<Location> FinalStates;
        public readonly HashSet<Location> InitialStates;
        public ILookup<Location, T> Transitions;
        public ILookup<Location, T> ReverseLookup;


        public TransitionSystem(HashSet<Location> states, HashSet<T> transitions, HashSet<Location> initialStates, HashSet<Location> finalStates) {
            States = states;
            FinalStates = finalStates;
            InitialStates = initialStates;
            Transitions = transitions.ToLookup(trans => trans.BeginState);
            ReverseLookup = transitions.ToLookup(trans => trans.EndState);
        }

        // remove deadlocks and those that cannot reach final
        public void Prune() {
            // do reverse BFS to remove deadlocking states
            bool changed;
            do {
                changed = false;

                #region deadlock removal (_states, _finalStates modified)
                var deadlocked = States.Where(state => !Transitions.Contains(state)).ToArray();
                while (!deadlocked.IsEmpty()) {
                    changed = true;
                    States.ExceptWith(deadlocked);
                    deadlocked =
                        deadlocked.SelectMany(state => ReverseLookup[state].Select(trans => trans.BeginState))
                            .Distinct()
                            .Where(
                                state =>
                                    States.Contains(state) &&
                                    Transitions[state].All(trans => !States.Contains(trans.EndState)))
                            .ToArray();
                }
                FinalStates.IntersectWith(States);
                #endregion

                #region final not reachable (_states modified)
                var threshold = FinalStates.ToArray();
                var canReach = new HashSet<Location>();
                while (!threshold.IsEmpty()) {
                    canReach.UnionWith(threshold);
                    threshold = threshold.SelectMany(state => ReverseLookup[state].Select(trans => trans.BeginState))
                        .Distinct()
                        .Where(state => States.Contains(state) && !canReach.Contains(state)).ToArray();
                }
                if (!States.SetEquals(canReach))
                    changed = true;
                States.IntersectWith(canReach);
                #endregion

                var validTransitions =
                    Transitions.SelectMany(g => g).Where(trans => States.Contains(trans.EndState)).ToArray();
                Transitions = validTransitions.ToLookup(t => t.BeginState);
                ReverseLookup = validTransitions.ToLookup(t => t.EndState);
            } while (changed);
            InitialStates.IntersectWith(States);
        }

        public int StateCount {
            get { return States.Count; }
        }

        public int TransitionCount {
            get {
                return Transitions.SelectMany(g => g).Count();
            }
        }

        public int FinalTransitionCount {
            get { return Transitions.SelectMany(t => t).Count(t => t.IsFinal); }
        }


        public abstract string DotString();

        protected XElement XmlNode(object obj) {
            if (obj is Location) {
                var node = obj as Location;
                return new XElement("state", new XAttribute("id", node), new XAttribute("initial", node.IsInitial()));
            }
            throw new NotImplementedException();
        }

        protected string DotNode(object obj) {
            if (obj is Location) {
                var node = obj as Location;
                var fill = (node.IsFinal() && !node.IsInitial()
                    ? ",style=\"filled,rounded\",fillcolor=skyblue"
                    : (node.IsFinal()
                        ? ",style=\"filled\",fillcolor=skyblue"
                        : (node.IsInitial() ? "" : ",style=\"rounded\"")));
                return String.Format("\"{0}\"[label=\"{0}\"{1}]", node, fill);
            }
            return obj.ToString();
        }

        private string DotTransition(object node1, object node2, Circuit symbol, bool isFinal) {
            return String.Format("\"{0}\" -> \"{1}\" [label=\"{2}\"]", node1, node2, symbol.ToString(isFinal));
        }
    }

    public class BddTransitionSystem : TransitionSystem<BddTransition> {

        private readonly Dictionary<int, Proposition> _indexedPropositions;

        public BddTransitionSystem(HashSet<Location> states, HashSet<BddTransition> transitions, HashSet<Location> initialStates, HashSet<Location> finalStates, Dictionary<int, Proposition> propositions)
            : base(states, transitions, initialStates, finalStates) {
            _indexedPropositions = propositions;
        }

        

        public int CountConnectedComponents {
            get { return new HashSet<Guess>(FinalStates.Select(loc => loc.GetGuess())).Count; }
        }

        private string DotTransition(BddTransition trans, bool noBdd) {
            //var label = new List<string>();
            //if(!input.IsZero)
                //label.Add(input.ToFormulaString(_indexedPropositions));
            //if(!buchi.IsZero)
            //    label.Add("[" + buchi.ToFormulaString(_indexedPropositions) + "]");
            //if (!initial.IsZero)
            //    label.Add("<" + initial.ToFormulaString(_indexedPropositions) + ">");
            var irrelevantPropsitions = _indexedPropositions.Values.Except(trans.Relevant).ToArray();
            var label = (noBdd
                ? "[" + String.Join(",", trans.InputSymbols.SelectMany(s => s.Extension(irrelevantPropsitions)).Select(s => s.ToString())) + "]"
                : trans.Input.ToFormulaString(_indexedPropositions));
            return String.Format("\"{0}\" -> \"{1}\" [label=\"{2}\"]", trans.BeginState, trans.EndState, label);
        }

        private XElement XmlTransition(BddTransition transition, bool noBdd) {
            var irrelevantPropsitions = _indexedPropositions.Values.Except(transition.Relevant).ToArray();
            var label = (noBdd
                ? "[" + String.Join(",", transition.InputSymbols.SelectMany(s => s.Extension(irrelevantPropsitions)).Select(s => s.ToString())) + "]"
                : transition.Input.ToFormulaString(_indexedPropositions));
            return new XElement("transition",
                new XAttribute("begin", transition.BeginState), new XAttribute("end", transition.EndState),
                new XAttribute("label", label));
        }

        private XElement XmlFinalTransition(BddTransition transition, bool noBdd) {
            var label = transition.BuchiLabel(noBdd, _indexedPropositions);
            return new XElement("finalTransition",
                new XAttribute("begin", transition.BeginState),
                new XAttribute("label", label));
        }




        public string DotString(bool noBdd) {
            return String.Format("digraph {{ node [shape=rectangle]; {0};\n{1} }}", String.Join(";\n", States.Select(DotNode)),
                String.Join(";\n", Transitions.SelectMany(g => g).Select(t => DotTransition(t, noBdd))));
        }

        public string AcceptanceCondition(bool noBdd) {
            var sb = new StringBuilder();
            if (States.IsEmpty())
                return "Number of final transitions: 0";
            if (States.First() is BufferedState) {
                
                sb.AppendLine(String.Format("Number of final transitions : {0}\n", FinalTransitionCount));
                foreach (var trans in Transitions.SelectMany(t => t).Where(t => t.IsFinal))
                    sb.AppendLine(trans.BeginState + " -> " + trans.BuchiLabel(noBdd, _indexedPropositions));
            } else if (States.First() is State) {
                sb.AppendLine(String.Format("Number of final transitions : {0}\n", CountConnectedComponents));
                foreach (
                    var trans in
                        Transitions.SelectMany(t => t)
                            .Where(t => t.IsFinal)
                            .ToLookup(trans => trans.BeginState.GetGuess())
                            .Select(g => g.First())) {
                    sb.AppendLine(trans.BeginState + " -> " + trans.BuchiLabel(noBdd, _indexedPropositions));
                }
            }
            else
                throw new NotImplementedException();
            return sb.ToString();
        }

        public override string DotString() {
            return DotString(false);
            //throw new NotImplementedException();
        }

        public XElement ToXml(bool noBdd) {
            var states = new XElement("states", States.Select(XmlNode));
            var transitions = new XElement("transitions",
                Transitions.SelectMany(g => g).Select(t => XmlTransition(t, noBdd)));
            var final = new XElement("finalTransitions",
                Transitions.SelectMany(g => g).Where(t => t.IsFinal).Select(t => XmlFinalTransition(t, noBdd)));
            return new XElement("automaton", states, transitions, final);
        }
    }

    public class ConcreteTransitionSystem : TransitionSystem<ConcreteTransition> {
        // state -> symbol -> Transitions
        private Dictionary<Location, ILookup<Symbol, ConcreteTransition>> _sortedTransitions;

        public Dictionary<Location, ILookup<Symbol, ConcreteTransition>> SortedTransitions {
            get {
                if (_sortedTransitions != null)
                    return _sortedTransitions;
                _sortedTransitions = Transitions.ToDictionary(g => g.Key, g => g.ToLookup(trans => trans.Input));
                return _sortedTransitions;
            }
        }

        public ConcreteTransitionSystem(HashSet<Location> states, HashSet<ConcreteTransition> transitions,
            HashSet<Location> initialStates, HashSet<Location> finalStates)
            : base(states, transitions, initialStates, finalStates) {
        }

        private bool Simulates(Location smallState, Location bigState) {
            bool incomingTransitions =
                ReverseLookup[smallState].All(
                    trans => (trans.BeginState.Equals(smallState)) ||
                        Transitions[trans.BeginState].Any(
                            transSimilar =>
                                trans.Input.Equals(transSimilar.Input) && transSimilar.EndState.Equals(bigState)));
            if (!incomingTransitions)
                return false;
            bool goodTransitions = Transitions[smallState].All(trans => Transitions[bigState].Any(transSimilar =>
                            trans.Input.Equals(transSimilar.Input) &&
                            (((Equals(trans.EndState, smallState)) && (Equals(transSimilar.EndState, bigState))) ||
                             ((!(Equals(trans.EndState, smallState))) && (Equals(transSimilar.EndState, trans.EndState))))));
            if (!goodTransitions)
                return false;
            bool goesToRemove =
                Transitions[bigState].Where(trans => Equals(trans.EndState, smallState))
                    .All(
                        trans =>
                            Transitions[bigState].Any(
                                transSimilar =>
                                    trans.Input.Equals(transSimilar.Input) && Equals(transSimilar.EndState, bigState)));
            return goesToRemove;
        }

        public void MergeStates() {
            bool changed;
            do {
                changed = false;
                // 0. consider only non-final states
                foreach (var remove in States.Except(FinalStates)) {
                    // 1. if remove is initial then compare only with initial states
                    var candidates = (InitialStates.Contains(remove) ? InitialStates : States);
                    if (candidates.Where(state => !state.Equals(remove)).Any(state => Simulates(remove, state))) {
                        States.Remove(remove);
                        var validTransitions =
                            Transitions.SelectMany(g => g)
                                .Where(trans => States.Contains(trans.BeginState) && States.Contains(trans.EndState))
                                .ToArray();
                        Transitions = validTransitions.ToLookup(t => t.BeginState);
                        ReverseLookup = validTransitions.ToLookup(t => t.EndState);
                        changed = true;
                        break;
                    }
                }
            } while (changed);
            InitialStates.IntersectWith(States);
        }

        public BddTransitionSystem MergeTransitions(BDDManager manager, Dictionary<int, Proposition> propositions) {
            
            var transitions = Transitions.SelectMany(g => g).ToArray();
            var combined =
                transitions.ToLookup(trans => Tuple.Create(trans.BeginState, trans.EndState))
                    .Select(
                        g =>
                            new BddTransition(g.Key.Item1, g.Key.Item2, 
                                g.Select(trans => trans.Input), 
                                g.Where(trans => trans.IsFinal).Select(trans => trans.Input),
                                manager, propositions, 
                                new HashSet<Proposition>(g.Key.Item1.GetRelevantPropositions())
                                ))
                    .ToArray();
            return new BddTransitionSystem(States, new HashSet<BddTransition>(combined), InitialStates,FinalStates, propositions);
        }

        private string DotTransition(object node1, object node2, Symbol symbol, bool isFinal) {
            //if (!initial.IsZero)
            //    label.Add("<" + initial.ToFormulaString(_indexedPropositions) + ">");

            return String.Format("\"{0}\" -> \"{1}\" [label=\"{2}\"]", node1, node2, (isFinal ? "[" + symbol + "]" : symbol.ToString()));
        }

        public override string DotString() {
            return String.Format("digraph {{ {0};\n{1} }}", String.Join(";\n", States.Select(DotNode)),
                String.Join(";\n",
                    Transitions.SelectMany(g => g)
                        .Select(t => DotTransition(t.BeginState, t.EndState, t.Input, t.IsFinal))));
        }
    }
    
}
