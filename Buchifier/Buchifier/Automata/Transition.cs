﻿using System;
using System.Collections.Generic;
using System.Linq;
using BDDSharp;
using Buchifier.Construction;
using Buchifier.Logic;
using Buchifier.Semantics;

namespace Buchifier.Automata {
    public class Transition {
        public readonly Location BeginState;
        public readonly Location EndState;

        public readonly bool IsFinal;
        //public readonly bool IsInitial;
        public Transition(Location beginState, Location endState, bool isFinal) {
            BeginState = beginState;
            EndState = endState;
            IsFinal = isFinal;
            //IsInitial = isInitial;
        }
    }

    public class ConcreteTransition : Transition {
        public readonly Symbol Input;

        public ConcreteTransition(Location beginState, Location endState, Symbol inputSymbol, bool isFinal) : base(beginState, endState, isFinal) {
            Input = inputSymbol;
        }
    }

    public class BddTransition : Transition {
        public readonly BDDNode Input;
        public readonly BDDNode Buchi;
        public readonly Symbol[] InputSymbols;
        public readonly Symbol[] BuchiSymbols;
        public readonly HashSet<Proposition> Relevant;

        public BddTransition(Location beginState, Location endState, IEnumerable<Symbol> inputSymbols, IEnumerable<Symbol> buchiSymbols, BDDManager manager, Dictionary<int, Proposition> propositions, HashSet<Proposition> relevantPropositions)
            : base(beginState, endState, buchiSymbols.Any()) {
            Relevant = new HashSet<Proposition>(relevantPropositions);
            InputSymbols = inputSymbols.ToArray();
            BuchiSymbols = buchiSymbols.ToArray();
            Input = BddForInputs(manager, InputSymbols, propositions, relevantPropositions);
            Buchi = BddForInputs(manager, BuchiSymbols, propositions, relevantPropositions);
        }

        private BDDNode BddForInputs(BDDManager manager, IEnumerable<Symbol> symbols, Dictionary<int, Proposition> propositions, HashSet<Proposition> relevantPropositions) {
            var cur = manager.Zero;
            foreach (var symbol in symbols) {
                var symbolNode = symbol.ToBddNode(manager, propositions, relevantPropositions);
                cur = manager.Or(symbolNode, cur);
                cur = manager.Reduce(cur);
            }
            return cur;
        }

        public string BuchiLabel(bool noBdd, Dictionary<int, Proposition> indexedPropositions) {
            var irrelevant = indexedPropositions.Values.Except(Relevant).ToArray();
            return (noBdd
                ? "[" + String.Join(",", BuchiSymbols.SelectMany(s => s.Extension(irrelevant)).Select(s => s.ToString())) + "]"
                : Buchi.ToFormulaString(indexedPropositions));
        }
    }
}
