﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Buchifier.Logic;

namespace Buchifier.Construction {
    public abstract class Location {
        public abstract bool IsInitial();
        public abstract bool IsFinal();
        public abstract IEnumerable<Proposition> GetRelevantPropositions();
        public abstract Guess GetGuess();
    }
}
