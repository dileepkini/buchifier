﻿using System;
using System.Collections.Generic;
using Antlr4.Runtime.Dfa;
using Buchifier.Logic;
using Utils;

namespace Buchifier.Construction.Enumeration {
    public class EnumerateNow : RecursiveEnumerator<HybridState> {

        private static readonly EnumerateNow Singleton = new EnumerateNow();
        private EnumerateNow() { }

        public static IEnumerable<HybridState> CallEnumerate(Predicate predicate, HybridState seed) {
            return EnumerateNow.Singleton.Enumerate(predicate, seed);
        }

        public override IEnumerable<HybridState> Enumerate(Predicate predicate, HybridState seed) {
            if (EvaluateNow.CallEvaluate(predicate, seed))
                return seed.Yield<HybridState>();
            return Enumerate((dynamic)predicate, seed);
        }

        public override IEnumerable<HybridState> Enumerate(Until until, HybridState seed) {
            if (EvaluateNow.CallEvaluate(until.Arg2, seed)) {
                yield return seed;
                yield break;
            }
            var withUntil = seed.MoveToUps(until);
            if (EvaluateNow.CallEvaluate(until.Arg2, withUntil))
                yield return withUntil;
            else {
                foreach (var delay in Enumerate(until.Arg1, withUntil))
                    foreach (var ret in EnumerateF.CallEnumerate(until.Arg2, delay))
                        yield return ret;
                foreach (var ret in Enumerate(until.Arg2, withUntil))
                    yield return ret;
            }
        }

        public override IEnumerable<HybridState> Enumerate(Future future, HybridState seed) {
            return EnumerateF.CallEnumerate(future.Predicate, seed.MoveToUps(future));
        }

        public override IEnumerable<HybridState> Enumerate(Global global, HybridState seed) {
            return EnumerateG.CallEnumerate(global.Predicate, seed.MoveToTau(global));
        }

        public override IEnumerable<HybridState> Enumerate(Not not, HybridState seed) {
            if(!(not.Arg is Proposition))
                throw new NegationNotLiteralException();
            var prop = not.Arg as Proposition;
            bool b;
            if (seed.Literals.TryGetValue(prop, out b)) {
                if (!b) yield return seed;
                yield break;
            }
            yield return new HybridState(seed, prop, false);
        }

        public override IEnumerable<HybridState> Enumerate(Proposition proposition, HybridState seed) {
            bool b;
            if (seed.Literals.TryGetValue(proposition, out b)) {
                if (b) yield return seed;
                yield break;
            }
            yield return new HybridState(seed, proposition, true);
        }
    }
}
