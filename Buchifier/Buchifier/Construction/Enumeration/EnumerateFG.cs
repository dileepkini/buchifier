﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Buchifier.Logic;
using Utils;

namespace Buchifier.Construction.Enumeration {
    public class EnumerateFG : RecursiveEnumerator<HybridState> {

        private static readonly EnumerateFG Singleton = new EnumerateFG();
        private EnumerateFG() { }

        public static IEnumerable<HybridState> CallEnumerate(Predicate predicate, HybridState seed) {
            return Singleton.Enumerate(predicate, seed);
        }

        public override IEnumerable<HybridState> Enumerate(Predicate predicate, HybridState seed) {
            //if (EvaluateFG.CallEvaluate(predicate, seed))
            //    return seed.Yield<HybridState>();
            return Enumerate((dynamic) predicate, seed);
        }

        public override IEnumerable<HybridState> Enumerate(Until until, HybridState seed) {
            throw new NotImplementedException();
        }

        public override IEnumerable<HybridState> Enumerate(Future future, HybridState seed) {
            return EnumerateGF.CallEnumerate(future.Predicate, seed.MoveToKap(future));
        }

        public override IEnumerable<HybridState> Enumerate(Global global, HybridState seed) {
            return Enumerate(global.Predicate, seed.MoveToUps(global));
        }

        public override IEnumerable<HybridState> Enumerate(Or or, HybridState seed) {
            foreach (var eventual in EnumerateF.CallEnumerate(or, seed)) {
                foreach (var always1 in Enumerate(or.Arg1, eventual))
                    yield return always1;
                foreach (var always2 in Enumerate(or.Arg2, eventual))
                    yield return always2;
                foreach (var infinite1 in EnumerateGF.CallEnumerate(or.Arg1, eventual))
                    foreach (var infinite in EnumerateGF.CallEnumerate(or.Arg2, infinite1))
                        yield return infinite;
            }
        }
    }
}
