﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Buchifier.Logic;
using Utils;

namespace Buchifier.Construction.Enumeration {
    public class EnumerateGF : RecursiveEnumerator<HybridState> {

        private static readonly EnumerateGF Singleton = new EnumerateGF();
        private EnumerateGF() { }

        public static IEnumerable<HybridState> CallEnumerate(Predicate predicate, HybridState hybridState) {
            return Singleton.Enumerate(predicate, hybridState);
        }

        public override IEnumerable<HybridState> Enumerate(Predicate predicate, HybridState seed) {
            //if (EvaluateGF.CallEvaluate(predicate, seed))
            //    return seed.Yield<HybridState>();
            return Enumerate((dynamic)predicate, seed);
        }

        public override IEnumerable<HybridState> Enumerate(Until until, HybridState seed) {
            throw new NotImplementedException();
        }

        public override IEnumerable<HybridState> Enumerate(Future future, HybridState seed) {
            return Enumerate(future.Predicate, seed.MoveToKap(future));
        }

        public override IEnumerable<HybridState> Enumerate(Global global, HybridState seed) {
            return EnumerateFG.CallEnumerate(global.Predicate, seed.MoveToUps(global));
        }
    }
}
