﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Buchifier.Construction.Evaluation;
using Buchifier.Logic;
using Utils;

namespace Buchifier.Construction.Enumeration {
    public class EnumerateG : RecursiveEnumerator<HybridState> {

        private static readonly EnumerateG Singleton = new EnumerateG();

        private EnumerateG() { }

        public static IEnumerable<HybridState> CallEnumerate(Predicate predicate, HybridState hybridState) {
            return Singleton.Enumerate(predicate, hybridState);
        }

        public override IEnumerable<HybridState> Enumerate(Predicate predicate, HybridState seed) {
            //if (EvaluateG.Evaluate(predicate, seed))
            //    return seed.Yield<HybridState>();
            return Enumerate((dynamic) predicate, seed);
        }

        public override IEnumerable<HybridState> Enumerate(Until until, HybridState seed) {
            throw new NotImplementedException();
        }

        public override IEnumerable<HybridState> Enumerate(Future future, HybridState seed) {
            return EnumerateGF.CallEnumerate(future.Predicate, seed.MoveToKap(future));
        }

        public override IEnumerable<HybridState> Enumerate(Global global, HybridState seed) {
            return Enumerate(global.Predicate, seed.MoveToTau(global));
        }

        public override IEnumerable<HybridState> Enumerate(Or or, HybridState seed) {
            foreach (var now in EnumerateNow.CallEnumerate(or, seed)) {
                foreach (var always1 in EnumerateFG.CallEnumerate(or.Arg1, now))
                    yield return always1;
                foreach (var always2 in EnumerateFG.CallEnumerate(or.Arg2, now))
                    yield return always2;
                foreach(var infinite1 in EnumerateGF.CallEnumerate(or.Arg1, now))
                    foreach (var infinite in EnumerateGF.CallEnumerate(or.Arg2, infinite1))
                        yield return infinite;
            }
        }

        public override IEnumerable<HybridState> Enumerate(Not not, HybridState seed) {
            return EnumerateNow.CallEnumerate(not, seed);
        }

        public override IEnumerable<HybridState> Enumerate(Proposition proposition, HybridState seed) {
            return EnumerateNow.CallEnumerate(proposition, seed);
        }
    }
}
