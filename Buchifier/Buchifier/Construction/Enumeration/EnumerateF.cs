﻿using System;
using System.Collections.Generic;
using System.Linq;
using Buchifier.Construction.Evaluation;
using Buchifier.Logic;
using Utils;

namespace Buchifier.Construction.Enumeration {
    public class EnumerateF : RecursiveEnumerator<HybridState> {

        private static readonly EnumerateF Singleton = new EnumerateF();
        private EnumerateF() {}

        public static IEnumerable<HybridState> CallEnumerate(Predicate predicate, HybridState seed) {
            return Singleton.Enumerate(predicate, seed);
        }

        public override IEnumerable<HybridState> Enumerate(Predicate predicate, HybridState seed) {
            //if (EvaluateF.Evaluate(predicate, seed))
            //    return seed.Yield<HybridState>();
            return Enumerate((dynamic)predicate, seed);
        }

        public override IEnumerable<HybridState> Enumerate(Until until, HybridState seed) {
            foreach (var second in Enumerate(until.Arg2, seed.MoveToTau(until))) {
                yield return second;
                foreach (var ret in Enumerate(until.Arg1, second))
                    yield return ret;
            }
        }

        public override IEnumerable<HybridState> Enumerate(Future future, HybridState seed) {
            return Enumerate(future.Predicate, seed.MoveToUps(future));
        }

        public override IEnumerable<HybridState> Enumerate(Global global, HybridState seed) {
            return EnumerateFG.CallEnumerate(global.Predicate, seed.MoveToUps(global));
        }
    }
}
