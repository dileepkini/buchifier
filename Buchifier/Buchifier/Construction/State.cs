﻿using System;
using System.Collections.Generic;
using System.Linq;
using Buchifier.Logic;
using Buchifier.Semantics;
using Utils;

namespace Buchifier.Construction {
    public class State : Location {
        private readonly HashSet<Predicate> _basis;
        private readonly Dictionary<int, Future> _order;
        private readonly Guess _guess;
        private readonly int _counter;
        private readonly Predicate _predicate;
        //private readonly Proposition[] _propositions;

        public State(HashSet<Predicate> basis, Guess guess, int counter, Predicate predicate, Dictionary<int, Future> order, bool isInitial) {
            _basis = basis;
            _guess = guess;
            _counter = counter;
            _order = order;
            _isInitial = isInitial;
            _predicate = predicate;
        }

        public HashSet<Symbol> InitialInputs { get; set; }

        public override bool IsFinal() {
            return _guess.IsUpsEmpty(); //(_counter == 0)
        }

        public override bool IsInitial() {
            return _isInitial;
        }

        private readonly bool _isInitial;


        public bool IsValid(Symbol symbol) {
            return _guess.EnumerateTau<Global>().All(g => g.Predicate.Evaluate(_guess, symbol));
        }

        private Proposition[] _localDepthPropositions;
        // for calculating depth, only G's in tau, F's in ups matter (if non-final, otherwise depth is decided by the _order[_counter])
        public IEnumerable<Proposition> LocalDepthPropositions() {
            if (_localDepthPropositions != null) return _localDepthPropositions;
            var initial = (_isInitial ? _predicate.LocalPropositions() : Enumerable.Empty<Proposition>());
            var global = _guess.EnumerateTau<Global>().SelectMany(g => g.Predicate.LocalPropositions());
            var final = (IsFinal() & _counter > 0 ? _order[_counter].Predicate.LocalPropositions(): Enumerable.Empty<Proposition>());
            var future = _guess.EnumerateUps<Future>().SelectMany(f => f.Predicate.LocalPropositions());
            return _localDepthPropositions = initial.Concat(global).Concat(final).Concat(future).Distinct().ToArray();
        }

        public IEnumerable<Proposition> LocalRelevantPropositions() {
            // look at all the local propositions of: globals in \tau, (isFinal? the counter numbered future in \kap : all f's in \ups)
            var initial = (_isInitial ? _predicate.LocalPropositions() : Enumerable.Empty<Proposition>());
            var globals = _guess.EnumerateTau<Global>().Concat(_guess.EnumerateUps<Global>()).SelectMany(global => global.Predicate.LocalPropositions());
            var final = (IsFinal() && _counter > 0 ? _order[_counter].Predicate.LocalPropositions() : Enumerable.Empty<Proposition>());
            var futures = _guess.EnumerateUps<Future>().SelectMany(future => future.Predicate.LocalPropositions());
            return initial.Concat(globals).Concat(final).Concat(futures).Distinct();
        }

        private Proposition[] _localInvariantPropositions;

        public IEnumerable<Proposition> LocalInvariantPropositions() {
            if (_localInvariantPropositions != null) return _localInvariantPropositions;
            var initial = (_isInitial ? _predicate.LocalPropositions() : Enumerable.Empty<Proposition>());
            var global = _guess.EnumerateTau<Global>().SelectMany(g => g.Predicate.LocalPropositions());
            return _localInvariantPropositions = initial.Concat(global).ToArray();
        }

        private Proposition[] _localProgressPropositions;

        public IEnumerable<Proposition> LocalProgressPropositions() {
            if (!IsFinal())
                throw new Exception();
            if (_localProgressPropositions != null) return _localProgressPropositions;
            var initial = (_isInitial ? _predicate.LocalPropositions() : Enumerable.Empty<Proposition>());
            var final = (_counter > 0 ? _order[_counter].Predicate.LocalPropositions(): Enumerable.Empty<Proposition>());
            return _localProgressPropositions = initial.Concat(final).ToArray();
        }

        public override IEnumerable<Proposition> GetRelevantPropositions() {
            var guessPropositions = _guess.GetFgPropositions(_basis);
            return (_isInitial ? _predicate.LocalPropositions().Concat(guessPropositions).Distinct() : guessPropositions);
        }

        public override Guess GetGuess() {
            return _guess;
        }

        public IEnumerable<Proposition> GetAllPropositions() {
            return _predicate.GetPropositions();
        }

        public bool HasSuccessorOn(Symbol symbol, HashSet<Predicate> maximal) {
            return _guess.FGSuccessors().Any(guess => _guess.IsGlobalGuessValid(symbol) && ConditionB(maximal, guess, symbol));
        }

        public IEnumerable<Tuple<Symbol, State>> Successors(HashSet<Predicate> maximal) {

            // the set of possible successors on an input depends on the set of F-subformulae that evaluate true on that input
            var eqClasses = new Dictionary<SymbolSignature, List<Symbol>>();
            #region create Equivalence Classes
            var allSymbols = _isInitial ? InitialInputs : Symbol.EnumerateSymbols(GetRelevantPropositions().ToArray());
            foreach (var symbol in allSymbols) {
                //condition (A)
                if (!_guess.IsGlobalGuessValid(symbol))
                    continue;

                var piUpsEmpty = _guess.EnumerateUps<Future>().IsEmpty();
                var signature =
                    new SymbolSignature(
                        (piUpsEmpty? (_counter > 0 && _order[_counter].Predicate.Evaluate(_guess, symbol) ? _order[_counter].Yield<Future>() : Enumerable.Empty<Future>()) :
                        _guess.EnumerateUps<Future>().Where(f => f.Predicate.Evaluate(_guess, symbol))
                        ));
                List<Symbol> list;
                if (eqClasses.TryGetValue(signature, out list))
                    list.Add(symbol);
                else
                    eqClasses.Add(signature, new List<Symbol> {symbol});
            }
            #endregion

            foreach (var guess in _guess.FGSuccessors()) {
                foreach (var group in eqClasses.Values) {
                    #region generates (input,state) pairs that are successors of this state
                    Symbol symbol = group[0];
                    
                    if (!ConditionB(maximal, guess, symbol)) continue;

                    //Condition D
                    var nextCounter = NextCounter(guess, symbol);

                    foreach (var sym in group) {
                        yield return Tuple.Create(sym, new State(_basis, guess, nextCounter, _predicate, _order, false));
                    } 
                    #endregion
                }
            }
        }

        IEnumerable<int> AllCounters() {
            return _order.Where(kvp => !_guess.InTau(kvp.Key)).Select(kvp => kvp.Key);
        }


        private int NextCounter(Guess guess, Symbol symbol) {
            int nextCounter;
            if (!_guess.IsUpsEmpty())
                nextCounter = _counter;
            else {
                var kappaF = _order.Where(kvp => !guess.InTau(kvp.Value)).ToArray();
                if (_counter == 0)
                    nextCounter = 0;
                else {
                    //kappaF cannot be empty if _counter is non-zero
                    var first = kappaF.Select(kvp => kvp.Key).Min();
                    var nextF = kappaF.Where(kvp => kvp.Key > _counter).ToArray();
                    if (_order[_counter].Predicate.Evaluate(_guess, symbol))
                        nextCounter = nextF.IsEmpty() ? first : nextF.Select(kvp => kvp.Key).Min();
                    else
                        nextCounter = _counter;
                }
            }
            return nextCounter;
        }

        private bool ConditionB(HashSet<Predicate> maximal, Guess guess, Symbol symbol) {
            if (
                _guess.EnumerateUps<Future>()
                    .Any(f => !guess.InUps(f) && !f.Predicate.Evaluate(_guess, symbol) && !f.Predicate.EvaluateValidity(guess)))
                return false;
            if (
                maximal.OfType<Future>()
                    .Any(f => guess.InUps(f) && (f.Predicate.Evaluate(_guess, symbol) || f.Predicate.EvaluateValidity(guess))))
                return false;
            return true;
        }

        public bool GuessCounterEquals(State other) {
            return Equals(_guess, other._guess) && _counter == other._counter;
        }

        protected bool Equals(State other) {
            return Equals(_guess, other._guess) && _counter == other._counter && (_isInitial == other._isInitial);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((State)obj);
        }

        public override int GetHashCode() {
            unchecked {
                int hashCode = (_guess != null ? _guess.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ _counter;
                return hashCode;
            }
        }

        private string _toString;

        public override string ToString() {
            return (_toString ?? (_toString = String.Format("{0}, ({1}){2}", _guess, _counter, (_isInitial ? "*": ""))));
        }

        public bool Progress(Symbol symbol) {
            return IsFinal() && (_counter == 0 || _order[_counter].Predicate.Evaluate(_guess, symbol));
        }
    }
}
