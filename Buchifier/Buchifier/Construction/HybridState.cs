﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Cryptography;
using Buchifier.Construction.Enumeration;
using Buchifier.Logic;
using Buchifier.Semantics;
using Utils;

namespace Buchifier.Construction {
    public class HybridState : Location {

        private readonly Predicate _predicate;
        private readonly HashSet<Predicate> _basis; 
        private readonly Guess _untils;
        private readonly Guess _guess;
        private readonly Dictionary<int, Future> _order;
        private bool _isInitial;
        private readonly HashSet<Predicate> _maximalFG;
        private int _counter;

        private Dictionary<Proposition, bool> _literals;
        public HashSet<Symbol> InitialInputs { get; set; }
        

        protected bool Equals(HybridState other) {
            return _untils.Equals(other._untils) && _guess.Equals(other._guess) &&
                   _predicate.Equals(other._predicate) && _isInitial == other._isInitial
                   && _counter == other._counter;
            //    && 
            //    (new HashSet<Proposition>(_literals.Keys).SetEquals((other._literals.Keys))) && 
            //_literals.Keys.All(key => _literals[key].Equals(other._literals[key]));
        }
        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((HybridState) obj);
        }
        public override int GetHashCode() {
            unchecked {
                int hashCode = (_untils != null ? _untils.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (_guess != null ? _guess.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (_predicate != null ? _predicate.GetHashCode() : 0);
                return hashCode;
            }
        }

        private string _toString;
        public override string ToString() {
            if (_toString != null) return _toString;
            return _toString = String.Format("[{0}], [{1}], ({2}){3}", _untils, _guess, _counter, (_isInitial ? "*": ""));
            //String.Join(",", _literals.Select(kvp => String.Format("({0},{1})", kvp.Key, (kvp.Value? "T":"⊥"))).OrderBy(s => s)));
        }

        public Predicate InputPredicate { get { return _predicate; } }
        public Guess Guess { get { return _guess;  } }
        public Guess Untils { get { return _untils; } }
        public Dictionary<Proposition, bool> Literals { get { return _literals; } }

        public HybridState(Predicate predicate, HashSet<Predicate> basis, Guess untils, Guess guess, bool isInitial, int counter, Dictionary<int, Future> order, Dictionary<Proposition, bool> literals) {
            _predicate = predicate;
            _basis = basis;
            _guess = guess;
            _untils = untils;
            _counter = counter;
            _order = order;
            _literals = literals;
            var topLevel = new HashSet<Predicate>(predicate.TopLevelFG());
            _maximalFG = new HashSet<Predicate>(topLevel.Where(p => topLevel.All(top => top.Equals(p) || !top.ConsistsOf(p))));
        }
        public HybridState(HybridState seed, Proposition proposition, bool b) {
            _predicate = seed._predicate;
            _basis = seed._basis;
            _untils = seed._untils;
            _guess = seed._guess;
            _order = seed._order;
            _counter = seed._counter;
            _literals = new Dictionary<Proposition, bool>(seed._literals){{proposition, b}};
            _maximalFG = seed._maximalFG;
        }
        private HybridState(HybridState seed, Guess untils, Guess fgGuess, int? counter = null) {
            _predicate = seed._predicate;
            _basis = seed._basis;
            _untils = untils;
            _guess = fgGuess;
            _order = seed._order;
            _counter = (counter ?? seed._counter);
            _literals = seed._literals;
            _maximalFG = seed._maximalFG;
        }

        public HybridState(HybridState seed, Symbol symbol) {
            _predicate = seed._predicate;
            _basis = seed._basis;
            _untils = seed._untils;
            _guess = seed._guess;
            _order = seed._order;
            _counter = seed._counter;
            _literals = symbol.ToDictionary(seed.GetAllPropositions());
            _isInitial = seed._isInitial;
            _maximalFG = seed._maximalFG;
        }

        public HybridState MoveToUps(Future future) {
            return new HybridState(this, _untils, _guess.MoveToUps(future));
        }
        public HybridState MoveToUps(Global global) {
            return new HybridState(this, _untils, _guess.MoveToUps(global));
        }
        public HybridState MoveToKap(Future future) {
            return new HybridState(this, _untils, _guess.MoveToKap(future));
        }
        public HybridState MoveToTau(Global global) {
            return new HybridState(this, _untils, _guess.MoveToTau(global));
        }
        public HybridState MoveToTau(Until until) {
            return new HybridState(this, _untils.MoveToTau(until), _guess);
        }
        public HybridState MoveToUps(Until until) {
            return new HybridState(this, _untils.MoveToUps(until), _guess);
        }

        public bool HasSuccessorOn(Symbol symbol) {
            if (!_guess.IsGlobalGuessValid(symbol)) return false;
            _literals = symbol.ToDictionary(GetRelevantPropositions());
            var untilsToKap = UntilsToKap();
            foreach(var fgGuess in _guess.FGSuccessors())
                foreach (var untilGuess in _untils.UntilSuccessors(untilsToKap)) {
                    if (!ConditionB(_maximalFG, fgGuess, symbol))
                        continue;
                    var nextCounter = NextCounter(fgGuess, symbol);
                    var nextState = new HybridState(this, untilGuess, fgGuess, nextCounter);
                    if (AreUntilsValid(nextState))
                        return true;
                }
            return false;
            //return (from fgGuess in _guess.FGSuccessors() from untilGuess in _untils.UntilSuccessors(untilsToKap) where ConditionB(_maximalFG, fgGuess, symbol) let nextCounter = NextCounter(fgGuess, symbol) select new HybridState(this, untilGuess, fgGuess, nextCounter)).Any(AreUntilsValid);
        }

        private HashSet<Until> UntilsToKap() {
            return new HashSet<Until>(_untils.EnumerateUps<Until>().Where(until => EvaluateNow.CallEvaluate(until.Arg2, this)));
        } 

        public IEnumerable<Tuple<Symbol, HybridState>> Successors() {
            var allSymbols = _isInitial ? InitialInputs : Symbol.EnumerateSymbols(GetRelevantPropositions().ToArray());
            foreach (var symbol in allSymbols) {
                _literals = symbol.ToDictionary(GetRelevantPropositions());
                var untilsToKap = UntilsToKap();
                foreach (var fgGuess in _guess.FGSuccessors()) {
                    foreach (var untilGuess in _untils.UntilSuccessors(untilsToKap)) {
                        //condition(A)
                        if (!_guess.IsGlobalGuessValid(symbol))
                            continue;

                        //condition(B)
                        if (!ConditionB(_maximalFG, fgGuess, symbol))
                            continue;

                        //condition(D)
                        var nextCounter = NextCounter(fgGuess, symbol);
                        var nextState = new HybridState(this, untilGuess, fgGuess, nextCounter);

                        // Condition(U)

                        if (!AreUntilsValid(nextState))
                            continue;

                        yield return Tuple.Create(symbol, nextState);
                    }
                }
            }
        }

        private bool ConditionB(HashSet<Predicate> maximal, Guess guess, Symbol symbol) {
            if (
                _guess.EnumerateUps<Future>()
                    .Any(f => !guess.InUps(f) && !f.Predicate.Evaluate(_guess, symbol) && !f.Predicate.EvaluateValidity(guess)))
                return false;
            if (
                maximal.OfType<Future>()
                    .Any(f => guess.InUps(f) && (f.Predicate.Evaluate(_guess, symbol) || f.Predicate.EvaluateValidity(guess))))
                return false;
            return true;
        }

        public bool UntilsLocalConsistent() {
            return _untils.EnumerateUps<Until>().All(until =>
                EvaluateNow.CallEvaluate(until.Arg1, this) || EvaluateNow.CallEvaluate(until.Arg2, this));
        }

        public bool UntilsTemporalConsistent(HybridState nextState) {
            return _untils.EnumerateUps<Until>()
                    .All(until => (EvaluateNow.CallEvaluate(until.Arg2, this) || EvaluateNow.CallEvaluate(until, nextState)));
        }

        public bool AreUntilsValid(HybridState nextState) {
            return UntilsLocalConsistent() && UntilsTemporalConsistent(nextState);
        }

        public void AssignCounter() {
            _counter = MinCounter();
        }
        private int MinCounter() {
            var kapF = new HashSet<Future>(_basis.OfType<Future>().Where(fut => !_guess.InTau(fut) && !_guess.InUps(fut)));
            return (kapF.IsEmpty() ? 0 : _order.Where(kvp => kapF.Contains(kvp.Value)).Select(kvp => kvp.Key).Min());
        }

        private int NextCounter(Guess guess, Symbol symbol) {
            int nextCounter;
            if (!_guess.IsUpsEmpty())
                nextCounter = _counter;
            else {
                var kappaF = _order.Where(kvp => !guess.InTau(kvp.Value)).ToArray();
                if (_counter == 0)
                    nextCounter = 0;
                else {
                    //kappaF cannot be empty if _counter is non-zero
                    var first = kappaF.Select(kvp => kvp.Key).Min();
                    var nextF = kappaF.Where(kvp => kvp.Key > _counter).ToArray();
                    if (_order[_counter].Predicate.Evaluate(_guess, symbol))
                        nextCounter = nextF.IsEmpty() ? first : nextF.Select(kvp => kvp.Key).Min();
                    else
                        nextCounter = _counter;
                }
            }
            return nextCounter;
        }

        public void SetInitial(bool isInitial) {
            _isInitial = isInitial;
        }
        public override bool IsInitial() {
            return _isInitial;
        }
        public override bool IsFinal() {
            return _guess.IsUpsEmpty() && _untils.IsUpsEmpty() && _untils.EnumerateTau<object>().IsEmpty();
        }

        private Proposition[] _propositions;
        public Proposition[] GetAllPropositions() {
            if (_propositions != null) return _propositions;
            return _propositions = _predicate.GetPropositions().ToArray();
        }

        public override IEnumerable<Proposition> GetRelevantPropositions() {
            var guessPropositions = _guess.GetFgPropositions(_basis);
            var untilPropositions = _untils.GetUntilPropositions();
            var allPropositions = guessPropositions.Concat(untilPropositions);
            return (_isInitial ? _predicate.LocalPropositions().Concat(allPropositions).Distinct() : allPropositions);
        }

        public override Guess GetGuess() {
            throw new NotImplementedException();
        }


        public bool GuessCounterEquals(HybridState other) {
            return _untils.Equals(other._untils) && _guess.Equals(other._guess) && _counter == other._counter;
        }

        public bool Progress(Symbol symbol) {
            return IsFinal() && (_counter == 0 || _order[_counter].Predicate.Evaluate(_guess, symbol));
        }
    }
}
