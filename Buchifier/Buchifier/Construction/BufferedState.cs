﻿using System;
using System.Collections.Generic;
using System.Linq;
using Buchifier.Automata;
using Buchifier.Logic;
using Buchifier.Semantics;
using Utils;

namespace Buchifier.Construction {
    public class BufferedState : Location {
        public override bool IsInitial() {
            return State.IsInitial() && Buffer.IsEmpty();
        }

        public override bool IsFinal() {
            return State.IsFinal();
        }

        public override IEnumerable<Proposition> GetRelevantPropositions() {
            //throw new NotImplementedException();
            return State.GetRelevantPropositions().Select(prop => AbstractToConcreteMap[prop].Item2);
        }

        public override Guess GetGuess() {
            return State.GetGuess();
        }

        // underlying abstract state
        public readonly State State;

        // concrete propositions
        public readonly Proposition[] Propositions;

        // abstract propositions
        public readonly Proposition[] AbstractPropositions;

        // maps abstract propositions to original propositions
        public readonly Dictionary<Proposition, Tuple<int, Proposition>> AbstractToConcreteMap;

        // sequence of sets, representing which propositions are relevant
        private List<HashSet<Proposition>> _relevant;

        //

        private List<HashSet<Proposition>> Relevant() {
            var relevant = new List<HashSet<Proposition>>();
            // look at all Locally relvant propositons
            var propositions = State.LocalRelevantPropositions().ToArray();  //LocalRelevantPropositions().ToArray();

            // determine the depth
            var depth = propositions.Select(prop => AbstractToConcreteMap[prop].Item1).DefaultIfEmpty(0).Max();

            for(int i = 0; i < depth; i++)
                relevant.Add(new HashSet<Proposition>());

            foreach (var prop in propositions) {
                for (int i = AbstractToConcreteMap[prop].Item1; i < depth; i++)
                    relevant[i].Add(AbstractToConcreteMap[prop].Item2);
            }
            return relevant;
        }

        // sequence of symbols over the original propositions
        public readonly List<Symbol> Buffer;

        // indicates the accuracy of the buffer, should be as long as the buffer
        public readonly List<HashSet<Proposition>> Accurate;

        private int SelectDepth(IEnumerable<Proposition> abstractPropositions) {
            return abstractPropositions.Select(prop => AbstractToConcreteMap[prop].Item1).DefaultIfEmpty(0).Max();
        }

        private int? _depth;
        private int Depth {
            get {
                if (_depth != null)
                    return _depth.Value;
                _depth = SelectDepth(State.LocalDepthPropositions());
                return _depth.Value;
            }
        }

        private int? _invarianceDepth;
        private int InvarianceDepth {
            get {
                if (_invarianceDepth != null)
                    return _invarianceDepth.Value;
                _invarianceDepth = SelectDepth(State.LocalInvariantPropositions());
                return _invarianceDepth.Value;
            }
        }

        private int? _progressDepth;
        private int ProgressDepth {
            get {
                if (_progressDepth != null)
                    return _progressDepth.Value;
                _progressDepth = SelectDepth(State.LocalProgressPropositions());
                return _progressDepth.Value;
            }
        }

        public BufferedState(State state, List<Symbol> buffer, List<HashSet<Proposition>> accurate, Proposition[] propositions, Proposition[] abstractPropositions, Dictionary<Proposition, Tuple<int, Proposition>> abstractToConcreteMap) {
            this.State = state;
            Buffer = buffer;
            Accurate = accurate;
            Propositions = propositions;
            AbstractToConcreteMap = abstractToConcreteMap;
            AbstractPropositions = abstractPropositions;
        }

        private BufferedState(State state, BufferedState parent, Change change, Symbol symbol = null) { // 
            State = state;
            Propositions = parent.Propositions;
            AbstractToConcreteMap = parent.AbstractToConcreteMap;
            AbstractPropositions = parent.AbstractPropositions;
            _relevant = Relevant();
            change(parent, symbol, out Buffer, out Accurate, _relevant, Depth);
        }

        private BufferedState(BufferedState parent, Change change, Symbol symbol = null) { // 
            State = parent.State;
            Propositions = parent.Propositions;
            AbstractToConcreteMap = parent.AbstractToConcreteMap;
            AbstractPropositions = parent.AbstractPropositions;
            _relevant = Relevant();
            change(parent, symbol, out Buffer, out Accurate, _relevant, Depth);
        }

        public delegate void Change(BufferedState parent, Symbol symbol, out List<Symbol> buffer, out List<HashSet<Proposition>> accurate, List<HashSet<Proposition>> relevant, int depth);

        //should be called in the constructor only
        private static void AppendBuffer(BufferedState parent, Symbol symbol, out List<Symbol> buffer, out List<HashSet<Proposition>> accurate, List<HashSet<Proposition>> relevant, int depth) {
            buffer= new List<Symbol>(parent.Buffer);
            accurate = new List<HashSet<Proposition>>(parent.Accurate);
            int pos = buffer.Count;
            buffer.Add(new Symbol(new HashSet<Proposition>(relevant[pos].Where(symbol.IsTrue))));
            accurate.Add(new HashSet<Proposition>(relevant[pos]));
        }

        //should be called in the constructor only
        private static void ConsumeBuffer(BufferedState parent, Symbol symbol, out List<Symbol> buffer, out List<HashSet<Proposition>> accurate, List<HashSet<Proposition>> relevant, int depth) {
            buffer = new List<Symbol>(parent.Buffer);
            accurate = new List<HashSet<Proposition>>(parent.Accurate);
            if(symbol != null) {buffer.Add(symbol); accurate.Add(new HashSet<Proposition>(parent.Propositions));}
            buffer.RemoveAt(0);
            accurate.RemoveAt(0);
            for (int i = 0; i < depth && i < buffer.Count; i++) {
                buffer[i] = buffer[i].Project(relevant[i]);
                accurate[i] = new HashSet<Proposition>(accurate[i].Intersect(relevant[i]));
            }
        }

        private IEnumerable<Symbol> AbstractSymbols(List<Symbol> buffer, List<HashSet<Proposition>> accurate) {
            var set = new HashSet<Proposition>();
            var unknown = new HashSet<Proposition>();
            var relevantAbstractPropositions = new HashSet<Proposition>(State.GetRelevantPropositions());

            foreach (var kvp in AbstractToConcreteMap.Where(kvp => relevantAbstractPropositions.Contains(kvp.Key))) {
                if (kvp.Value.Item1 < buffer.Count && accurate[kvp.Value.Item1].Contains(kvp.Value.Item2)) {
                    if (buffer[kvp.Value.Item1].IsTrue(kvp.Value.Item2))
                        set.Add(kvp.Key);
                }
                else
                    unknown.Add(kvp.Key);
            }
            foreach (var split in unknown.Split()) {
                split.Item1.UnionWith(set);
                yield return new Symbol(split.Item1);
            }
        }

        // can only be called when Buffer.length >= Depth
        private Symbol AbstractSymbol() {
            var set = new HashSet<Proposition>();
            foreach (var kvp in AbstractToConcreteMap) {
                if (Buffer.Count <= kvp.Value.Item1) {
                    //if(State.LocalDepthPropositions().Contains(kvp.Key))
                    //    throw new Exception("AbstractSymbol to be called only when buffer is deep enough!");
                }
                else if (Buffer[kvp.Value.Item1].IsTrue(kvp.Value.Item2))
                    set.Add(kvp.Key);
            }
            return new Symbol(set);
        }

        // given an underlying Transition System over abstract propositions, gives successors for this buffered state
        public IEnumerable<Tuple<Symbol, bool, BufferedState>> Successors(ConcreteTransitionSystem TS) {
            if (Buffer.Count > Depth)
                throw new Exception("A deep enough buffer should be simplified");
            // assume that the current state is in Simplified form
            foreach (var symbol in Symbol.EnumerateSymbols(Propositions)) {
                var buf = new List<Symbol>(Buffer) {symbol};
                var acc = new List<HashSet<Proposition>>(Accurate) {new HashSet<Proposition>(Propositions)};
                var abstractSymbols = AbstractSymbols(buf, acc);

                bool flag = false;
                foreach (var tup in EnumerateCommonSuccessors(TS, abstractSymbols, symbol)) {
                    yield return tup;
                    flag = true;
                }
                if (!flag && Buffer.Count < Depth)
                    yield return Tuple.Create(symbol, false, new BufferedState(this, AppendBuffer, symbol));
            }

            //if(Buffer.Count < Depth)
            //    foreach (var symbol in Symbol.EnumerateSymbols(Propositions)) {
            //        // if state is final (and hence deterministic) see if we have sufficient information to possibly proceed else append

            //        bool flag = false;
            //        if (State.IsFinal()) {
            //            var abstractSymbols = AbstractSymbols(Buffer, Accurate);
            //            foreach (var tup in EnumerateCommonSuccessors(TS, abstractSymbols, symbol)) {
            //                yield return tup;
            //                flag = true;
            //            }
            //        }
            //        if (!flag)
            //            yield return Tuple.Create(symbol, false, new BufferedState(this, AppendBuffer, symbol));
            //    }
            //if(Buffer.Count == Depth)
        }

        private IEnumerable<Tuple<Symbol, bool, BufferedState>> EnumerateCommonSuccessors(ConcreteTransitionSystem TS, IEnumerable<Symbol> abstractSymbols, Symbol concreteSymbol = null) {
            if (Buffer.Count == 0 && concreteSymbol == null)
                return Enumerable.Empty<Tuple<Symbol, bool, BufferedState>>();
            var common = abstractSymbols.Select(absSymbol => TS.SortedTransitions[State][absSymbol].Select(trans => Tuple.Create(trans.IsFinal, trans.EndState))).Intersection();
            return common.SelectMany(tup => (new BufferedState((State) tup.Item2, this, ConsumeBuffer, concreteSymbol))
                .Simplify(TS, tup.Item1)).Select(tup => Tuple.Create(concreteSymbol, tup.Item1, tup.Item2));
        }

        private bool IsInvariantSatisfied(ConcreteTransitionSystem TS, IEnumerable<Symbol> abstractSymbols) {
            if(!State.IsFinal())
                throw new Exception("Invariants make sense only for final states");
            return abstractSymbols.All(absSymbol => TS.SortedTransitions[State][absSymbol].Any());
        }
        
        private IEnumerable<Tuple<bool, BufferedState>> Simplify(ConcreteTransitionSystem TS, bool final = false) {
            if (Buffer.Count == 0) {
                yield return Tuple.Create(final, this);
                yield break;
            }
            var abstractSymbols = AbstractSymbols(Buffer, Accurate).ToArray();
            bool flag = false;
            foreach (var tup in EnumerateCommonSuccessors(TS, abstractSymbols)) {
                yield return Tuple.Create(tup.Item2 || final, tup.Item3);
                flag = true;
            }
            if (!flag) {
                if (State.IsFinal()) {
                    if (IsInvariantSatisfied(TS, abstractSymbols)) {
                        if (Buffer.Count <= ProgressDepth)
                            yield return Tuple.Create(final, this);
                        else
                            foreach (var tup in (new BufferedState(this, ConsumeBuffer)).Simplify(TS, final))
                                yield return tup;
                    }
                    else {
                        if (Buffer.Count <= InvarianceDepth)
                            yield return Tuple.Create(final, this);
                    }
                }
                else {
                    if (Buffer.Count <= Depth) {
                        yield return Tuple.Create(final, this);
                    }
                }
            }
            //if (Buffer.Count <= Depth) {
                //bool flag = false;
                //if (State.IsFinal()) {
                //    //var abstractSymbols = AbstractSymbols(Buffer, Accurate);
                //    foreach (var tup in EnumerateCommonSuccessors(TS, abstractSymbols)) {
                //        yield return Tuple.Create(tup.Item2, tup.Item3);
                //        flag = true;
                //    }
                //}
                //if (!flag)
                //    yield return Tuple.Create(final, this);
            //}
            //else if (Buffer.Count > Depth) {
            //    //var absSymbol = AbstractSymbol();
            //    var abstractSymbols = AbstractSymbols(Buffer, Accurate);
            //    foreach (var tup in EnumerateCommonSuccessors(TS, abstractSymbols)) {
                    
            //    }
            //    //foreach (var simplified in EnumerateSimplifiedSuccessors(TS, absSymbol, final: final))
            //    //    yield return simplified;
            //}
        }

        private IEnumerable<Symbol> Symbolize(IEnumerable<HashSet<Proposition>> accurate) {
            return accurate.Select(set => new Symbol(set)).ToList();
        }

        protected bool Equals(BufferedState other) {
            return Equals(State, other.State) && Buffer.SequenceEqual(other.Buffer) && (Symbolize(Accurate)).SequenceEqual(Symbolize(other.Accurate));
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((BufferedState) obj);
        }

        public override int GetHashCode() {
            unchecked {
                int bufferHashCode = Buffer.Aggregate(397, (current, symbol) => (current*397) ^ symbol.GetHashCode());
                int accurateHashCode = (Symbolize(Accurate)).Aggregate(397, (current, symbol) => (current*397) ^ symbol.GetHashCode());
                return ((State != null ? State.GetHashCode() : 0)*397) ^ (bufferHashCode) ^ (accurateHashCode);
            }
        }

        private string _toString;
        public override string ToString() {
            return (_toString ?? (_toString = String.Format("{0}, [{1}], [{2}]", State, String.Join(",", Buffer), String.Join(",", Symbolize(Accurate)))));
        }
    }
}
