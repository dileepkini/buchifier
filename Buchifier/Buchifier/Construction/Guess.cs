﻿using System;
using System.Collections.Generic;
using System.Linq;
using Buchifier.Logic;
using Buchifier.Semantics;
using Utils;

namespace Buchifier.Construction {
    public class Guess {
        private readonly HashSet<object> _tau;
        private readonly HashSet<object> _ups;

        protected bool Equals(Guess other) {
            return _tau.SetEquals(other._tau) && _ups.SetEquals(other._ups);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Guess)obj);
        }

        public override int GetHashCode() {
            unchecked {
                return ((_tau != null ? _tau.OrderIndependenHashCode() : 0) * 397) ^ (_ups != null ? _ups.OrderIndependenHashCode() : 0);
            }
        }

        public Guess(IEnumerable<object> tau, IEnumerable<object> ups) {
            _tau = new HashSet<object>(tau);
            _ups = new HashSet<object>(ups);
        }

        public Guess() {
            _tau = new HashSet<object>();
            _ups = new HashSet<object>();
        }


        public bool InTau(object o) {
            return _tau.Contains(o);
        }

        public bool InUps(object o) {
            return _ups.Contains(o);
        }

        public IEnumerable<T> EnumerateTau<T>() {
            return _tau.OfType<T>();
        }

        public IEnumerable<T> EnumerateUps<T>() {
            return _ups.OfType<T>();
        }

        public Guess MoveToUps(Future future) {
            if (InTau(future))
                return new Guess(_tau.Except(future.Yield<object>()), _ups.With(future));
            return this;
        }
        public Guess MoveToUps(Global global) {
            if(!InTau(global) && !InUps(global))
                return new Guess(_tau, _ups.With(global));
            return this;
        }
        public Guess MoveToKap(Future future) {
            if(InTau(future) || InUps(future))
                return new Guess(_tau.Without(future), _ups.Without(future));
            return this;
        }
        public Guess MoveToTau(Global global) {
            if(!InTau(global))
                return new Guess(_tau.With(global), _ups.Without(global));
            return this;
        }
        public Guess MoveToTau(Until until) {
            if(!InUps(until) && !InTau(until))
                return new Guess(_tau.With(until), _ups);
            return this;
        }

        public Guess MoveToUps(Until until) {
            if(!InUps(until))
                return new Guess(_tau.Without(until), _ups.With(until));
            return this;
        }

        public bool IsGlobalGuessValid(Symbol symbol) {
            return EnumerateTau<Global>().All(g => g.Predicate.Evaluate(this, symbol));
        }

        public IEnumerable<Guess> FGSuccessors() {
            foreach (var tup in _ups.Split()) {
                var nextTau = new HashSet<object>(_tau);
                nextTau.UnionWith(tup.Item1);
                yield return new Guess(nextTau, tup.Item2);
            }
        }

        public IEnumerable<Guess> UntilSuccessors(HashSet<Until> allUpsToKap) {

            var tau = new HashSet<Until>(_tau.OfType<Until>());
            var ups = new HashSet<Until>(_ups.OfType<Until>());
            var moveToKap = new HashSet<Until>();
            while (true) {
                var relevantUntils =
                    new HashSet<Until>(ups.Concat(tau));
                var maxRelevant =
                    new HashSet<Until>(
                        relevantUntils.Where(u => relevantUntils.All(v => u.Equals(v) || !v.ConsistsOf(u))));

                var upsToKapSure = new HashSet<Until>(maxRelevant.Intersect(allUpsToKap));
                if (upsToKapSure.IsEmpty())
                    break;
                ups.ExceptWith(upsToKapSure);
                moveToKap.UnionWith(upsToKapSure);
            }
            var upsToKapUnsure = new HashSet<Until>(allUpsToKap.Intersect(ups));

            return upsToKapUnsure.Split().SelectMany(splitUpsUnsure => tau.Split().Select(splitTau =>
                new Guess(splitTau.Item1, ups.Except(splitUpsUnsure.Item1))));
        }

        public override string ToString() {
            return String.Format("{{{0}}}, {{{1}}}", String.Join(", ", _tau.Select(o => o.ToString()).OrderBy(s => s)), String.Join(", ", _ups.Select(o => o.ToString()).OrderBy(s => s)));
        }

        public bool IsUpsEmpty() {
            return _ups.IsEmpty();
        }



        public IEnumerable<Proposition> GetUntilPropositions() {
            return EnumerateUps<Until>().SelectMany(u => u.GetPropositions()).Distinct();
        }
        
        public IEnumerable<Proposition> GetFgPropositions(IEnumerable<Predicate> basis) {
            return EnumerateTau<Global>().Cast<Predicate>()
                .Concat(basis.OfType<Future>().Where(f => !InTau(f))).SelectMany(p => p.GetPropositions()).Distinct();
        }
    }
}
