﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Buchifier.Logic;

namespace Buchifier.Construction.Evaluation {
    public class EvaluateG : Evaluator<HybridState> {

        public override bool Evaluate(Proposition proposition, HybridState data) {
            throw new NotImplementedException();
        }

        public override bool Evaluate(Until until, HybridState data) {
            throw new NotImplementedException();
        }

        public override bool Evaluate(Future future, HybridState data) {
            return !data.Guess.InTau(future) && !data.Guess.InUps(future);
        }

        public override bool Evaluate(Global global, HybridState data) {
            return data.Guess.InTau(global);
        }

        public override bool Evaluate(Not not, HybridState data) {
            throw new NotImplementedException();
        }

        public override bool Evaluate(Or or, HybridState data) {
            return false;
        }
    }
}
