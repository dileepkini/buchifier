﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Buchifier.Logic;
using Buchifier.Semantics;
using NUnit.Framework;

namespace Buchifier.Construction {
    public class EvaluateNow : Evaluator<HybridState> {
        private static readonly EvaluateNow Evaluator = new EvaluateNow();
        private EvaluateNow() {}


        public static bool CallEvaluate(Predicate predicate, HybridState data) {
            return Evaluator.Evaluate(predicate, data);
        }

        public override bool Evaluate(Proposition proposition, HybridState data) {
            bool b;
            return data.Literals.TryGetValue(proposition, out b) && b;
        }

        public override bool Evaluate(Until until, HybridState data) {
            return data.Untils.InUps(until);
        }


        public override bool Evaluate(Future future, HybridState data) {
            return !data.Guess.InTau(future);
        }

        public override bool Evaluate(Global global, HybridState data) {
            return data.Guess.InTau(global);
        }


        public override bool Evaluate(Not not, HybridState data) {
            return !Evaluate(not.Arg, data);
        }
    }
}
