﻿using System;
using Buchifier.Logic;

namespace Buchifier.Construction.Evaluation {
    public class EvaluateF : Evaluator<HybridState> {

        private static readonly  EvaluateF Singleton = new EvaluateF();
        private EvaluateF() { }

        public static bool CallEvaluate(Predicate predicate, HybridState data) {
            return Singleton.Evaluate(predicate, data);
        }

        public override bool Evaluate(Proposition proposition, HybridState data) {
            throw new NotImplementedException();
        }

        public override bool Evaluate(Until until, HybridState data) {
            throw new NotImplementedException();
        }

        public override bool Evaluate(Future future, HybridState data) {
            throw new NotImplementedException();
        }

        public override bool Evaluate(Global global, HybridState data) {
            throw new NotImplementedException();
        }

        public override bool Evaluate(Not not, HybridState data) {
            throw new NotImplementedException();
        }
    }
}
