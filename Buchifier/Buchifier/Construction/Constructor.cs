﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using BDDSharp;
using Buchifier.Automata;
using Buchifier.Construction.Enumeration;
using Buchifier.Logic;
using Buchifier.Semantics;
using NUnit.Framework;
using Utils;

namespace Buchifier.Construction {
    public static class Constructor {

        public static ConcreteTransitionSystem Construct(this Predicate predicate) {
            return predicate.ContainsNext() ? predicate.ConstructNext() : predicate.ConstructNoNext();
        }

        public static ConcreteTransitionSystem ConstructNoNext(this Predicate inputPredicate) {
            var stopwatch = new Stopwatch(); stopwatch.Start();

            if(!inputPredicate.IsFg())
                throw new NotImplementedException();

            #region initialize variables
            var propositions = inputPredicate.GetPropositions().ToArray();
            
            var symbols = Symbol.EnumerateSymbols(propositions).ToArray();
            var basis = inputPredicate.GetBasis(); 
            var order = basis.OfType<Future>().Select((f, i) => Tuple.Create(i+1, f)).ToDictionary(tup => tup.Item1, tup => tup.Item2);
            var topLevel = new HashSet<Predicate>(inputPredicate.TopLevelFG());
            var maximal = new HashSet<Predicate>(topLevel.Where(p => topLevel.All(top => top.Equals(p) || !top.ConsistsOf(p))));

            var botLevel = basis.Except(topLevel).ToArray();
            var initSet = new HashSet<State>();
            #endregion

            #region construct initial set of states
            foreach (var topTriple in topLevel.Triples()) {
                var topGuess = new Guess(topTriple.Item1, topTriple.Item2);
                var topFalse = topTriple.Item1.OfType<Future>()
                    .Cast<Predicate>()
                    .Concat(topTriple.Item3.OfType<Global>());

                // those formulae that are true now or some point in the future
                var topTrue = topLevel.Except(topFalse);


                if (symbols.Any(symbol => inputPredicate.Evaluate(topGuess, symbol) && inputPredicate.TopGuessIsBest(topGuess, symbol, topLevel))) {
                    // A FG subformula is "supported" if it is the subformula of some topLevel formula that is true (or will become true)
                    var unSupported = botLevel.Where(subf => !FormulaIsSupported(subf, topTrue)).ToArray();
                    var unSupportedF = unSupported.OfType<Future>();
                    var unSupportedG = unSupported.OfType<Global>();
                    var supported = botLevel.Except(unSupported);

                    // We don't care about the truth of unsupported subformulae
                    var unSupportedTriple = Tuple.Create(new HashSet<Predicate>(unSupportedF), new HashSet<Predicate>(), new HashSet<Predicate>(unSupportedG));

                    foreach (var botTriple in supported.Triples()) {
                        var tripleTemp = MergeTriples(topTriple, botTriple);
                        var triple = MergeTriples(tripleTemp, unSupportedTriple);

                        var guess = new Guess(triple.Item1, triple.Item2);
                        // counter = 0 only if \kap has no \future. otherwise 1
                        int counter = (triple.Item3.OfType<Future>().IsEmpty() ? 0 : order.Where(kvp => triple.Item3.Contains(kvp.Value)).Select(kvp => kvp.Key).Min());
                        var state = new State(basis, guess, counter, inputPredicate, order, true);

                        //all the G's claimed to be true should evaluate to true, guess should be best possible and predicate should evaluate to true
                        if (symbols.Any(symbol => guess.IsGlobalGuessValid(symbol)
                            && inputPredicate.Evaluate(guess, symbol)
                            && inputPredicate.GuessIsBestInitial(guess, basis, topLevel, symbol))) { // 
                            initSet.Add(state);
                            var set = new HashSet<Symbol>();
                            foreach (
                                var symbol in
                                    symbols.Where(
                                        (symbol =>
                                            guess.IsGlobalGuessValid(symbol) &&
                                            inputPredicate.Evaluate(guess, symbol) &&
                                            inputPredicate.GuessIsBestInitial(guess, basis, topLevel, symbol)))) {
                                set.Add(symbol);
                            }
                            state.InitialInputs = set;
                        }
                    }
                }
            }
            #endregion

            stopwatch.Stop();
            Debug.WriteLine("Initial States    : {0}/{1}", initSet.Count, Math.Pow(3, basis.Count));
            Debug.WriteLine("Initial Time      : {0}", stopwatch.ElapsedMilliseconds); stopwatch.Restart();
            var concTrSystem = ComputeConcreteTransitionSystem(initSet, maximal);  stopwatch.Stop();
            Debug.WriteLine("Construction Time : {0}", stopwatch.ElapsedMilliseconds); stopwatch.Restart();
            concTrSystem.Prune();                            stopwatch.Stop();
            Debug.WriteLine("Prune Time        : {0}, (size: {1})", stopwatch.ElapsedMilliseconds, concTrSystem.StateCount); stopwatch.Restart();
            concTrSystem.MergeStates();                      stopwatch.Stop();
            Debug.WriteLine("MergeState Time   : {0}", stopwatch.ElapsedMilliseconds);
            return concTrSystem;
        }

        //public static ConcreteTransitionSystem ConstructFull(this Predicate predicate) {

        //    var propositions = predicate.GetPropositions().ToArray();
        //    var symbols = Symbol.EnumerateSymbols(propositions).ToArray();
        //    var topLevel = new HashSet<Predicate>(predicate.TopLevel());
        //    var initSet = new HashSet<FullState>();

        //    foreach (var partition in topLevel.Split()) {
        //        var topTrue = partition.Item1;

        //        if (symbols.Any(symbol => predicate.Evaluate(topTrue, symbol) &&
        //                                  predicate.TopGuessIsBest(partition, symbol, topLevel))) {
        //            // supported = those temporal subformulae which are subformulae
        //            //  of some toplevel subformula that is true

        //            // supportedF = the F's derived from supported
        //            // supportedG

        //            // unsupportedFG = allFG - supported

        //            // Fix unsupportedFGs in the triple

        //            // Split supportedFGs into triples
        //            // merge with the previous triple and construct guess
        //            // counter = 0 only if \kap has no \future otherwise 1
        //            // construct the initial state
        //            // add initialInputs

        //        }
        //    }
        //    var concTrSystem = ComputeConcreteTransitionSystem(initSet);
        //    concTrSystem.Prune();
        //    concTrSystem.MergeStates();
        //    return concTrSystem;
        //}

        public static BddTransitionSystem ConstructBddTransitionSystem(this Predicate predicate) {
            return ConstructBddTransitionSystem(predicate.Construct(), predicate);
        }

        public static BddTransitionSystem ConstructBddTransitionSystem(ConcreteTransitionSystem concreteTransitionSystem, Predicate predicate) {
            var stopwatch = new Stopwatch();
            var propositions = predicate.GetPropositions();
            var propCount = propositions.Count;
            var manager = new BDDManager(propCount);
            var indexedPropositions =
               propositions.OrderBy(p => p.Name)
                   .Select((p, i) => Tuple.Create(p, i))
                   .ToDictionary(t => t.Item2, t => t.Item1);
            stopwatch.Start();
            var ret = concreteTransitionSystem.MergeTransitions(manager, indexedPropositions); stopwatch.Stop();
            Debug.WriteLine("MergeTrans Time   : {0}", stopwatch.ElapsedMilliseconds);
            return ret;
        }

        private static ConcreteTransitionSystem ConstructUsingHybrid(this Predicate predicate) {
            var initialStates = EnumerateInitialHybridStates(predicate);
            var transitions = new HashSet<ConcreteTransition>();
            var pending = new HashSet<HybridState>(initialStates);
            var explored = new HashSet<Location>();
            while (!pending.IsEmpty()) {
                var curr = pending.First();
                foreach (var succ in curr.Successors()) {
                    var nextState = succ.Item2;
                    var nextSymbol = succ.Item1;
                    var initialCopy = initialStates.Where(state => state.GuessCounterEquals(nextState));
                    if (!initialCopy.Any()) {
                        var trans = new ConcreteTransition(curr, nextState, nextSymbol, curr.Progress(nextSymbol));
                        transitions.Add(trans);
                        if (!pending.Contains(nextState) && !explored.Contains(nextState))
                            pending.Add(nextState);
                    }
                    else {
                        var initSimilar = initialCopy.First();
                        var allSymbols = Symbol.EnumerateSymbols(nextState.GetAllPropositions().ToArray());
                        var extraSymbols = allSymbols.Except(initSimilar.InitialInputs);
                        if (!extraSymbols.Any(symbol => nextState.HasSuccessorOn(symbol)))
                            nextState = initSimilar;
                        var trans = new ConcreteTransition(curr, nextState, nextSymbol, curr.Progress(nextSymbol));
                        transitions.Add(trans);
                        if (!pending.Contains(nextState) && !explored.Contains(nextState))
                            pending.Add(nextState);
                    }
                }
                explored.Add(curr);
                pending.Remove(curr);
            }
            var initial = new HashSet<Location>(initialStates); initial.IntersectWith(explored);
            return new ConcreteTransitionSystem(explored, transitions, initial, new HashSet<Location>(explored.Where(state => state.IsFinal())));
        }

        public static HashSet<HybridState> EnumerateInitialHybridStates(Predicate predicate) {
            var basis = predicate.GetBasis();
            var order = basis.OfType<Future>().Select((f, i) => Tuple.Create(i + 1, f)).ToDictionary(tup => tup.Item1, tup => tup.Item2);
            var bottom = new HybridState(predicate, basis, new Guess(), new Guess(basis.OfType<Future>(),
                Enumerable.Empty<Predicate>()), true, 0, order, new Dictionary<Proposition, bool>());
           return new HashSet<HybridState>(EnumerateNow.CallEnumerate(predicate, bottom).Where(InitialConditions));
        }

        private static bool InitialConditions(HybridState state) {
            var symbols = Symbol.EnumerateSymbols(state.GetAllPropositions().ToArray());
            state.AssignCounter();
            state.SetInitial(true);
            state.InitialInputs = new HashSet<Symbol>();
            bool flag = false;
            foreach (var symbol in symbols) {
                var data = new HybridState(state, symbol);
                if (data.Guess.IsGlobalGuessValid(symbol) 
                    && data.UntilsLocalConsistent()
                    && EvaluateNow.CallEvaluate(data.InputPredicate, data)) {
                    state.InitialInputs.Add(symbol);
                    flag = true;
                }
            }
            return flag;
        }

        private static ConcreteTransitionSystem ComputeConcreteTransitionSystem(HashSet<State> initialStates, HashSet<Predicate> maximal) {
            var transitions = new HashSet<ConcreteTransition>();
            var pending = new HashSet<State>(initialStates);
            var explored = new HashSet<Location>();
            while(!pending.IsEmpty()) {
                var curr = pending.First();
                foreach (var succ in curr.Successors(maximal)) {
                    var nextState = succ.Item2;
                    var nextSymbol = succ.Item1;
                    var initialCopy = initialStates.Where(state => state.GuessCounterEquals(nextState));
                    if (!initialCopy.Any()) {
                        var trans = new ConcreteTransition(curr, nextState, nextSymbol, curr.Progress(succ.Item1));
                        transitions.Add(trans);
                        if (!pending.Contains(nextState) && !explored.Contains(nextState))
                            pending.Add(nextState);
                    }
                    else {
                        var initSimilar = initialCopy.First();
                        var allSymbols = Symbol.EnumerateSymbols(nextState.GetAllPropositions().ToArray());
                        //Symbol.EnumerateSymbols(nextState.GetRelevantPropositions().ToArray());
                        var extraSymbols = allSymbols.Except(initSimilar.InitialInputs);
                        if (!extraSymbols.Any(symbol => nextState.HasSuccessorOn(symbol, maximal))) {
                            nextState = initSimilar;
                        }
                        var trans = new ConcreteTransition(curr, nextState, nextSymbol, curr.Progress(nextSymbol));
                        transitions.Add(trans);
                        if (!pending.Contains(nextState) && !explored.Contains(nextState))
                            pending.Add(nextState);
                    }
                }
                explored.Add(curr);
                pending.Remove(curr);
            }
            var initial = new HashSet<Location>(initialStates); initial.IntersectWith(explored);
            return new ConcreteTransitionSystem(explored, transitions, initial, new HashSet<Location>(explored.Where(state => state.IsFinal())));
        }

        //private static ConcreteTransitionSystem ComputeConcreteTransitionSystem(HashSet<FullState> initialStates) {
        //    var transitions = new HashSet<ConcreteTransition>();
        //    var pending = new HashSet<FullState>(initialStates);
        //    var explored = new HashSet<Location>();
        //    while (!pending.IsEmpty()) {
        //        var curr = pending.First();
        //        foreach (var succ in curr.Successors()) {
        //            var nextState = succ.Item2;
        //            var nextSymbol = succ.Item1;
        //            var initialCopy = initialStates.Where(fstate => fstate.SyntacticEquals(nextState)).ToArray();
        //            if (initialCopy.Any()) {
        //                var initSimilar = initialCopy.First();
        //                var allSymbols = Symbol.EnumerateSymbols(nextState.GetAllPropositions().ToArray());
        //                var extraSymbols = allSymbols.Except(initSimilar.InitialInputs);
        //                if (!extraSymbols.Any(symbol => nextState.HasSuccessorOn(symbol)))
        //                    nextState = initSimilar;
        //            }
        //            var trans = new ConcreteTransition(curr, nextState, nextSymbol, curr.Progress());
        //            transitions.Add(trans);
        //            if (!pending.Contains(nextState) && explored.Contains(nextState))
        //                pending.Add(nextState);
        //        }
        //        explored.Add(curr);
        //        pending.Remove(curr);
        //    }
        //    var initial = new HashSet<Location>(initialStates);
        //    initial.IntersectWith(explored);
        //    return new ConcreteTransitionSystem(explored, transitions, initial, new HashSet<Location>(explored.Where(state => state.IsFinal())));
        //}

        private static Tuple<HashSet<T>, HashSet<T>, HashSet<T>> MergeTriples<T>(
            Tuple<HashSet<T>, HashSet<T>, HashSet<T>> triple1, Tuple<HashSet<T>, HashSet<T>, HashSet<T>> triple2) {
            return
                Tuple.Create(new HashSet<T>(triple1.Item1.Concat(triple2.Item1)),
                    new HashSet<T>(triple1.Item2.Concat(triple2.Item2)),
                    new HashSet<T>(triple1.Item3.Concat(triple2.Item3)));
        }

        //Enumerate formulae in bottom that are irrelevant (there is none in predicates that are support (consist) of 
        private static bool FormulaIsSupported(Predicate predicate, IEnumerable<Predicate> predicates) {
            return predicates.Any(p => p.ConsistsOf(predicate));
        }

        private static ConcreteTransitionSystem ConstructNext(this Predicate inputPredicate) {
            var propositions = inputPredicate.GetPropositions().ToArray();
            var normalizedPredicate = inputPredicate.PushNext(0);
            var dict = new Dictionary<Proposition, Tuple<int, Proposition>>();
            var replacedPredicate = normalizedPredicate.ReplaceNext(dict, 0);
            var abstractPropositions = replacedPredicate.GetPropositions().ToArray();

            var replacedTransitionSystem = replacedPredicate.ConstructNoNext();

            var initialStates = new HashSet<BufferedState>(replacedTransitionSystem.InitialStates.Select(state => new BufferedState((State)state, new List<Symbol>(), new List<HashSet<Proposition>>(), propositions, abstractPropositions, dict)));
            var transitions = new HashSet<ConcreteTransition>();
            var pending = new HashSet<BufferedState>(initialStates);
            var explored = new HashSet<Location>();
            while (!pending.IsEmpty()) {
                var curr = pending.First();
                foreach (var tuple in curr.Successors(replacedTransitionSystem)) {
                    var trans =
                        new ConcreteTransition(curr, tuple.Item3, tuple.Item1, tuple.Item2); //initialInputs.ContainsKey(curr) && initialInputs[curr].Contains(tuple.Item1)
                    transitions.Add(trans);
                    if (!pending.Contains(tuple.Item3) && !explored.Contains(tuple.Item3))
                        pending.Add(tuple.Item3);
                }
                explored.Add(curr);
                pending.Remove(curr);
            }
            var initial = new HashSet<Location>(initialStates); initial.IntersectWith(explored);
            var ret =  new ConcreteTransitionSystem(explored, transitions, initial, new HashSet<Location>(explored.Where(state => state.IsFinal())));
            Debug.WriteLine("== Pruning Main Tranisition System ==");
            var stopwatch = new Stopwatch(); stopwatch.Start();
            ret.Prune();                            stopwatch.Stop();
            Debug.WriteLine("Prune Time        : {0}, (size: {1})", stopwatch.ElapsedMilliseconds, ret.StateCount); stopwatch.Restart();
            ret.MergeStates();                      stopwatch.Stop();
            Debug.WriteLine("MergeState Time   : {0}", stopwatch.ElapsedMilliseconds);
            return ret;
        }
    }
}
