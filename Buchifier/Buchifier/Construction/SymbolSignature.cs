﻿using System.Collections.Generic;
using Buchifier.Logic;
using Utils;

namespace Buchifier.Construction {
    public class SymbolSignature {
        private readonly HashSet<Future> _futures;

        public SymbolSignature(IEnumerable<Future> futures) {
            _futures = new HashSet<Future>(futures);
        }

        protected bool Equals(SymbolSignature other) {
            return _futures.SetEquals(other._futures);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((SymbolSignature)obj);
        }

        public override int GetHashCode() {
            return (_futures != null ? _futures.OrderIndependenHashCode() : 0);
        }
    }
}
