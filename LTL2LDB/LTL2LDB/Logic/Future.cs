﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BDDSharp;
using LTL2LDB.Construction;
using LTL2LDB.Semantics;
using Utils;

namespace LTL2LDB.Logic {
    public class Future : Predicate {
        private readonly Predicate _predicate;
        public Predicate Arg { get { return _predicate; }}

        public Future(Predicate predicate, bool inScope) : base(inScope) {
            _predicate = predicate;
        }

        public override IEnumerable<Predicate> Unfold(Guess g, Symbol s) {
            var usualUnfold = new Or(_predicate, new Next(this), _inScope);
            if (!_inScope)
                return usualUnfold.Unfold(g, s);
            return (!g.A.Contains(this)).ConvertToPredicate().Yield<Predicate>();
        }

        public override Predicate FixScope(bool inScope = false) {
            return new Future(_predicate.FixScope(inScope), inScope);
        }

        public override HashSet<Predicate> FGdependents() {
            var result = new HashSet<Predicate>();
            if (_inScope)
                result.Add(this);
            result.UnionWith(_predicate.FGdependents());
            return result;
        }

        public override bool IsImmediateParentOf(Predicate predicate) {
            return _predicate.Equals(predicate) || _predicate.IsImmediateParentOf(predicate);
        }

        public override Predicate Negate() {
            return new Global(_predicate.Negate());
        }

        public override Predicate NegationNormalize(bool inScope) {
            return new Future(_predicate.NegationNormalize(inScope), inScope);
        }

        public override BDDNode ConvertToBdd(Context context) {
            return ConvertSubPredicateToBddNode(context);
        }

        public override bool ContainsPendingObligation() {
            return !_inScope;
        }

        public override IEnumerable<Predicate> SubPredicates() {
            return this.Yield<Predicate>().Concat(_predicate.SubPredicates());
        }

        public override IEnumerable<Proposition> GetPropositions() {
            return _predicate.GetPropositions();
        }

        public override Predicate Evaluate(Guess guess, Context context) {
            if (guess.B.Contains(this) || guess.C.Contains(this))
                return new True();
            if (_inScope)
                return new False();
            #region optimization
            var now = _predicate.Evaluate(guess, context).ConvertToBdd(context);
            if (now.IsOne)
                return new True();
            #endregion
            return this;
        }

        public override IEnumerable<Predicate> TopLevel() {
            yield return this;
        }

        public override bool Consists(object predicate) {
            return Equals(predicate) || _predicate.Consists(predicate);
        }

        protected bool Equals(Future other) {
            return base.Equals(other) && Equals(_predicate, other._predicate);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Future) obj);
        }

        public override int GetHashCode() {
            unchecked {
                return (base.GetHashCode()*397) ^ (_predicate != null ? _predicate.GetHashCode() : 0);
            }
        }

        public override string ToString() {
            return "F " + _predicate;
        }
    }
}
