﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BDDSharp;
using LTL2LDB.Construction;
using LTL2LDB.Semantics;
using Utils;

namespace LTL2LDB.Logic {
    public class Until : Predicate {
        private readonly Predicate _predicate1;
        private readonly Predicate _predicate2;

        private readonly Predicate _fdependent;
        private readonly Predicate _gdependent;

        public Until(Predicate predicate1, Predicate predicate2, bool inScope) : base(inScope) {
            _predicate2 = predicate2;
            _predicate1 = predicate1;
            if (inScope) {
                _fdependent = new Future(predicate2, true);
                _gdependent = new Global(predicate1);
            }
        }

        public Predicate Arg1 { get { return _predicate1; } }
        public Predicate Arg2 { get { return _predicate2; } }

        public override IEnumerable<Predicate> Unfold(Guess g, Symbol s) {
            var usualUnfold = new Or(_predicate2, new And(_predicate1, new Next(this), _inScope), _inScope);
            if (!_inScope)
                return usualUnfold.Unfold(g, s);
            if (g.A.Contains(_fdependent))
                return false.ConvertToPredicate().Yield<Predicate>();
            if (g.A.Contains(_gdependent))
                return true.ConvertToPredicate().Yield<Predicate>();
            return usualUnfold.Unfold(g, s);
        }

        public override Predicate FixScope(bool inScope = false) {
            return new Until(_predicate1.FixScope(inScope), _predicate2.FixScope(inScope), inScope);
        }

        public override HashSet<Predicate> FGdependents() {
            var result = new HashSet<Predicate>();
            if (_inScope) {
                result.Add(_fdependent);
                //result.Add(_gdependent);
            }
            result.UnionWith(_predicate1.FGdependents());
            result.UnionWith(_predicate2.FGdependents());
            return result;
        }


        public override bool IsImmediateParentOf(Predicate predicate) {
            return predicate.Equals(_predicate1) || predicate.Equals(_predicate2) ||
                   _predicate1.IsImmediateParentOf(predicate) || _predicate2.IsImmediateParentOf(predicate);
        }

        public override Predicate Negate() {
            return new Release(_predicate1.Negate(), _predicate2.Negate());
        }

        public override Predicate NegationNormalize(bool inScope) {
            return new Until(_predicate1.NegationNormalize(inScope), _predicate2.NegationNormalize(inScope), inScope);
        }

        public override BDDNode ConvertToBdd(Context context) {
            return ConvertSubPredicateToBddNode(context);
        }

        public override bool ContainsPendingObligation() {
            return !_inScope;
        }

        public override IEnumerable<Predicate> SubPredicates() {
            return this.Yield<Predicate>().Concat(_predicate1.SubPredicates().Concat(_predicate2.SubPredicates()));
        }

        public override IEnumerable<Proposition> GetPropositions() {
            return _predicate1.GetPropositions().Concat(_predicate2.GetPropositions());
        }

        public override Predicate Evaluate(Guess guess, Context context) {
            return this;
        }

        public override IEnumerable<Predicate> TopLevel() {
            yield return this;
        }

        public override bool Consists(object predicate) {
            return Equals(predicate) || _predicate1.Consists(predicate) || _predicate2.Consists(predicate);
        }

        protected bool Equals(Until other) {
            return base.Equals(other) && Equals(_predicate1, other._predicate1) && Equals(_predicate2, other._predicate2);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Until) obj);
        }

        public override int GetHashCode() {
            unchecked {
                int hashCode = base.GetHashCode();
                hashCode = (hashCode*397) ^ (_predicate1 != null ? _predicate1.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (_predicate2 != null ? _predicate2.GetHashCode() : 0);
                return hashCode;
            }
        }

        public override string ToString() {
            return "(" + _predicate1 + " U " + _predicate2 + ")";
        }
    }
}
