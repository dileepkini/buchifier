﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using BDDSharp;
using LTL2LDB.Construction;
using LTL2LDB.Semantics;
using Utils;

namespace LTL2LDB.Logic {
    public class Or : Predicate {
        private readonly Predicate _predicate1;
        private readonly Predicate _predicate2;

        public Predicate Arg1 { get { return _predicate1; }}
        public Predicate Arg2 { get { return _predicate2; } }

        public Or(Predicate predicate1, Predicate predicate2, bool inScope) : base(inScope) {
            _predicate1 = predicate1;
            _predicate2 = predicate2;
        }

        // should be evaluated (true/false)
        public override IEnumerable<Predicate> Unfold(Guess g, Symbol s) {
            if (_inScope)
                return (from pred1 in _predicate1.Unfold(g, s)
                    from pred2 in _predicate2.Unfold(g, s)
                    select new Or(pred1, pred2, true));
            return _predicate1.Unfold(g, s).Concat(_predicate2.Unfold(g, s));
        }

        public override IEnumerable<Predicate> Choose() {
            return _inScope ? base.Choose() : _predicate1.Choose().Concat(_predicate2.Choose());
        }

        public override Predicate FixScope(bool inScope = false) {
            return new Or(_predicate1.FixScope(inScope), _predicate2.FixScope(inScope), inScope);
        }

        public override HashSet<Predicate> FGdependents() {
            var result = new HashSet<Predicate>();
            result.UnionWith(_predicate1.FGdependents());
            result.UnionWith(_predicate2.FGdependents());
            return result;
        }

        public override bool IsImmediateParentOf(Predicate predicate) {
            throw new NotImplementedException();
        }

        public override Predicate Negate() {
            // third argument does not matter
            return new And(_predicate1.Negate(), _predicate2.Negate(), true);
        }

        public override Predicate NegationNormalize(bool inScope) {
            return new Or(_predicate1.NegationNormalize(inScope), _predicate2.NegationNormalize(inScope), inScope);
        }

        public override BDDNode ConvertToBdd(Context context) {
            var manager = context.Manager;
            return manager.Reduce(manager.Or(_predicate1.ConvertToBdd(context), _predicate2.ConvertToBdd(context)));
        }

        public override bool ContainsPendingObligation() {
            return (!_inScope && (_predicate1.ContainsPendingObligation() || _predicate1.ContainsPendingObligation()));
        }

        public override IEnumerable<Predicate> SubPredicates() {
            return _predicate1.SubPredicates().Concat(_predicate2.SubPredicates());
        }

        public override IEnumerable<Proposition> GetPropositions() {
            return _predicate1.GetPropositions().Concat(_predicate2.GetPropositions());
        }

        public override Predicate Evaluate(Guess guess, Context context) {
            var arg1 = _predicate1.Evaluate(guess, context);
            var arg2 = _predicate2.Evaluate(guess, context);
            if (arg1 is False)
                return arg2;
            if (arg1 is True)
                return arg1;
            if (arg2 is False)
                return arg1;
            if (arg2 is True)
                return arg2;
            return new Or(arg1, arg2, _inScope);
        }

        public override IEnumerable<Predicate> TopLevel() {
            return _predicate1.TopLevel().Concat(_predicate2.TopLevel());
        }

        public override bool Consists(object predicate) {
            return Equals(predicate) || _predicate1.Equals(predicate);
        }

        protected bool Equals(Or other) {
            return base.Equals(other) && Equals(_predicate1, other._predicate1) && Equals(_predicate2, other._predicate2);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Or) obj);
        }

        public override int GetHashCode() {
            unchecked {
                int hashCode = base.GetHashCode();
                hashCode = (hashCode*397) ^ (_predicate1 != null ? _predicate1.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (_predicate2 != null ? _predicate2.GetHashCode() : 0);
                return hashCode;
            }
        }

        public override string ToString() {
            return "(" + _predicate1 + " | " + _predicate2 + ")";
        }
    }
}
