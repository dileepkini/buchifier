﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LTL2LDB.Construction;
using LTL2LDB.Semantics;

namespace LTL2LDB.Logic {
    public class FutureN : Predicate {
        private readonly Predicate _predicate;

        public FutureN(Predicate predicate) {
            _predicate = predicate;
        }

        protected bool Equals(FutureN other) {
            return Equals(_predicate, other._predicate);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((FutureN) obj);
        }

        public override int GetHashCode() {
            return (_predicate != null ? _predicate.GetHashCode() : 0);
        }

        public override Predicate DUnfold(Guess g, Symbol s) {
            throw new NotImplementedException();
        }

        public override IEnumerable<Predicate> NUnfold(Guess g, Symbol s) {
            yield return this;
            foreach (var p in _predicate.NUnfold(g, s))
                yield return p;
        }

        public override HashSet<Predicate> FGdependents() {
            var result = new HashSet<Predicate> { this };
            result.UnionWith(_predicate.FGdependents());
            return result;
        }

        public override Predicate Negate() {
            return new Global(_predicate.Negate());
        }

        public override Predicate NegationNormalize(bool inScope) {
            if(inScope)
                return new Future(_predicate.NegationNormalize(true));
            return new FutureN(_predicate.NegationNormalize(false));
        }
    }
}
