﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BDDSharp;
using LTL2LDB.Construction;
using LTL2LDB.Semantics;

namespace LTL2LDB.Logic {
    public class True : Predicate {
        public override IEnumerable<Predicate> Unfold(Guess g, Symbol s) {
            yield return this;
        }

        public override Predicate FixScope(bool inScope = false) {
            return this;
        }

        public override HashSet<Predicate> FGdependents() {
            return new HashSet<Predicate>();
        }

        public override bool IsImmediateParentOf(Predicate predicate) {
            throw new NotImplementedException();
        }

        public override Predicate Negate() {
            return new False();
        }

        public override Predicate NegationNormalize(bool inScope) {
            return this;
        }

        public override BDDNode ConvertToBdd(Context context) {
            return context.Manager.One;
        }

        public override bool ContainsPendingObligation() {
            return false;
        }

        public override IEnumerable<Predicate> SubPredicates() {
            yield break;
        }

        public override IEnumerable<Proposition> GetPropositions() {
            yield break;
        }

        public override Predicate Evaluate(Guess guess, Context context) {
            return this;
        }

        public override IEnumerable<Predicate> TopLevel() {
            yield break;
        }

        public override bool Consists(object predicate) {
            return Equals(predicate);
        }

        public override string ToString() {
            return "True";
        }
    }
}
