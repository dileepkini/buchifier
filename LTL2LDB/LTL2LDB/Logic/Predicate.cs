﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management.Instrumentation;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using BDDSharp;
using LTL2LDB.Construction;
using LTL2LDB.Semantics;

namespace LTL2LDB.Logic {
    public abstract class Predicate {
        protected readonly bool _inScope;

        // obligations for the next state
        public abstract IEnumerable<Predicate> Unfold(Guess g, Symbol s);

        // resolving non-deterministic choice
        public virtual IEnumerable<Predicate> Choose() {
            yield return this;
        }

        protected Predicate(bool inScope) {
            _inScope = inScope;
        }

        protected Predicate() {
            _inScope = true;
        }

        public abstract Predicate FixScope(bool inScope = false);

        public abstract HashSet<Predicate> FGdependents();

        // should only be called for Futures/Globals
        public abstract bool IsImmediateParentOf(Predicate predicate);

        public abstract Predicate Negate();

        public abstract Predicate NegationNormalize(bool inScope);

        // check if the given formula is one of the required
        protected BDDNode ConvertSubPredicateToBddNode(Context context) {
            int index;
            var manager = context.Manager;
            if (context.SubPredicates.TryGetValue(this, out index))
                return manager.Create(index, manager.One, manager.Zero);
            throw new Exception();
        }

        public abstract BDDNode ConvertToBdd(Context context);

        // start rotating counter only when top level obligations have been dispatched
        public abstract bool ContainsPendingObligation();

        public abstract IEnumerable<Predicate> SubPredicates();

        public abstract IEnumerable<Proposition> GetPropositions();

        public abstract Predicate Evaluate(Guess guess, Context context);

        public abstract IEnumerable<Predicate> TopLevel();

        public abstract bool Consists(object predicate);

        protected bool Equals(Predicate other) {
            return _inScope.Equals(other._inScope);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Predicate) obj);
        }

        public override int GetHashCode() {
            return _inScope.GetHashCode();
        }


        public bool IsFofUntil() {
            var f = this as Future;
            if (f == null) return false;
            var u = f.Arg as Until;
            return u != null;
        }

        public Future GetArg2FofUntil() {
            var f = this as Future;
            if (f == null) return null;
            var u = f.Arg as Until;
            if (u == null) return null;
            return new Future(u.Arg2, true);
        }
    }
}
