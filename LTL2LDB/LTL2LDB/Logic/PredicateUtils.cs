﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BDDSharp;
using LTL2LDB.Construction;

namespace LTL2LDB.Logic {
    public static class PredicateUtils {
        public static Predicate ConvertToPredicate(this bool a) {
            return (a ? (Predicate)new True() : new False());
        }
    }
}
