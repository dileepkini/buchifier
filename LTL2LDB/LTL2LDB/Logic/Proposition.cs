﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using BDDSharp;
using LTL2LDB.Construction;
using LTL2LDB.Semantics;
using Utils;

namespace LTL2LDB.Logic {
    public class Proposition : Predicate {
        public string Name { get; private set; }

        public Proposition(string name) {
            Name = name;
        }

        public override IEnumerable<Predicate> Unfold(Guess g, Symbol s) {
            return s.IsTrue(this).ConvertToPredicate().Yield<Predicate>();
        }

        public override Predicate FixScope(bool inScope = false) {
            return this;
        }

        public override HashSet<Predicate> FGdependents() {
            return new HashSet<Predicate>();
        }

        public override bool IsImmediateParentOf(Predicate predicate) {
            throw new NotImplementedException();
        }

        public override Predicate Negate() {
            return new Not(this);
        }

        public override Predicate NegationNormalize(bool inScope) {
            return this;
        }

        public override BDDNode ConvertToBdd(Context context) {
            var manager = context.Manager;
            return manager.Create(context.SubPredicates[this], manager.One, manager.Zero);
        }

        public override bool ContainsPendingObligation() {
            return false;
        }

        public override IEnumerable<Predicate> SubPredicates() {
            yield break;
        }

        public override IEnumerable<Proposition> GetPropositions() {
            yield return this;
        }

        public override Predicate Evaluate(Guess guess, Context context) {
            return this;
        }

        public override IEnumerable<Predicate> TopLevel() {
            yield break;
        }

        public override bool Consists(object predicate) {
            return Equals(predicate);
        }

        protected bool Equals(Proposition other) {
            return base.Equals(other) && string.Equals(Name, other.Name);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Proposition) obj);
        }

        public override int GetHashCode() {
            unchecked {
                return (base.GetHashCode()*397) ^ (Name != null ? Name.GetHashCode() : 0);
            }
        }

        public override string ToString() {
            return Name;
        }

    }
}
