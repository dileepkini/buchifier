﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BDDSharp;
using LTL2LDB.Construction;
using LTL2LDB.Semantics;
using Utils;

namespace LTL2LDB.Logic {
    public class Global : Predicate {
        private readonly Predicate _predicate;

        public Predicate Arg {
            get { return _predicate; }
        }

        public Global(Predicate predicate) {
            _predicate = predicate;
        }

        public override IEnumerable<Predicate> Unfold(Guess g, Symbol s) {
            return (g.A.Contains(this)).ConvertToPredicate().Yield<Predicate>();
        }

        public override Predicate FixScope(bool inScope = false) {
            return new Global(_predicate.FixScope(true));
        }

        public override HashSet<Predicate> FGdependents() {
            var result = new HashSet<Predicate> {this};
            result.UnionWith(_predicate.FGdependents());
            return result;
        }

        public override bool IsImmediateParentOf(Predicate predicate) {
            return _predicate.Equals(predicate) || _predicate.IsImmediateParentOf(predicate);
        }

        public override Predicate Negate() {
            // second argument does not matter
            return new Future(_predicate.Negate(), true);
        }

        public override Predicate NegationNormalize(bool inScope) {
            return new Global(_predicate.NegationNormalize(true));
        }

        public override BDDNode ConvertToBdd(Context context) {
            return ConvertSubPredicateToBddNode(context);
        }

        public override bool ContainsPendingObligation() {
            return false;
        }

        public override IEnumerable<Predicate> SubPredicates() {
            return this.Yield<Predicate>().Concat(_predicate.SubPredicates());
        }

        public override IEnumerable<Proposition> GetPropositions() {
            return _predicate.GetPropositions();
        }

        public override Predicate Evaluate(Guess guess, Context context) {
            return (guess.A.Contains(this)).ConvertToPredicate();
        }

        public override IEnumerable<Predicate> TopLevel() {
            yield return this;
        }

        public override bool Consists(object predicate) {
            return Equals(predicate) || _predicate.Consists(predicate);
        }

        protected bool Equals(Global other) {
            return base.Equals(other) && Equals(_predicate, other._predicate);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Global) obj);
        }

        public override int GetHashCode() {
            unchecked {
                return (base.GetHashCode()*397) ^ (_predicate != null ? _predicate.GetHashCode() : 0);
            }
        }

        public override string ToString() {
            return "G " + _predicate;
        }
    }
}
