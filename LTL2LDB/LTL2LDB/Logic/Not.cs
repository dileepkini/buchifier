﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BDDSharp;
using LTL2LDB.Construction;
using LTL2LDB.Semantics;
using Utils;

namespace LTL2LDB.Logic {
    public class Not : Predicate {
        private readonly Predicate _predicate;

        public Not(Predicate predicate) {
            _predicate = predicate;
        }

        //unfolding should be done on negation normalized form
        public override IEnumerable<Predicate> Unfold(Guess g, Symbol s) {
            var prop = _predicate as Proposition;
            if(prop == null)
                throw new NotImplementedException();
            return (!s.IsTrue(prop)).ConvertToPredicate().Yield<Predicate>();
        }

        public override Predicate FixScope(bool inScope = false) {
            return new Not(_predicate.FixScope(inScope));
        }

        public override HashSet<Predicate> FGdependents() {
            return _predicate.FGdependents();
        }

        public override bool IsImmediateParentOf(Predicate predicate) {
            throw new NotImplementedException();
        }

        public override Predicate Negate() {
            return _predicate;
        }

        public override Predicate NegationNormalize(bool inScope) {
            if (_predicate is Proposition)
                return this;
            return _predicate.Negate().NegationNormalize(inScope);
        }

        public override BDDNode ConvertToBdd(Context context) {
            var manager = context.Manager;
            return manager.Reduce(manager.Negate(_predicate.ConvertToBdd(context)));
        }

        public override bool ContainsPendingObligation() {
            return (!_inScope && _predicate.ContainsPendingObligation());
        }

        public override IEnumerable<Predicate> SubPredicates() {
            return _predicate.SubPredicates();
        }

        public override IEnumerable<Proposition> GetPropositions() {
            return _predicate.GetPropositions();
        }

        public override Predicate Evaluate(Guess guess, Context context) {
            return this;
        }

        public override IEnumerable<Predicate> TopLevel() {
            // assuming formula is in negation normal
            yield break;
        }

        public override bool Consists(object predicate) {
            return Equals(predicate) || _predicate.Consists(predicate);
        }

        protected bool Equals(Not other) {
            return base.Equals(other) && Equals(_predicate, other._predicate);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Not) obj);
        }

        public override int GetHashCode() {
            unchecked {
                return (base.GetHashCode()*397) ^ (_predicate != null ? _predicate.GetHashCode() : 0);
            }
        }

        public override string ToString() {
            return "!" + _predicate;
        }
    }
}
