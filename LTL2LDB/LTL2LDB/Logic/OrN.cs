﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LTL2LDB.Construction;
using LTL2LDB.Semantics;

namespace LTL2LDB.Logic {
    public class OrN : Predicate {
        public OrN(Predicate predicate1, Predicate predicate2) {
            _predicate1 = predicate1;
            _predicate2 = predicate2;
        }

        protected bool Equals(OrN other) {
            return Equals(_predicate1, other._predicate1) && Equals(_predicate2, other._predicate2);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((OrN) obj);
        }

        public override int GetHashCode() {
            unchecked {
                return ((_predicate1 != null ? _predicate1.GetHashCode() : 0)*397) ^ (_predicate2 != null ? _predicate2.GetHashCode() : 0);
            }
        }

        private readonly Predicate _predicate1;
        private readonly Predicate _predicate2;

        public override Predicate DUnfold(Guess g, Symbol s) {
            throw new NotImplementedException();
        }

        public override IEnumerable<Predicate> NUnfold(Guess g, Symbol s) {
            return _predicate1.NUnfold(g, s).Concat(_predicate2.NUnfold(g, s));
        }

        public override HashSet<Predicate> FGdependents() {
            var result = new HashSet<Predicate>();
            result.UnionWith(_predicate1.FGdependents());
            result.UnionWith(_predicate2.FGdependents());
            return result;
        }

        public override Predicate Negate() {
            return new And(_predicate1.Negate(), _predicate2.Negate());
        }

        public override Predicate NegationNormalize(bool inScope) {
            var n1 = _predicate1.NegationNormalize(inScope);
            var n2 = _predicate2.NegationNormalize(inScope);
            if(inScope)
                return new Or(n1, n2);
            return new OrN(n1, n2);
        }
    }
}
