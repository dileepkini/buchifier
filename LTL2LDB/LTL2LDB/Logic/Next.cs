﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BDDSharp;
using LTL2LDB.Construction;
using LTL2LDB.Semantics;
using Utils;

namespace LTL2LDB.Logic {
    public class Next : Predicate {
        private readonly Predicate _predicate;

        public Next(Predicate predicate) {
            _predicate = predicate;
        }

        public override IEnumerable<Predicate> Unfold(Guess g, Symbol s) {
            return _predicate.Choose();
        }

        public override Predicate FixScope(bool inScope = false) {
            return new Next(_predicate.FixScope(inScope));
        }

        public override HashSet<Predicate> FGdependents() {
            return _predicate.FGdependents();
        }

        public override bool IsImmediateParentOf(Predicate predicate) {
            throw new NotImplementedException();
        }

        public override Predicate Negate() {
            return new Next(_predicate.Negate());
        }

        public override Predicate NegationNormalize(bool inScope) {
            return new Next(_predicate.NegationNormalize(inScope));
        }

        public override BDDNode ConvertToBdd(Context context) {
            return ConvertSubPredicateToBddNode(context);
        }

        public override bool ContainsPendingObligation() {
            return !_inScope && _predicate.ContainsPendingObligation();
        }

        public override IEnumerable<Predicate> SubPredicates() {
            return this.Yield<Predicate>().Concat(_predicate.SubPredicates());
        }

        public override IEnumerable<Proposition> GetPropositions() {
            return _predicate.GetPropositions();
        }

        public override Predicate Evaluate(Guess guess, Context context) {
            return this;
        }

        public override IEnumerable<Predicate> TopLevel() {
            yield return this;
        }

        public override bool Consists(object predicate) {
            return Equals(predicate) || _predicate.Consists(predicate);
        }

        protected bool Equals(Next other) {
            return base.Equals(other) && Equals(_predicate, other._predicate);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Next) obj);
        }

        public override int GetHashCode() {
            unchecked {
                return (base.GetHashCode()*397) ^ (_predicate != null ? _predicate.GetHashCode() : 0);
            }
        }

        public override string ToString() {
            return "X " + _predicate;
        }
    }
}
