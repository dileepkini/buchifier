﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LTL2LDB.Logic;

namespace LTL2LDB.Grammar {
    class ExprVisitor : LTLBaseVisitor<Predicate> {
        public override Predicate VisitProp(LTLParser.PropContext context) {
            return new Proposition(context.PROP().GetText());
        }

        public override Predicate VisitTrue(LTLParser.TrueContext context) {
            return new True();
        }

        public override Predicate VisitNot(LTLParser.NotContext context) {
            return new Not(Visit(context.expr()));
        }


        public override Predicate VisitAndOr(LTLParser.AndOrContext context) {
            var left = Visit(context.expr(0));
            var right = Visit(context.expr(1));
            if (context.op.Type == LTLParser.AND)
                return new And(left, right, true);
            return new Or(left, right, true);
        }

        public override Predicate VisitUntil(LTLParser.UntilContext context) {
            return new Until(Visit(context.expr(0)), Visit(context.expr(1)), true);
        }

        public override Predicate VisitRelease(LTLParser.ReleaseContext context) {
            return new Release(Visit(context.expr(0)), Visit(context.expr(1)));
        }

        public override Predicate VisitAlwsEvntNxt(LTLParser.AlwsEvntNxtContext context) {
            var sub = Visit(context.expr());
            switch (context.op.Type) {
                case LTLParser.ALW:
                    return new Global(sub);
                case LTLParser.EVN:
                    return new Future(sub, true);
                case LTLParser.NXT:
                    return new Next(sub);
                default:
                    throw new NotImplementedException();
            }
        }

        public override Predicate VisitParen(LTLParser.ParenContext context) {
            return Visit(context.expr());
        }
    }
}
