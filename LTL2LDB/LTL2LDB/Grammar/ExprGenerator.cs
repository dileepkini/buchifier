﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antlr4.Runtime;
using LTL2LDB.Logic;

namespace LTL2LDB.Grammar {
    public static class ExprGenerator {
        public static Predicate Generate(string str) {
            var input = new AntlrInputStream(str);
            var lexer = new LTLLexer(input);
            var tokens = new CommonTokenStream(lexer);
            var parser = new LTLParser(tokens);
            var tree = parser.expr();
            var visitor = new ExprVisitor();
            return visitor.Visit(tree);
        }

        public static Predicate GenerateFromFile(string filename) {
            var input = new AntlrFileStream(filename);
            var lexer = new LTLLexer(input);
            var tokens = new CommonTokenStream(lexer);
            var parser = new LTLParser(tokens);
            var tree = parser.expr();
            var visitor = new ExprVisitor();
            return visitor.Visit(tree);
        }
    }
}
