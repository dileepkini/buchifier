﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using BDDSharp;
using LTL2LDB.Logic;

namespace LTL2LDB.Construction {
    public class Context {
        public readonly Dictionary<int,Future> Order;

        // all temporal/propositions
        public readonly Dictionary<Predicate,int> SubPredicates;
        //public readonly Dictionary<Proposition,int> PropositionsDictionary;
        public readonly Proposition[] PropositionsArray;
        public readonly BDDManager Manager;

        public Context(Dictionary<int, Future> order, IEnumerable<Predicate> subPredicates, Proposition[] propositions) {
            Order = order;
            var numberOfProps = propositions.Length;
            SubPredicates = propositions.Concat(subPredicates).Select((pred, i) => Tuple.Create(pred, i))
                .ToDictionary(tup => tup.Item1, tup => tup.Item2);
            PropositionsArray = propositions;
            //PropositionsDictionary = propositions.Select((p, i) => Tuple.Create(p, i)).ToDictionary(tup => tup.Item1, tup => tup.Item2);
            Manager = new BDDManager(SubPredicates.Count);
        }
    }
}
