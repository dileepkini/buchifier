﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BDDSharp;
using LTL2LDB.Automata;
using LTL2LDB.Logic;
using LTL2LDB.Semantics;
using Utils;

namespace LTL2LDB.Construction {
    public static class Constructor {
        public static ConcreteTransitionSystem Construct(this Predicate inputPredicate) {
            var stopwatch = new Stopwatch(); stopwatch.Start();

            #region initialize variables
            var predicate = inputPredicate.FixScope().NegationNormalize(false);
            var propositions = predicate.GetPropositions().Distinct().ToArray();
            var dependents = predicate.FGdependents();
            // dependents.RemoveWhere(IsFofUntil);
            // remove F (x U y) because equivalent F y already present
            var order =
                dependents.OfType<Future>().Where(f => !f.IsFofUntil())
                    .Select((f, i) => Tuple.Create(i + 1, f))
                    .ToDictionary(tup => tup.Item1, tup => tup.Item2);
            var initSet = new HashSet<State>();
            var subPredicates = predicate.SubPredicates().Distinct();
            //var manager = new BDDManager(propositions.Length + subPredicates.Count());
            var context = new Context(order, subPredicates, propositions);

            var topLevel = new HashSet<Predicate>(inputPredicate.TopLevel());
            var topLevelG = topLevel.OfType<Global>();
            var maximalG = new HashSet<Global>(topLevelG.Where(p => topLevel.All(top => top.Equals(p) || !top.Consists(p))));
            var initChoice = predicate.Choose();
            #endregion

            #region construct initial set of states

            foreach (var choice in initChoice) {
                // choice is a conjunction of temporal formulae
                var choiceDependents = choice.FGdependents();
                // TODO - we need to make sure that every dependent that is claimed true (now or later)
                // is either a top level formula or
                // has an immediate parent that is claimed true (now or later)

                var otherDependents = dependents.Except(choiceDependents);
                foreach (var guess in EnumerateGuesses(choiceDependents, otherDependents)) {
                    // guess should be weak w.r.t maximal G subformulae
                    // i.e weakening a maximal G-assumption (A->B->C) should lead to a strictly stronger obligation
                    //if(guess.SubformulaOptimized())
                    if (!guess.IsWeakest(maximalG, context, choice))
                        continue;
                    var obligation = choice.Evaluate(guess, context);
                    var counter = (guess.C.OfType<Future>().Where(f => !f.IsFofUntil()).IsEmpty()
                        ? 0
                        : order.Where(kvp => guess.C.Contains(kvp.Value)).Select(kvp => kvp.Key).Min());
                    var state = new State(guess, obligation, new False(), counter, true, context);
                    initSet.Add(state);
                }
            }
            Debug.WriteLine("initSet size: {0}", initSet.Count);
            #endregion

            stopwatch.Stop();
            Console.WriteLine("Initial Time         : {0}, (size: {1})\n", stopwatch.ElapsedMilliseconds, initSet.Count);
            stopwatch.Restart();

            #region perform successor computation to explore graph
            var concreteTransitionSystem = PostComputation(initSet);
            #endregion

            stopwatch.Stop();
            Console.WriteLine("PostComputation Time : {0}, (size: {1})\n", stopwatch.ElapsedMilliseconds, concreteTransitionSystem.StateCount);
            stopwatch.Restart();

            concreteTransitionSystem.Prune();

            stopwatch.Stop();
            Console.WriteLine("Prune Time           : {0}, (size: {1})\n", stopwatch.ElapsedMilliseconds, concreteTransitionSystem.StateCount); 
            stopwatch.Restart();

            //concreteTransitionSystem.MyhillNerodeReduction();

            //stopwatch.Stop();
            //Console.WriteLine("MyhillNerode Time    : {0}, (size: {1})\n", stopwatch.Elapsed,
            //    concreteTransitionSystem.StateCount);
            //stopwatch.Restart();

            concreteTransitionSystem.MergeStates();

            stopwatch.Stop();
            Console.WriteLine("MergeState Time      : {0}, (size: {1})\n", stopwatch.ElapsedMilliseconds,
                concreteTransitionSystem.StateCount);

            return concreteTransitionSystem;
        }

        private static ConcreteTransitionSystem PostComputation(HashSet<State> initStates) {
            var transitions = new HashSet<ConcreteTransition>();
            var pending = new HashSet<State>(initStates);
            var explored = new HashSet<State>();
            while (!pending.IsEmpty()) {
                var curr = pending.First();
                foreach (var succ in curr.Successors()) {
                    var nextSymbol = succ.Item1;
                    var nextState = succ.Item2;
                    var copy = pending.Concat(explored).FirstOrDefault(loc => loc.Equals(nextState));
                    if (copy != null)
                        nextState = copy;
                    var trans = new ConcreteTransition(curr, nextState, nextSymbol);
                    transitions.Add(trans);
                    if (copy == null) {
                        pending.Add(nextState);
                    }
                }
                explored.Add(curr);
                pending.Remove(curr);
            }
            var initial = new HashSet<Location>(initStates);
            initial.IntersectWith(explored);
            var final = new HashSet<Location>(explored.Where(state => state.IsFinal()));
            return new ConcreteTransitionSystem(new HashSet<Location>(explored), transitions, initial, final);
        }

        private static IEnumerable<Guess> EnumerateGuesses(IEnumerable<Predicate> predicates, IEnumerable<Predicate> unsupported) {
            var result = new Dictionary<Predicate, int>();
            foreach (var dict in RecursiveTripleSplit(predicates, result)) {
                var fullDict = new Dictionary<Predicate, int>(dict);
                if (fullDict.Any(kvp => kvp.Value == 1 && IsFGorGf(kvp.Key)))
                    continue;
                // If F (x U y) exists then so should F (y) and in the same partition
                if (fullDict.Any(kvp => kvp.Key.IsFofUntil() && (fullDict[kvp.Key.GetArg2FofUntil()] != kvp.Value)))
                    continue;

                foreach (var f in unsupported.OfType<Future>())
                    fullDict[f] = 0;
                foreach (var g in unsupported.OfType<Global>())
                    fullDict[g] = 2;
                yield return new Guess(fullDict);
            }
        }

        private static bool IsFGorGf(Predicate predicate) {
            var f = predicate as Future;
            Global g;
            if (f != null) {
                g = f.Arg as Global;
                return g != null;
            }
            g = predicate as Global;
            if (g != null) {
                f = g.Arg as Future;
                return f != null;
            }
            return false;
        }

        

        private static IEnumerable<Dictionary<Predicate, int>> RecursiveTripleSplit(IEnumerable<Predicate> predicates, Dictionary<Predicate, int> dictionary) {
            if (!predicates.Any()) {
                yield return dictionary;
                yield break;
            }
            foreach (var dict in RecursiveTripleSplit(predicates.Skip(1), dictionary)) {
                var predicate = predicates.First();
                dict[predicate] = 0;
                yield return dict;
                dict[predicate] = 1;
                yield return dict;
                dict[predicate] = 2;
                yield return dict;
            }
        }

        public static BddTransitionSystem ConstructBddTransitionSystem(this Predicate predicate) {
            return ConstructBddTransitionSystem(predicate.Construct(), predicate);
        }

        public static BddTransitionSystem ConstructBddTransitionSystem(ConcreteTransitionSystem concreteTransitionSystem, Predicate predicate) {
            var stopwatch = new Stopwatch();
            var propositions = predicate.GetPropositions().Distinct();
            var propCount = propositions.Count();
            // this manager is only for managing propositional symbols fo the transitions
            // this has nothing to do with the bdds used in representing the states
            var manager = new BDDManager(propCount);
            var indexedPropositions =
               propositions.OrderBy(p => p.Name)
                   .Select((p, i) => Tuple.Create(p, i))
                   .ToDictionary(t => t.Item2, t => t.Item1);
            stopwatch.Start();
            var ret = concreteTransitionSystem.MergeTransitions(manager, indexedPropositions); stopwatch.Stop();
            Debug.WriteLine("MergeTrans Time   : {0}", stopwatch.ElapsedMilliseconds);
            return ret;
        }
    }
}
