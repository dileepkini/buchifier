﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BDDSharp;
using LTL2LDB.Logic;
using LTL2LDB.Semantics;
using Utils;

namespace LTL2LDB.Construction {
    public class State : Location {
        private readonly Guess _guess;
        private readonly BDDNode _conjBddNode;
        private readonly BDDNode _disjBddNode;
        private readonly Predicate _conjPredicate;
        private readonly Predicate _disjPredicate;
        private readonly int _counter;
        private readonly bool _isInitial;
        private readonly Context _context;
        private readonly bool Final;
        private readonly Proposition[] _relevantPropositions;
        private readonly Proposition[] _irrelevantPropositions;

        public State(Guess guess, Predicate conjPredicate, Predicate disjPredicate, int counter, bool isInitial, Context context) {
            _guess = guess;
            _conjBddNode = conjPredicate.ConvertToBdd(context);
            _disjBddNode = disjPredicate.ConvertToBdd(context);
            _conjPredicate = conjPredicate;
            _disjPredicate = disjPredicate;
            _counter = counter;
            _isInitial = isInitial;
            _context = context;
            _relevantPropositions = _getRelevantPropositions().ToArray();
            _irrelevantPropositions = _context.PropositionsArray.Except(_relevantPropositions).ToArray();
        }

        public Predicate ConjPredicate {
            get { return _conjPredicate; }
        }

        public Predicate DisjPredicate {
            get { return _disjPredicate; }
        }

        private int NextCounter(Guess guess) {
            if (_counter == 0 || guess.B.Any() || _disjBddNode != _context.Manager.One || _conjPredicate.ContainsPendingObligation())
                return _counter;
            var allFinC = _context.Order.Where(kvp => _guess.InC(kvp.Value)).Select(kvp => kvp.Key).ToArray();
            var first = allFinC.Min();
            var nextF = allFinC.Where(i => i > _counter).ToArray();
            return nextF.IsEmpty() ? first : nextF.Min();
        }

        public override Proposition[] GetRelevantPropositionArray() {
            return _relevantPropositions;
        }

        public override Proposition[] GetIrrelevantPropositionArray() {
            return _irrelevantPropositions;
        }

        private IEnumerable<Proposition> _getRelevantPropositions() {
            // all the G's in A, all the F's in B, _counter F in C (if \nu is true then next counter), conj predicate
            var relevantPredicates = new List<Predicate>();
            relevantPredicates.AddRange(_guess.A.OfType<Global>());
            relevantPredicates.AddRange(_guess.B.OfType<Future>());
            if (_counter > 0 && _guess.B.IsEmpty())
                relevantPredicates.Add(_disjBddNode.IsOne
                    ? _context.Order[NextCounter(_guess)]
                    : _context.Order[_counter]);
            // TODO cleanup the conjunctive predicate Bdd -> Formula
            relevantPredicates.Add(_conjPredicate);
            return relevantPredicates.SelectMany(pred => pred.GetPropositions()).Distinct();
        }

        public override IEnumerable<Proposition> GetRelevantPropositions() {
            return _getRelevantPropositions();
        }

        public bool IsSimulatedBy(State state) {
            return _guess.Equals(state._guess) && _context.Manager.Implies(_conjBddNode, state._conjBddNode);
        }

        public IEnumerable<Tuple<Symbol, State>> Successors() {
            foreach (var guess in _guess.Successors()) {
                //var relevantPropositions = GetRelevantPropositions().ToArray();
                //var irrelevantPropositions = _context.PropositionsArray.Except(relevantPropositions).ToArray();
                foreach (var symbol in _relevantPropositions.EnumerateSymbols()) {

                    #region update counter
                    int nextCounter = NextCounter(guess);
                    #endregion

                    #region update disjunct
                    var oldDisj = _disjPredicate;
                    Predicate newDisj;
                    if (_counter > 0 && !_guess.B.Any())
                        if (_disjBddNode == _context.Manager.One) {
                            var nextFormula = _context.Order[nextCounter];
                            newDisj = nextFormula.Arg.Unfold(_guess, symbol).First().Evaluate(guess, _context);
                        }
                        else {
                            var curFormula = _context.Order[_counter];
                            newDisj = (new Or(oldDisj, curFormula.Arg, true)).Unfold(_guess, symbol).First().Evaluate(guess, _context);
                        }
                    else
                        newDisj = oldDisj;
                    #endregion

                    #region update conjunct, iterate through unfolding
                    var conj = _conjPredicate;
                    conj = _guess.B.OfType<Future>().Intersect(guess.A.OfType<Future>()).Aggregate(conj, (current, future) => new And(current, future.Arg, false));
                    conj = _guess.A.OfType<Global>().Aggregate(conj, (current, global) => new And(current, global.Arg, false));

                    // not just choosing distinct, but choosing least according to the implication partial order
                    foreach (var conjUnfolded in conj.Unfold(_guess, symbol).Select(pred => pred.Evaluate(guess, _context)).WeakestObligations(_context)) {
                        var nextState = new State(guess, conjUnfolded, newDisj, nextCounter, false, _context);
                        //foreach(var extSymbol in symbol.Extension(irrelevantPropositions))
                            yield return Tuple.Create(symbol, nextState);
                    }
                    #endregion
                }
            }
        }

        protected bool Equals(State other) {
            return Equals(_guess, other._guess) && _context.Manager.Equivalent(_conjBddNode, other._conjBddNode) && _context.Manager.Equivalent(_disjBddNode, other._disjBddNode) && _counter == other._counter; 
                //&& _isInitial.Equals(other._isInitial);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((State) obj);
        }

        public override int GetHashCode() {
            unchecked {
                int hashCode = (_guess != null ? _guess.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (_conjBddNode != null ? _conjBddNode.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (_disjBddNode != null ? _disjBddNode.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ _counter;
                //hashCode = (hashCode * 397) ^ _isInitial.GetHashCode();
                return hashCode;
            }
        }

        public override bool IsInitial() {
            return _isInitial;
        }

        public override bool IsFinal() {
            return (_guess.B.IsEmpty() && !_conjBddNode.Equals(_context.Manager.Zero) && (_disjBddNode.Equals(_context.Manager.One) || _counter == 0) &&
                    !_conjPredicate.ContainsPendingObligation());
        }

        public override Guess GetGuess() {
            return _guess;
        }

        private string _toString;

        public override string ToString() {
            return (_toString ?? (_toString = String.Format("{0},{1},{2} ({3}){4}", _guess, _conjPredicate, _disjPredicate,_counter, (_isInitial ? "*" : ""))));
        }
    }
}
