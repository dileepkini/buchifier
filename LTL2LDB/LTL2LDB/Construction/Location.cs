﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LTL2LDB.Logic;

namespace LTL2LDB.Construction {
    public abstract class Location {
        public abstract bool IsInitial();
        public abstract bool IsFinal();
        public abstract Guess GetGuess();
        public abstract IEnumerable<Proposition> GetRelevantPropositions();
        public abstract Proposition[] GetRelevantPropositionArray();
        public abstract Proposition[] GetIrrelevantPropositionArray();

        public bool SameComponent(Location other) {
            return !GetGuess().C.Except(other.GetGuess().C).Any();
        }
    }
}
