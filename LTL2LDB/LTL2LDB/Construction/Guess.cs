﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using LTL2LDB.Logic;
using LTL2LDB.Semantics;
using Utils;

namespace LTL2LDB.Construction {
    /// <summary>
    /// Each guess maps a FG-dependent to either 0/1/2 corresponding to components A/B/C
    /// </summary>
    public class Guess {
        public IEnumerable<Predicate> A {
            get { return GetComponent(0); }
        }

        public IEnumerable<Predicate> B {
            get { return GetComponent(1); }
        }

        public IEnumerable<Predicate> C {
            get { return GetComponent(2); }
        }

        public bool InC(Predicate predicate) {
            return _partition[predicate] == 2;
        }

        private IEnumerable<Predicate> GetComponent(int n) {
            return _partition.Where(kvp => kvp.Value == n).Select(kvp => kvp.Key);
        }
 
        private readonly Dictionary<Predicate, int> _partition;

        public IEnumerable<Guess> Successors() {
            foreach (var tup in B.Split()) {
                var newPartition = new Dictionary<Predicate, int>(_partition);
                foreach (var pred in tup.Item1) {
                    newPartition[pred] = 0;
                }
                if (newPartition.Any(kvp => kvp.Key.IsFofUntil() && (newPartition[kvp.Key.GetArg2FofUntil()] != kvp.Value)))
                    continue;
                yield return new Guess(newPartition);
            }
        }

        public bool IsWeakest(HashSet<Global> maximalGlobals, Context context, Predicate predicate) {
            var obligation = predicate.Evaluate(this, context).ConvertToBdd(context);
            var manager = context.Manager;
            foreach (var g in maximalGlobals.Intersect(A.Union(B))) {
                var partition = new Dictionary<Predicate, int>(_partition);
                partition[g] = 2;
                var newGuess = new Guess(partition);
                var newObligation = predicate.Evaluate(newGuess, context).ConvertToBdd(context);
                if (manager.Implies(obligation, newObligation))
                    return false;
            }
            return true;
        }

        public Guess(Dictionary<Predicate,int> dictionary) {
            _partition = dictionary;
        }

        protected bool Equals(Guess other) {
            return _partition.Count == other._partition.Count && !_partition.Except(other._partition).Any();
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Guess) obj);
        }

        public override int GetHashCode() {
            return (_partition != null ? (((A.OrderIndependenHashCode() * 397) ^ B.OrderIndependenHashCode())*397) ^ C.OrderIndependenHashCode() : 0);
        }

        private string OrderPredicates(IEnumerable<Predicate> predicates) {
            return String.Join(", ", predicates.Select(p => p.ToString()).OrderBy(o => o));
        }

        public override string ToString() {
            return String.Format("({0} $ {1} $ {2})", OrderPredicates(A), OrderPredicates(B), OrderPredicates(C));
        }
    }
}
