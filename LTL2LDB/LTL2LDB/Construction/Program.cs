﻿using System;
using System.IO;
using LTL2LDB.Automata;
using LTL2LDB.Construction;
using LTL2LDB.Grammar;
using LTL2LDB.Logic;
using CommandLine;
using CommandLine.Text;

namespace LTL2LDB {
    public static class Program {
        public static void Main(string[] args) {
            var options = new Options();
            if (Parser.Default.ParseArguments(args, options)) {
                Predicate predicate;
                string fileName;
                if (options.InputFile != null) {
                    predicate = ExprGenerator.GenerateFromFile(options.InputFile);
                    fileName = options.InputFile;
                } else if (options.PredicateString != null) {
                    predicate = ExprGenerator.Generate(options.PredicateString);
                    fileName = @".\null";
                } else {
                    Console.WriteLine("Error: please specify input file or input formula (use -h for help)");
                    return;
                }

                var automaton = predicate.ConstructBddTransitionSystem();
                PrintWrite(automaton, fileName, predicate.ToString(), options.Xml, options.Hoaf);
            }
        }

        private static void PrintWrite(BddTransitionSystem automaton, string fileName, string automatonName, bool xml, bool hoaf) {
            Console.WriteLine("Number of States             : {0}", automaton.StateCount);
            Console.WriteLine("Number of Transitions        : {0}", automaton.TransitionCount);
            Console.WriteLine("Number of Final States       : {0}", automaton.FinalCount);
            var dir = Path.GetDirectoryName(fileName);
            if (String.IsNullOrEmpty(dir))
                dir = Directory.GetCurrentDirectory();
            var name = Path.GetFileName(fileName);

            if (!xml) {
                var filePath = dir + "\\" + name + ".dot";
                CreateFile(filePath);
                using (var writer = new StreamWriter(filePath))
                    writer.Write(automaton.DotString());
                // filePath = dir + "\\" + name + ".acc";
                //using (var writer = new StreamWriter(filePath))
                //    writer.Write(automaton.AcceptanceCondition(noBdd));
            } else {
                var filePath = dir + "\\" + name + ".xml";
                CreateFile(filePath);
                using (var writer = new StreamWriter(filePath))
                    writer.Write(automaton.ToXml());
            }

            if (hoaf) {
                var filePath = dir + "\\" + name + ".hoaf";
                CreateFile(filePath);
                using (var writer = new StreamWriter(filePath))
                    writer.Write(automaton.ToHOAF(automatonName));
            }
        }

        private static void CreateFile(string filePath) {
            var fi = new FileInfo(filePath);
            if (!fi.Directory.Exists)
                Directory.CreateDirectory(fi.DirectoryName);
        }
    }

    class Options {
        [Option('f', "file", HelpText = "input file containing the LTL formula")]
        public string InputFile { get; set; }

        [Option('s', "spec", HelpText = "the input LTL formula provided as cmd line argument")]
        public string PredicateString { get; set; }

        //[Option("noBDD", DefaultValue = false, HelpText = "use explicit edges instead of BDD edges")]
        //public bool NoBdd { get; set; }

        [Option("xml", DefaultValue = false, HelpText = "use xml format instead of the default dot for output.")]
        public bool Xml { get; set; }

        [Option("hoaf", DefaultValue = false, HelpText = "additionaly output the automaton in HOA format")]
        public bool Hoaf { get; set; }

        [HelpOption]
        public string GetUsage() {
            return HelpText.AutoBuild(this,
              current => HelpText.DefaultParsingErrorsHandler(this, current));
        }
    }
}

