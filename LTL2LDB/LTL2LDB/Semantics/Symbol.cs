﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BDDSharp;
using LTL2LDB.Construction;
using LTL2LDB.Logic;

namespace LTL2LDB.Semantics {
    public class Symbol {
        private readonly HashSet<Proposition> _assignment;

        public Symbol(HashSet<Proposition> assignment) {
            _assignment = assignment;
        }

        public bool IsTrue(Proposition p) {
            return _assignment.Contains(p);
        }

        public BDDNode ToBddNode(BDDManager manager, Dictionary<int, Proposition> propositions, HashSet<Proposition> relevantPropositions) {
            var ret = manager.One;
            for (int i = 0; i < manager.N; i++) {
                if (!relevantPropositions.Contains(propositions[i]))
                    continue;
                var bddProp = manager.Create(i, manager.One, manager.Zero);
                var cur = _assignment.Contains(propositions[i])
                    ? bddProp
                    : manager.Negate(bddProp);
                cur = manager.Reduce(cur);
                ret = manager.And(ret, cur);
                ret = manager.Reduce(ret);
            }
            return ret;
        }

        public Symbol Project(Proposition[] relevantPropositions) {
            return new Symbol(new HashSet<Proposition>(_assignment.Intersect(relevantPropositions)));
        }

        public IEnumerable<Symbol> Extension(Proposition[] irrelevantPropositions) {
            return
                irrelevantPropositions.EnumerateSymbols().Select(ext => new Symbol(new HashSet<Proposition>(ext._assignment.Union(_assignment))));
        }

        protected bool Equals(Symbol other) {
            return _assignment.SetEquals(other._assignment);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Symbol)obj);
        }

        public override int GetHashCode() {
            return (_assignment != null ? _assignment.GetHashCode() : 0);
        }

        public override string ToString() {
            return "{" + String.Join(", ", _assignment.OrderBy(prop => prop.Name)) + "}";
        }
    }
}
