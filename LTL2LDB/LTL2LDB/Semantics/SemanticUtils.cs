﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BDDSharp;
using LTL2LDB.Construction;
using LTL2LDB.Logic;

namespace LTL2LDB.Semantics {
    public static class SemanticUtils {
        public static IEnumerable<Symbol> EnumerateSymbols(this Proposition[] propositions) {
            var length = propositions.Count();
            for (int i = 0; i < (1 << length); i++) {
                var assign = new HashSet<Proposition>();
                for (int j = 0; j < propositions.Length; j++)
                    if ((i >> j)%2 == 1)
                        assign.Add(propositions[j]);
                yield return new Symbol(assign);
            }
        }

        public static bool Equivalent(this BDDManager manager, BDDNode node1, BDDNode node2) {
            var eq = manager.Reduce(manager.And(manager.Or(manager.Negate(node1), node2), manager.Or(node1, manager.Negate(node2))));
            return eq.IsOne;
        }

        public static bool Implies(this BDDManager manager, BDDNode node1, BDDNode node2) {
            var eq = manager.Reduce(manager.Or(manager.Negate(node1), node2));
            return eq.IsOne;
        }

        public static IEnumerable<Predicate> WeakestObligations(this IEnumerable<Predicate> predicates, Context context) {
            var bdds = predicates.Select(p => Tuple.Create(p, p.ConvertToBdd(context))).ToArray();
            var manager = context.Manager;
            for (int i = 0; i < bdds.Length; i++) {
                // should ignore i if
                // there exists j != i such that bdd[i] => bdd[j] and (!(bdd[j] <=> bdd[i]) | j < i)
                var ignore = false;
                for (int j = 0; j < bdds.Length; j++) {
                    if ((j != i) && manager.Implies(bdds[i].Item2, bdds[j].Item2) &&
                        (j < i || !manager.Equivalent(bdds[i].Item2, bdds[j].Item2))) {
                        ignore = true;
                        break;
                    }
                }
                if (!ignore)
                    yield return bdds[i].Item1;
            }
        }

        public static string ToFormulaString(this BDDNode v, Dictionary<int, Proposition> prop) {
            if (v.IsZero)
                return "ff";
            if (v.IsOne)
                return "tt";
            if (v.High.IsZero) {
                if (v.Low.IsOne)
                    return "!" + prop[v.Index];
                return String.Format("!{0}.{1}", prop[v.Index], v.Low.ToFormulaString(prop));
            }
            if (v.High.IsOne) {
                if (v.Low.IsZero)
                    return prop[v.Index].ToString();
                return String.Format("({0}+!{0}.{1})", prop[v.Index], v.Low.ToFormulaString(prop));
            }
            if (v.Low.IsZero) {
                return String.Format("{0}.{1}", prop[v.Index], v.High.ToFormulaString(prop));
            }
            if (v.Low.IsOne) {
                return String.Format("({0}.{1}+!{0})", prop[v.Index], v.High.ToFormulaString(prop));
            }
            return String.Format("({0}.{1}+!{0}.{2})", prop[v.Index], v.High.ToFormulaString(prop),
                v.Low.ToFormulaString(prop));
        }

        public static string ToFormulaString(this BDDNode v) {
            if (v.IsZero)
                return "f";
            if (v.IsOne)
                return "t";
            if (v.High.IsZero) {
                if (v.Low.IsOne)
                    return "!" + v.Index;
                return String.Format("!{0}.{1}", v.Index, v.Low.ToFormulaString());
            }
            if (v.High.IsOne) {
                if (v.Low.IsZero)
                    return v.Index.ToString();
                return String.Format("({0}+!{0}.{1})", v.Index, v.Low.ToFormulaString());
            }
            if (v.Low.IsZero) {
                return String.Format("{0}.{1}", v.Index, v.High.ToFormulaString());
            }
            if (v.Low.IsOne) {
                return String.Format("({0}.{1}+!{0})", v.Index, v.High.ToFormulaString());
            }
            return String.Format("({0}.{1}+!{0}.{2})", v.Index, v.High.ToFormulaString(),
                v.Low.ToFormulaString());
        }
    }
}
