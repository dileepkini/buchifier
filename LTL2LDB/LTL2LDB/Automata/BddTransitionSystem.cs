﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using LTL2LDB.Construction;
using LTL2LDB.Logic;
using LTL2LDB.Semantics;
using Utils;

namespace LTL2LDB.Automata {
    public class BddTransitionSystem : TransitionSystem<BddTransition> {

        private readonly Dictionary<int, Proposition> _indexedPropositions;

        public BddTransitionSystem(HashSet<Location> states, HashSet<BddTransition> transitions, HashSet<Location> initialStates, HashSet<Location> finalStates, Dictionary<int, Proposition> propositions)
            : base(states, transitions, initialStates, finalStates) {
            _indexedPropositions = propositions;
        }



        public int CountConnectedComponents {
            get { return new HashSet<Guess>(FinalLocations.Select(loc => loc.GetGuess())).Count; }
        }

        private string DotTransition(BddTransition trans) {
            var label = (trans.Input.ToFormulaString(_indexedPropositions));
            return String.Format("\"{0}\" -> \"{1}\" [label=\"{2}\"]", trans.BeginLocation, trans.EndLocation, label);
        }

        private XElement XmlTransition(BddTransition transition) {
            var irrelevantPropsitions = _indexedPropositions.Values.Except(transition.Relevant).ToArray();
            var label = transition.Input.ToFormulaString(_indexedPropositions);
            return new XElement("transition",
                new XAttribute("begin", transition.BeginLocation), new XAttribute("end", transition.EndLocation),
                new XAttribute("label", label));
        }

        //private XElement XmlFinalTransition(BddTransition transition, bool noBdd) {
        //    var label = transition.BuchiLabel(noBdd, _indexedPropositions);
        //    return new XElement("finalTransition",
        //        new XAttribute("begin", transition.BeginLocation),
        //        new XAttribute("label", label));
        //}




        public string DotString() {
            return String.Format("digraph {{ node [shape=rectangle]; {0};\n{1} }}", String.Join(";\n", Locations.Select(DotNode)),
                String.Join(";\n", Transitions.SelectMany(g => g).Select(DotTransition)));
        }

        //public string AcceptanceCondition(bool noBdd) {
        //    var sb = new StringBuilder();
        //    if (Locations.IsEmpty())
        //        return "Number of final transitions: 0";
        //    if (Locations.First() is BufferedState) {

        //        sb.AppendLine(String.Format("Number of final transitions : {0}\n", FinalTransitionCount));
        //        foreach (var trans in Transitions.SelectMany(t => t).Where(t => t.IsFinal))
        //            sb.AppendLine(trans.BeginState + " -> " + trans.BuchiLabel(noBdd, _indexedPropositions));
        //    } else if (States.First() is State) {
        //        sb.AppendLine(String.Format("Number of final transitions : {0}\n", CountConnectedComponents));
        //        foreach (
        //            var trans in
        //                Transitions.SelectMany(t => t)
        //                    .Where(t => t.IsFinal)
        //                    .ToLookup(trans => trans.BeginLocation.GetGuess())
        //                    .Select(g => g.First())) {
        //            sb.AppendLine(trans.BeginLocation + " -> " + trans.BuchiLabel(noBdd, _indexedPropositions));
        //        }
        //    } else
        //        throw new NotImplementedException();
        //    return sb.ToString();
        //}

        public XElement ToXml() {
            var states = new XElement("states", Locations.Select(XmlNode));
            var transitions = new XElement("transitions",
                Transitions.SelectMany(g => g).Select(XmlTransition));
            var final = new XElement("finalStates", FinalLocations.Select(XmlNode));
                // Transitions.SelectMany(g => g).Where(t => t.IsFinal).Select(t => XmlFinalTransition(t, noBdd)));
            return new XElement("automaton", states, transitions, final);
        }

        public string ToHOAF(string name) {
            var header = String.Join(
                Environment.NewLine,
                "HOA: v1",
                String.Format("name: LDBA for {0}", name),
                String.Format("States: {0}", StateCount)
                );
            var locationNumber = Locations.Select((loc, i) => Tuple.Create(loc, i))
                .ToDictionary(tup => tup.Item1, tup => tup.Item2);
            header = header + "\n" + 
                String.Join(Environment.NewLine, 
                InitialLocations.Select(loc => String.Format("Start: {0}", locationNumber[loc])));
            header = header + "\n" +
                     String.Join(Environment.NewLine,
                         "acc-name: Buchi",
                         "Acceptance: 1 Inf(0)"
                         );
            var orderedPropositions =
                Enumerable.Range(0, _indexedPropositions.Count).Select(i => "\"" + _indexedPropositions[i] + "\"").ToArray();

            header = header + "\n" + String.Format("AP: {0} ", _indexedPropositions.Count) +
                     String.Join(" ", orderedPropositions);

            var body = "--BODY--\n";
            foreach (var loc in Locations) {
                var begin = String.Format("State: {0} \"{1}\"", locationNumber[loc], loc);
                if (FinalLocations.Contains(loc))
                    begin += " {0}";
                begin += "\n";
                var transitions =
                    String.Join(Environment.NewLine,
                        Transitions[loc].Select(
                            trans => String.Format("\t[{0}] {1}", trans.Input.ToFormulaString(), locationNumber[trans.EndLocation])));
                body = body + begin + transitions + "\n";
            }
            body = body + "--END--\n";
            return header + "\n" + body;
        }
    }
}
