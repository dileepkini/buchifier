﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using BDDSharp;
using LTL2LDB.Construction;
using LTL2LDB.Logic;
using LTL2LDB.Semantics;
using Utils;

namespace LTL2LDB.Automata {
    public class ConcreteTransitionSystem : TransitionSystem<ConcreteTransition> {
        private Dictionary<Location, ILookup<Symbol, ConcreteTransition>> _sortedTransitions;

        public Dictionary<Location, ILookup<Symbol, ConcreteTransition>> SortedTransitions {
            get {
                if (_sortedTransitions != null)
                    return _sortedTransitions;
                _sortedTransitions = Transitions.ToDictionary(g => g.Key, g => g.ToLookup(trans => trans.Input));
                return _sortedTransitions;
            }
        }

        public ConcreteTransitionSystem(HashSet<Location> locations, HashSet<ConcreteTransition> transitions, HashSet<Location> initialLocations, HashSet<Location> finalLocations) : base(locations, transitions, initialLocations, finalLocations) { }

        public ConcreteTransitionSystem MyhillNerodeReduction() {
            // 1. Find out states reachable from Final
            // 2. Do BFS on Reverse product graph starting from Final,NonFinal
            // 3. Remember to add sink state

            var finalComponent = new HashSet<Location>();
            var threshold = FinalLocations.ToArray();
            while (!threshold.IsEmpty()) {
                finalComponent.UnionWith(threshold);
                threshold = threshold.SelectMany(state => Transitions[state].Select(trans => trans.EndLocation))
                    .Distinct()
                    .Where(state => !finalComponent.Contains(state)).ToArray();
            }

            int n = finalComponent.Count;
            var stateNumber = finalComponent.Select((loc, i) => Tuple.Create(loc, i + 1))
                .ToDictionary(tup => tup.Item1, tup => tup.Item2);
            var numberState = stateNumber.ToDictionary(kvp => kvp.Value, kvp => kvp.Key);
            // construct graph of 0...n where n = number of states in final component
            // 0 state = sink

            var finalStates = new HashSet<int>(finalComponent.Where(state => state.IsFinal()).Select(state => stateNumber[state]));
            var nonFinalStates = new HashSet<int>(Enumerable.Range(0, n + 1).Except(finalStates));
            var edges = new Dictionary<int, Dictionary<Symbol, int>>();
            var reverse = new Dictionary<int, Dictionary<Symbol, int>>();
            for (int i = 0; i <= n; i++) {
                edges[i] = new Dictionary<Symbol, int>();
                reverse[i] = new Dictionary<Symbol, int>();
            }
            foreach (var state in finalComponent) {
                int nState = stateNumber[state];
                foreach (var trans in Transitions[state]) {
                    edges[nState][trans.Input] = stateNumber[trans.EndLocation];
                    reverse[stateNumber[trans.EndLocation]][trans.Input] = nState;
                }
                foreach (var symbol in state.GetRelevantPropositionArray().EnumerateSymbols().Except(edges[nState].Keys)) {
                    edges[nState][symbol] = 0;
                    reverse[0][symbol] = nState;
                }
            }

            // 0 to 0 on everything.

            // the following array should only be looked up for (i,j) where i < j
            var myhillNerode = new bool[n + 1, n + 1];
            var queue = new Queue<Tuple<int, int>>();
            for (int i = 0; i <= n; i++) {
                var iFinal = finalStates.Contains(i);
                for (int j = i + 1; j <= n; j++) {
                    var jFinal = finalStates.Contains(j);
                    myhillNerode[j, i] = myhillNerode[i, j] = (iFinal == jFinal);
                    if(iFinal != jFinal)
                        queue.Enqueue(Tuple.Create(i,j));
                }
            }
            //while (!queue.IsEmpty()) {
            //    var tup = queue.Dequeue();
            //    var i = tup.Item1;
            //    var j = tup.Item2;
            //    foreach(int k in reverse[i])
            //}
            throw new NotImplementedException();
        }

        public void MergeStates() {
            bool changed;
            do {
                changed = false;
                // 0. it's ok to consider final states
                foreach (var remove in Locations) {
                    // 1. if remove is initial then compare only with initial states
                    var candidates = (InitialLocations.Contains(remove) ? InitialLocations : Locations).Where(loc => remove.SameComponent(loc));
                    if (candidates.Where(state => !state.Equals(remove)).Any(state => Simulates(remove, state))) {
                        Locations.Remove(remove);
                        var validTransitions =
                            Transitions.SelectMany(g => g)
                                .Where(trans => Locations.Contains(trans.BeginLocation) && Locations.Contains(trans.EndLocation))
                                .ToArray();
                        Transitions = validTransitions.ToLookup(t => t.BeginLocation);
                        ReverseLookup = validTransitions.ToLookup(t => t.EndLocation);
                        changed = true;
                        break;
                    }
                }
            } while (changed);
            InitialLocations.IntersectWith(Locations);
            FinalLocations.IntersectWith(Locations);
        }

        private static bool IsForwardSimulatedBy(Location loc1, Location  loc2) {
            var s1 = loc1 as State;
            var s2 = loc2 as State;
            if (s1 != null && s2 != null)
                return s1.IsSimulatedBy(s2);
            return Equals(loc1, loc2);
        }

        private bool Simulates(Location smallState, Location bigState) {
            if (smallState.IsFinal() && !bigState.IsFinal())
                return false;
            var commonRelevantPropositions =
                smallState.GetRelevantPropositionArray().Intersect(bigState.GetRelevantPropositionArray()).ToArray();
            var extraPropositions = bigState.GetRelevantPropositionArray().Except(commonRelevantPropositions).ToArray();
            bool incomingTransitions =
                ReverseLookup[smallState].All(
                    trans => (trans.BeginLocation.Equals(smallState)) ||
                        Transitions[trans.BeginLocation].Any(
                            transSimilar =>
                                trans.Input.Equals(transSimilar.Input) && transSimilar.EndLocation.Equals(bigState)));
            if (!incomingTransitions)
                return false;
            bool goodTransitions = Transitions[smallState].All(trans => 
                trans.Input.Project(commonRelevantPropositions).Extension(extraPropositions).All(symbol =>
                    Transitions[bigState].Any(transSimilar =>
                            symbol.Equals(transSimilar.Input) &&
                            (((Equals(trans.EndLocation, smallState)) && (Equals(transSimilar.EndLocation, bigState))) ||
                             ((!(Equals(trans.EndLocation, smallState))) && (!(Equals(transSimilar.EndLocation, smallState))) && (IsForwardSimulatedBy(trans.EndLocation, transSimilar.EndLocation))))))
                );
            if (!goodTransitions)
                return false;
            bool goesToRemove =
                Transitions[bigState].Where(trans => Equals(trans.EndLocation, smallState))
                    .All(
                        trans =>
                            Transitions[bigState].Any(
                                transSimilar =>
                                    trans.Input.Equals(transSimilar.Input) && Equals(transSimilar.EndLocation, bigState)));
            return goesToRemove;
        }

        public BddTransitionSystem MergeTransitions(BDDManager manager, Dictionary<int, Proposition> propositions) {

            var transitions = Transitions.SelectMany(g => g).ToArray();
            var combined =
                transitions.ToLookup(trans => Tuple.Create(trans.BeginLocation, trans.EndLocation))
                    .Select(
                        g =>
                            new BddTransition(g.Key.Item1, g.Key.Item2,
                                g.Select(trans => trans.Input), manager, propositions, new HashSet<Proposition>(g.Key.Item1.GetRelevantPropositions())))
                    .ToArray();
            return new BddTransitionSystem(Locations, new HashSet<BddTransition>(combined), InitialLocations, FinalLocations, propositions);
        }

        private string DotTransition(object node1, object node2, Symbol symbol) {
            return String.Format("\"{0}\" -> \"{1}\" [label=\"{2}\"]", node1, node2, symbol);
        }

        public string DotString() {
            return String.Format("digraph {{ node [shape=rectangle]; {0};\n{1} }}", String.Join(";\n", Locations.Select(DotNode)),
                String.Join(";\n",
                    Transitions.SelectMany(g => g)
                        .Select(t => DotTransition(t.BeginLocation, t.EndLocation, t.Input))));
        }
    }
}
