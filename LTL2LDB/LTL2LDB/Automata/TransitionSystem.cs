﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using LTL2LDB.Construction;
using LTL2LDB.Logic;
using LTL2LDB.Semantics;
using Utils;

namespace LTL2LDB.Automata {
    public abstract class TransitionSystem<T> where T : Transition {
        public readonly HashSet<Location> Locations;
        protected readonly HashSet<Location> FinalLocations;
        public readonly HashSet<Location> InitialLocations;
        public ILookup<Location, T> Transitions;
        public ILookup<Location, T> ReverseLookup;

        protected TransitionSystem(HashSet<Location> locations, HashSet<T> transitions, HashSet<Location> initialLocations, HashSet<Location> finalLocations) {
            Locations = locations;
            FinalLocations = finalLocations;
            InitialLocations = initialLocations;
            Transitions = transitions.ToLookup(trans => trans.BeginLocation);
            ReverseLookup = transitions.ToLookup(trans => trans.EndLocation);
        }


        private HashSet<Location> CanReach(IEnumerable<Location> targetLocations) {
            var threshold = targetLocations.ToArray();
            var canReach = new HashSet<Location>();
            while (!threshold.IsEmpty()) {
                canReach.UnionWith(threshold);
                threshold = threshold.SelectMany(state => ReverseLookup[state].Select(trans => trans.BeginLocation))
                    .Distinct()
                    .Where(state => Locations.Contains(state) && !canReach.Contains(state)).ToArray();
            }
            return canReach;
        }

        // remove deadlocks and those that cannot reach final
        public void Prune() {
            // do reverse BFS to remove deadlocking states
            bool changed;
            do {
                changed = false;

                #region deadlock removal (_states, _finalStates modified)
                var deadlocked = Locations.Where(state => !Transitions.Contains(state)).ToArray();
                while (!deadlocked.IsEmpty()) {
                    changed = true;
                    Locations.ExceptWith(deadlocked);
                    deadlocked =
                        deadlocked.SelectMany(state => ReverseLookup[state].Select(trans => trans.BeginLocation))
                            .Distinct()
                            .Where(
                                state =>
                                    Locations.Contains(state) &&
                                    Transitions[state].All(trans => !Locations.Contains(trans.EndLocation)))
                            .ToArray();
                }
                FinalLocations.IntersectWith(Locations);
                #endregion

                #region final not reachable (_states modified)
                var canReach = CanReach(FinalLocations);
                var finalCanReachAgain =
                    FinalLocations.Where(final => Transitions[final].Any(trans => canReach.Contains(trans.EndLocation)));
                var canReachFinalAgain = CanReach(finalCanReachAgain);
                if (!Locations.SetEquals(canReachFinalAgain))
                    changed = true;
                Locations.IntersectWith(canReachFinalAgain);
                #endregion

                var validTransitions =
                    Transitions.SelectMany(g => g).Where(trans => Locations.Contains(trans.EndLocation)).ToArray();
                Transitions = validTransitions.ToLookup(t => t.BeginLocation);
                ReverseLookup = validTransitions.ToLookup(t => t.EndLocation);
            } while (changed);
            InitialLocations.IntersectWith(Locations);
        }

        public int StateCount {
            get { return Locations.Count; }
        }

        public int FinalCount {
            get { return Locations.Count(loc => loc.IsFinal()); }
        }

        public int TransitionCount {
            get {
                return Transitions.SelectMany(g => g).Count();
            }
        }

        public int CountConnectedComponents {
            get { return FinalLocations.Select(loc => loc.GetGuess()).Distinct().Count(); }
        }

        protected string DotNode(object obj) {
            if (obj is Location) {
                var node = obj as Location;
                var fill = (node.IsFinal() && !node.IsInitial()
                    ? ",style=\"filled,rounded\",fillcolor=skyblue"
                    : (node.IsFinal()
                        ? ",style=\"filled\",fillcolor=skyblue"
                        : (node.IsInitial() ? "" : ",style=\"rounded\"")));
                return String.Format("\"{0}\"[label=\"{0}\"{1}]", node, fill);
            }
            return obj.ToString();
        }

        protected XElement XmlNode(object obj) {
            if (obj is Location) {
                var node = obj as Location;
                return new XElement("state", new XAttribute("id", node), new XAttribute("initial", node.IsInitial()));
            }
            throw new NotImplementedException();
        }
    }

    
}
