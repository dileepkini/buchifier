﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BDDSharp;
using LTL2LDB.Construction;
using LTL2LDB.Logic;
using LTL2LDB.Semantics;

namespace LTL2LDB.Automata {
    public abstract class Transition {
        public readonly Location BeginLocation;
        public readonly Location EndLocation;

        public Transition(Location beginLocation, Location endLocation) {
            BeginLocation = beginLocation;
            EndLocation = endLocation;
        }
    }

    public class ConcreteTransition : Transition {
        public readonly Symbol Input;

        public ConcreteTransition(Location beginLocation, Location endLocation, Symbol input) : base(beginLocation, endLocation) {
            Input = input;
        }
    }

    public class BddTransition : Transition {
        public readonly BDDNode Input;
        public readonly Symbol[] InputSymbols;
        public readonly HashSet<Proposition> Relevant;

        public BddTransition(Location beginState, Location endState, IEnumerable<Symbol> inputSymbols, BDDManager manager, Dictionary<int, Proposition> propositions, HashSet<Proposition> relevantPropositions)
            : base(beginState, endState) {
            Relevant = new HashSet<Proposition>(relevantPropositions);
            InputSymbols = inputSymbols.ToArray();
            Input = BddForInputs(manager, InputSymbols, propositions, relevantPropositions);
        }

        private BDDNode BddForInputs(BDDManager manager, IEnumerable<Symbol> symbols, Dictionary<int, Proposition> propositions, HashSet<Proposition> relevantPropositions) {
            var cur = manager.Zero;
            foreach (var symbol in symbols) {
                var symbolNode = symbol.ToBddNode(manager, propositions, relevantPropositions);
                cur = manager.Or(symbolNode, cur);
                cur = manager.Reduce(cur);
            }
            return cur;
        }

        //public string BuchiLabel(Dictionary<int, Proposition> indexedPropositions) {
        //    var irrelevant = indexedPropositions.Values.Except(Relevant).ToArray();
        //    return (Buchi.ToFormulaString(indexedPropositions));
        //}
    }
}
