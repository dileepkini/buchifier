﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utils {
    public static class HashUtils {
        public static int OrderIndependenHashCode<T>(this IEnumerable<T> collection,
            IEqualityComparer<T> comparer = null) {
            comparer = comparer ?? EqualityComparer<T>.Default;
            unchecked {
                return collection == null
                    ? 0
                    : collection.Aggregate(OrderIndependentSeed,
                        (x, y) => OrderIndependentCombiner(x, comparer.GetHashCode(y)));
            }
        }

        private static int OrderIndependentCombiner(int i, int j) {
            return i ^ j;
        }

        public static int OrderIndependentSeed = 397;
    }
}
