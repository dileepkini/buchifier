﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utils {
    public static class Divide {
        public static IEnumerable<Tuple<HashSet<T>, HashSet<T>>> Split<T>(this IEnumerable<T> set) {
            if (set.IsEmpty()) {
                yield return Tuple.Create(new HashSet<T>(), new HashSet<T>());
                yield break;
            }
            var member = set.First();
            var remainder = new HashSet<T>(set);
            remainder.Remove(member);
            foreach (var tup in Split(remainder)) {
                var set1 = new HashSet<T>(tup.Item1) { member };
                var set2 = new HashSet<T>(tup.Item2);
                yield return Tuple.Create(set1, set2);
                tup.Item2.Add(member);
                yield return tup;
            }
        }
    }
}
