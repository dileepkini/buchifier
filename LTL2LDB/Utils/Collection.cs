﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utils {
    public static class Collection {
        public static bool IsEmpty<T>(this IEnumerable<T> enumerable) {
            return !enumerable.Any();
        }

        public static IEnumerable<T> Yield<T>(this object obj) {
            yield return (T)obj;
        }

        public static IEnumerable<IEnumerable<T>> Product<T>(this IEnumerable<IEnumerable<T>> sequences) {
            IEnumerable<IEnumerable<T>> emptyProduct = new[] {Enumerable.Empty<T>()};
            return sequences.Aggregate(
                emptyProduct, 
                (acc, seq) =>
                from accseq in acc
                from item in seq
                select accseq.Concat(new[] {item}));
        }
    }
}
