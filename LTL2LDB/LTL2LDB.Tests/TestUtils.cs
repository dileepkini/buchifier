﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using LTL2LDB.Construction;
using LTL2LDB.Grammar;
using LTL2LDB.Logic;
using NUnit.Framework;

namespace LTL2LDB.Tests {
    public static class TestUtils {
        private static void CreateFile(string filePath) {
            var fi = new FileInfo(filePath);
            if (!fi.Directory.Exists) {
                Directory.CreateDirectory(fi.DirectoryName);
            }
        }

        private static string GetClass(string fullName) {
            var match = Regex.Match(fullName, @"\.(\w+)\.\w+$");
            return match.Groups[1].Value;
        }

        private const string OutputFolder = @"C:\Users\Dileep\Documents\LTL2LDB\";

        public static void PerformTest(string predicateString, bool listSpace = false) {
            PerformTest(ExprGenerator.Generate(predicateString), listSpace);
        }

        public static void PerformTest(Predicate predicate, bool listSpace = false) {
            Console.WriteLine("Performing Test on : {0}", predicate);
            //Console.WriteLine("LaTeX: {0}", predicate.ToTex());
            var automaton = predicate.ConstructBddTransitionSystem();
            Console.WriteLine("{0}, {1}({2}), Final:{3}\n", automaton.StateCount, automaton.TransitionCount,  automaton.CountConnectedComponents, automaton.FinalCount);
            if (listSpace) {
                Console.WriteLine("Guess Space: " + String.Join(", ", automaton.Locations.Select(loc => loc.GetGuess().ToString()).Distinct()) + "\n");
                Console.WriteLine("Conj Space: " + String.Join(", ", automaton.Locations.Select(loc => ((State)loc).ConjPredicate).Distinct()) + "\n");
                Console.WriteLine("Disj Space: " + String.Join(", ", automaton.Locations.Select(loc => ((State)loc).DisjPredicate).Distinct()) + "\n");

            }
            var filename = TestContext.CurrentContext.Test.Name;
            var folder = GetClass(TestContext.CurrentContext.Test.FullName);
            var filePath = OutputFolder + folder + "\\" + filename + ".dot";
            CreateFile(filePath);
            using (var writer = new StreamWriter(filePath)) {
                writer.Write(automaton.DotString());
            }
        }

        public static void PerformChoose(string predicateString) {
            PerformChoose(ExprGenerator.Generate(predicateString));
        }

        public static void PerformChoose(Predicate predicate) {
            Console.WriteLine("Performing Choose on : {0}", predicate);
            predicate = predicate.FixScope().NegationNormalize(false);
            foreach (var pred in predicate.Choose()) {
                Console.WriteLine(pred);
            }
        }
    }
}
