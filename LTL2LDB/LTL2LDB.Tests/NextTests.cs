﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace LTL2LDB.Tests {
    [TestFixture]
    public class NextTests {
        [Test]
        public void InfinteAnd() {
            TestUtils.PerformTest("G F (a & X b) & G F (c & X d)");
        }

        [Test]
        public void EH5() {
            TestUtils.PerformTest("G (q | X G p) & G (r | X G ! p)");
        }

        [Test]
        public void EK9() {
            TestUtils.PerformTest("G F (X X X a & X X X X b)  & G F (b | X c) & G F (c & X X a)"); // 
        }

        [Test]
        public void EK9Flatten() {
            TestUtils.PerformTest("G F (a & b) & G F (c | d)"); //  & G F (c & X X a)
        }

        [Test]
        public void EK10() {
            TestUtils.PerformTest("(G F a | F G b) & (G F c | F G (d | X e))");
        }

        [Test]
        public void EK11() {
            TestUtils.PerformTest("(G F (a & X X c) | F G b) & (G F c | F G (d | (X a & X X b)))");
        }

        [Test]
        public void EK8() {
            TestUtils.PerformTest("((G F (a & X X b)) | F G b) & (F G (c | (X a & (X X b))))");
        }

        [Test]
        public void EK8SecondFlat() {
            TestUtils.PerformTest("(F G (c | (a & b)))"); // ((G F (a & X b)) | F G b) &
        }

        [Test]
        public void InfiniteNext() {
            TestUtils.PerformTest("G F (a | X b)");
        }

        [Test]
        public void GFaAndXXb() {
            TestUtils.PerformTest("G F (a & X X b)");
        }

        [Test]
        public void Xa() {
            TestUtils.PerformTest("X a");
        }

        [Test]
        public void GXa() {
            TestUtils.PerformTest("G X a");
        }
    }
}
