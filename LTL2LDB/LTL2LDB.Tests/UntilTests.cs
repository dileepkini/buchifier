﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace LTL2LDB.Tests {
    [TestFixture]
    public class UntilTests {
        [Test]
        public void GofXorUntil() {
            TestUtils.PerformTest("G((X a) & (b U c))");
        }

        [Test]
        public void GofXorUntil2() {
            TestUtils.PerformTest("G(((X a0) & (b0 U c0)) | ((X a1) & (b1 U c1)))");
        }

        [Test]
        public void GofXorUntil3() {
            TestUtils.PerformTest("G(((X a0) & (b0 U c0)) | ((X a1) & (b1 U c1)) | ((X a2) & (b2 U c2)))");
        }

        [Test]
        public void GofUntils() {
            TestUtils.PerformTest("G ((a0 U b0) | (a1 U b1) | (a2 U b2) | (a3 U b3))", listSpace: true); //"G ((a0 U b0) | (a1 U b1))"
        }

        [Test]
        public void GofUntils2() {
            TestUtils.PerformTest("G ((a0 U b0) | (a1 U b1))"); //"G ((a0 U b0) | (a1 U b1))"
        }

        [Test]
        public void GofUntils3() {
            TestUtils.PerformTest("G ((a0 U X X b0) | (a1 U X X b1) | (a2 U X X b2))"); //"G ((a0 U b0) | (a1 U b1))"
        }

        [Test]
        public void aUntilb() {
            TestUtils.PerformTest("a U b");
        }

        [Test]
        public void aUntilGb() {
            TestUtils.PerformTest("(a U (G b)) & (G F c)");
        }

        [Test]
        public void GXaUntilXb() {
            TestUtils.PerformTest("G ((X a) U (X b))");
        }

        [Test]
        public void FaUntilb() {
            TestUtils.PerformTest("F (a U b)");
        }

        [Test]
        public void GaUntilb() {
            TestUtils.PerformTest("G (a U b)");
        }

        [Test]
        public void GaUntilXXb() {
            TestUtils.PerformTest("G (a U (X X b))");
        } 

        [Test]
        public void EK6() {
            TestUtils.PerformTest("(X (G r | r U (r & (s U p)))) U (G r | (r U (r & s)))");
        }

        [Test]
        public void EK7() {
            TestUtils.PerformTest("p U (q & X (r & (F(s & X (F (t & X (F (u & X F v))))))))");
        }

        [Test]
        public void EK13() {
            TestUtils.PerformTest("F G ((a & (X X b) & (G F b)) U (G (X X !c | (X X (a & b)))))");
        }

        [Test]
        public void EK14() {
            TestUtils.PerformTest("(G (F !a & (F(b & X !c)) & G F (a U d))) & G F ((X d) U (b | G c))");
        }

        [Test]
        public void EK14NoGFpre() {
            TestUtils.PerformTest("(G ((F !a) & (F(b & X !c)) & G (a U d)))");
        }

        [Test]
        public void EK14pre() {
            TestUtils.PerformTest("(G ((F !a) & (F(b & X !c)) & G F (a U d)))");
        }

        [Test]
        public void EK14prepre() {
            TestUtils.PerformTest("(G ((F !a) & (F(b & X !c))");
        }

        [Test]
        public void EK14NoGFpost() {
            TestUtils.PerformTest("G ((X d) U (b | G c))");
        }

        [Test]
        public void EK14NoGFpostX() {
            TestUtils.PerformTest("G ((X X X d) U (b | G c))");
        }

        [Test]
        public void GofFs() {
            TestUtils.PerformTest("(G ((F !a) & (F(b & X !c))))");
        }

        [Test]
        public void GFUntil() {
            TestUtils.PerformTest("G F (a U b)");
        }

        [Test]
        public void FGUntil() {
            TestUtils.PerformTest("F G (a U b)");
        }

        [Test]
        public void GUntilUntil() {
            TestUtils.PerformTest("G ((a) U ((b0 U c0) & (F d) & (F e)))");
        }

        [Test]
        public void GOrUntils() {
            TestUtils.PerformTest("G ((c0 & (a0 U b0)) | ((a1 U b1) & c1))");
        }

        [Test]
        public void NestedUntil() {
            TestUtils.PerformTest("(G F a) U (X X b)");
        }

        [Test]
        public void NestedUntilEq() {
            TestUtils.PerformTest(("(X X b) | ((X X F b) & (G F a))"));
        }

        [Test]
        public void NestedExUntil() {
            TestUtils.PerformTest(("G((a00 & F b00)) U G((a10 & F b10) | (a11 & F b11))"));
        }

        [Test]
        public void Nested2ExXUntil() {
            TestUtils.PerformTest(("(X X X X a) U G((a10 & F b10) | (a11 & F b11))"));
        }

        [Test]
        public void NestedExUntil3() {
            TestUtils.PerformTest(("(a) U G((a10 & F b10) | (a11 & F b11) | (a3 & F b3))"));
        }

        [Test]
        public void NestedExXUntil() {
            TestUtils.PerformTest(("(X X X a) U G((a10 & F b10) | (a11 & F b11) | (a3 & F b3))"));
        }

        [Test]
        public void NestedEx4XUntil() {
            TestUtils.PerformTest(("(X X X X a) U G((a10 & F b10) | (a11 & F b11) | (a3 & F b3))"));
        }

        [Test]
        public void UntilParams() {
            TestUtils.PerformTest("(X X X X a) U (b | ((c U X X X X d) & (e U X X X X f)))");
        }

        [Test]
        public void GUntilUntilParams() {
            TestUtils.PerformTest("((G p) U (q | ((a U b))))");
        }

        [Test]
        public void GaUntilbExp() {
            TestUtils.PerformTest("G (a U (b U (c U d)))");
        }
    }
}
