﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace LTL2LDB.Tests {
    public class UnfoldTests {
        [Test]
        public void CheckUnfolding() {
            TestUtils.PerformChoose("(F G a | G F b) & (F G c | G F d)");
        }
    }
}
