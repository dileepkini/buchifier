﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using LTL2LDB.Logic;
using NUnit.Framework;

namespace LTL2LDB.Tests {
    [TestFixture]
    public class WorstCaseTests {
        private static Proposition A(int i) {
            return new Proposition("a" + i);
        }

        private static Proposition B(int i) {
            return new Proposition("b" + i);
        }

        private static Proposition C(int i) {
            return new Proposition("c" + i);
        }

        private static Proposition D(int i) {
            return new Proposition("d" + i);
        }

        private Predicate U(Predicate p1, Predicate p2) {
            return new Until(p1, p2, false);
        }

        private Predicate G(Predicate p) {
            return new Global(p);
        }

        private Predicate F(Predicate p) {
            return new Future(p, true);
        }

        private Predicate Or(params Predicate[] operands) {
            if (operands.Length == 0)
                return new False();
            var build = operands[0];
            for (int i = 1; i < operands.Length; i++) {
                build = new Or(build, operands[i], true);
            }
            return build;
        }

        private Predicate And(params Predicate[] operands) {
            if (operands.Length == 0)
                return new True();
            var build = operands[0];
            for (int i = 1; i < operands.Length; i++) {
                build = new And(build, operands[i], true);
            }
            return build;
        }

        private Predicate Ack1(int i) {
            return Or(A(i), F(B(i)));
        }

        private Predicate Ack2(int i) {
            return Or(A(i), And(F(B(i)), F(C(i))));
        }

        private Predicate AndAlws(int i) {
            return And(A(i), G(B(i)));
        }

        private Predicate AndEvnt(int i) {
            return And(A(i), F(B(i)));
        }

        

        private delegate Predicate Predicator(int i);

        private Predicate[] Repeat(Predicator P, int count) {
            return Enumerable.Range(0, count).Select(i => P(i)).ToArray();
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        [TestCase(4)]
        public void GlobalAck(int n) {
            TestUtils.PerformTest(G(And(Repeat(Ack1, n))));
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        public void Global2Ack(int n) {
            TestUtils.PerformTest(G(And(Repeat(Ack2, n))));
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        [TestCase(4)]
        public void GlobalAndEvnt(int n) {
            TestUtils.PerformTest(G(Or(Repeat(AndEvnt, n))));
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        [TestCase(4)]
        [TestCase(5)]
        public void GlobalAndAlws(int n) {
            TestUtils.PerformTest(G(Or(Repeat(AndAlws, n))));
        }


        [TestCase(2,1)]
        [TestCase(2,2)]
        [TestCase(2,3)]
        [TestCase(4,1)]
        [TestCase(4,2)]
        [TestCase(4,3)]
        public void ExUntil(int m, int n) {
            var nxts = string.Concat(Enumerable.Repeat("X ", m));
            var innerUtemplate = "(a{0} U (" + nxts +" b{0}))";
            var conj = String.Join(" & ", Enumerable.Range(0, n).Select(i => String.Format(innerUtemplate, i)));
            var result = String.Format("({0} p) U ((G q) | ({1}))", nxts, conj);
            TestUtils.PerformTest(result);
        }
    }
}
